The `experiment.py` file contains the the functions to generate the data (`generate_data`) and plot the data (`plot_data`).
You need to install cargo (available via rustup at rustup.rs) to run the simulator and additional python libraries as specified in requirements.txt

