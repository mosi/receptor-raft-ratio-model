use crate::flatten::*;
use crate::input::structures::{
    FloatOp, LogicalOp, PrimitiveTypes, RateExpr, RateExprTerm, SpeciesLeftIdentifier,
};
use rand::prelude::*;
use std::collections::hash_map::RandomState;
use std::collections::{HashMap, HashSet};
use std::convert::{Infallible, TryFrom};
use std::iter;
use std::mem::swap;

//use rayon::prelude::*;

const USE_SLOW_FIRST_REACTION_METHOD: bool = false;
type Uindex = usize;
fn to_uindex(i: &usize) -> Uindex {
    match Uindex::try_from(*i) {
        Ok(y) => y,
        Err(k) => {
            panic!("Could not convert index! 32 bit are not enough for {}", i)
        }
    }
}

#[derive(Clone)]
pub struct IndexedLogicalConstraint {
    pub left: IndexedRateExpr,
    pub op: LogicalOp,
    pub right: IndexedRateExpr,
}

#[derive(Clone)]
struct EnumeratedIndexedReaction {
    pub species_in_idx: Vec<Uindex>,
    pub species_in_amt: Vec<Uindex>,
    pub species_out: Vec<Uindex>,
    pub reaction_number_of_prototype: usize,
    invokes_structural_change: bool,
    guards: Vec<IndexedLogicalConstraint>,
    global_val_changes: Vec<(usize, IndexedRateExprTerm)>,
    rate: IndexedEnumeratedRate,
    dependend_reactions: Vec<Uindex>,
    count: usize,
}
impl EnumeratedIndexedReaction {
    fn shrink(&mut self) {
        self.species_in_idx.shrink_to_fit();
        self.species_in_amt.shrink_to_fit();
        self.species_out.shrink_to_fit();
        self.guards.shrink_to_fit();
        self.global_val_changes.shrink_to_fit();
        self.dependend_reactions.shrink_to_fit();
    }
    fn from(
        reac: &EnumeratedReaction,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        converte_globals: &HashMap<EnumeratedGlobalValue, usize>,
    ) -> EnumeratedIndexedReaction {
        //assert!(reac.global_val_changes.is_empty());
        let spec_in: Vec<_> = reac
            .species_in
            .iter()
            .map(|(_, v)| converse_map.get(v).unwrap())
            .map(|k| to_uindex(k))
            .collect();
        let spec_in_unique: HashSet<_> = spec_in.iter().cloned().collect();
        let spec_in_unique: Vec<Uindex> = spec_in_unique.into_iter().collect();
        let conv_left: HashMap<SpeciesLeftIdentifier, usize> = reac
            .species_in
            .iter()
            .map(|(k, v)| (k.clone(), *converse_map.get(v).unwrap()))
            .collect();
        let mut res = EnumeratedIndexedReaction {
            guards: reac
                .guard
                .iter()
                .map(|v| IndexedLogicalConstraint {
                    left: IndexedRateExpr::from(
                        &v.left,
                        converse_map,
                        &conv_left,
                        converte_globals,
                    ),
                    op: v.op,
                    right: IndexedRateExpr::from(
                        &v.right,
                        converse_map,
                        &conv_left,
                        converte_globals,
                    ),
                })
                .collect(),
            species_in_amt: spec_in_unique
                .iter()
                .map(|v| spec_in.iter().filter(|o| *o == v).count())
                .map(|k| to_uindex(&k))
                .collect(),
            species_in_idx: spec_in_unique,
            species_out: reac
                .species_out
                .iter()
                .map(|v| converse_map.get(v).unwrap())
                .map(|k| to_uindex(&k))
                .collect(),
            reaction_number_of_prototype: reac.reaction_number_of_prototype,
            rate: match &reac.rate {
                EnumeratedRate::MassActionConstant(k) => {
                    IndexedEnumeratedRate::MassActionConstant(*k)
                }
                EnumeratedRate::MassActionExpression(y) => {
                    IndexedEnumeratedRate::MassActionExpression(IndexedRateExpr::from(
                        &y,
                        converse_map,
                        &conv_left,
                        converte_globals,
                    ))
                }
                EnumeratedRate::ComplexExpression(ex) => IndexedEnumeratedRate::ComplexExpression(
                    IndexedRateExpr::from(&ex, converse_map, &conv_left, converte_globals),
                ),
            },
            global_val_changes: reac
                .global_val_changes
                .iter()
                .map(|(v, n)| {
                    (
                        *converte_globals.get(v).unwrap(),
                        match n {
                            PrimitiveOrExpr::Primitive(p) => {
                                IndexedRateExprTerm::Primitive(p.clone())
                            }
                            PrimitiveOrExpr::Expr(x) => {
                                IndexedRateExprTerm::Expr(Box::new(IndexedRateExpr::from(
                                    x,
                                    converse_map,
                                    &conv_left,
                                    converte_globals,
                                )))
                            }
                        },
                    )
                })
                .collect(),
            dependend_reactions: vec![],
            invokes_structural_change: reac.invokes_structural_changes,
            count: 0,
        };
        res.shrink();
        res
    }

    fn get_involved(&self) -> InvolvedInReaction {
        let mut spec: HashSet<Uindex> = HashSet::new();
        let mut globs = HashSet::new();
        let mut time_dep = false;

        match &self.rate {
            IndexedEnumeratedRate::MassActionConstant(_) => spec.extend(self.species_in_idx.iter()),
            IndexedEnumeratedRate::MassActionExpression(y) => {
                spec.extend(self.species_in_idx.iter());
                y.get_involved(&mut spec, &mut globs, &mut time_dep);
            }
            IndexedEnumeratedRate::ComplexExpression(y) => {
                y.get_involved(&mut spec, &mut globs, &mut time_dep);
            }
        }

        InvolvedInReaction {
            changing_species: self
                .species_in_idx
                .iter()
                .chain(self.species_out.iter())
                .cloned()
                .collect(),
            changing_globals: self
                .global_val_changes
                .iter()
                .map(|(v, _)| v)
                .cloned()
                .collect(),
            time_dependend: time_dep,
            rate_species: spec,
            rate_globals: globs,
        }
    }
}

struct InvolvedInReaction {
    changing_species: HashSet<Uindex>,
    changing_globals: HashSet<usize>,
    time_dependend: bool,
    rate_species: HashSet<Uindex>,
    rate_globals: HashSet<Uindex>,
}

#[derive(Clone)]
pub enum IndexedEnumeratedRate {
    MassActionConstant(f64),
    MassActionExpression(IndexedRateExpr),
    ComplexExpression(IndexedRateExpr),
}

#[derive(Clone)]
pub struct IndexedRateExpr {
    pub left: IndexedRateExprTerm,
    pub op: Option<(FloatOp, IndexedRateExprTerm)>,
}

impl IndexedRateExpr {
    fn from(
        rate: &RateExpr,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        converte_left: &HashMap<SpeciesLeftIdentifier, usize>,
        converte_globals: &HashMap<EnumeratedGlobalValue, usize>,
    ) -> IndexedRateExpr {
        IndexedRateExpr {
            left: IndexedRateExprTerm::from(
                &rate.left,
                converse_map,
                converte_left,
                converte_globals,
            ),
            op: match &rate.op {
                None => None,
                Some((op, right)) => Some((
                    *op,
                    IndexedRateExprTerm::from(
                        &right,
                        converse_map,
                        converte_left,
                        converte_globals,
                    ),
                )),
            },
        }
    }
    fn get_involved(
        &self,
        species2rule: &mut HashSet<Uindex>,
        globvals2rule: &mut HashSet<Uindex>,
        use_time: &mut bool,
    ) {
        self.left
            .get_involved(species2rule, globvals2rule, use_time);
        match &self.op {
            None => {}
            Some((_, r)) => r.get_involved(species2rule, globvals2rule, use_time),
        }
    }
}

#[derive(Clone)]
pub enum IndexedRateExprTerm {
    Primitive(PrimitiveTypes),
    //Parameter(String),
    //AttRef(AttributeRef),
    GlobAttRef(usize),
    Expr(Box<IndexedRateExpr>),
    //SpecCount(SpeciesCount),
    GlobSpecCount(usize),
    SpecificCount(Vec<usize>),
    GlobalTime(),
}

fn glob_val_from_left_ident(
    left: &Option<SpeciesLeftIdentifier>,
    converse_map: &HashMap<EnumeratedSpecies, usize>,
    converte_left: &HashMap<SpeciesLeftIdentifier, usize>,
    name: &String,
) -> EnumeratedGlobalValue {
    let left = left.clone().unwrap();
    let left = converte_left.get(&left).unwrap();
    let (left, _) = converse_map.iter().find(|(_k, v)| *v == left).unwrap();
    EnumeratedGlobalValue {
        spec: left.clone(),
        name: name.clone(),
    }
}

impl IndexedRateExprTerm {
    fn from(
        rate: &RateExprTerm,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        converte_left: &HashMap<SpeciesLeftIdentifier, usize>,
        converte_globals: &HashMap<EnumeratedGlobalValue, usize>,
    ) -> IndexedRateExprTerm {
        match rate {
            RateExprTerm::Primitive(t) => IndexedRateExprTerm::Primitive(t.clone()),
            RateExprTerm::Parameter(_) => unreachable!("Parameter"),
            RateExprTerm::AttRef(_) => unreachable!("Att ref"),
            RateExprTerm::GlobAttRef(x) => IndexedRateExprTerm::GlobAttRef(
                *converte_globals
                    .get(&glob_val_from_left_ident(
                        &x.species_id,
                        converse_map,
                        converte_left,
                        &x.att_name,
                    ))
                    .unwrap(),
            ),
            RateExprTerm::Expr(x) => match &x.op {
                None => IndexedRateExprTerm::from(
                    &x.left,
                    converse_map,
                    converte_left,
                    converte_globals,
                ),
                Some(_) => IndexedRateExprTerm::Expr(Box::new(IndexedRateExpr::from(
                    &(*x),
                    converse_map,
                    converte_left,
                    converte_globals,
                ))),
            },
            RateExprTerm::SpecCount(x) => {
                IndexedRateExprTerm::GlobSpecCount(*converte_left.get(&x.idx_on_left).unwrap())
            }
            RateExprTerm::GlobSpecCount(_y) => unimplemented!("GlobSpecCount"),
            RateExprTerm::SpecificCount(x) => IndexedRateExprTerm::SpecificCount(
                x.iter()
                    .map(|v| converse_map.get(v).unwrap())
                    .cloned()
                    .collect(),
            ),
            RateExprTerm::GlobalTime() => IndexedRateExprTerm::GlobalTime(),
        }
    }

    fn get_involved(
        &self,
        species2rule: &mut HashSet<Uindex>,
        globvals2rule: &mut HashSet<Uindex>,
        use_time: &mut bool,
    ) {
        match self {
            IndexedRateExprTerm::Primitive(_) => {}
            IndexedRateExprTerm::GlobAttRef(y) => {
                globvals2rule.insert(to_uindex(y));
            }
            IndexedRateExprTerm::Expr(y) => y.get_involved(species2rule, globvals2rule, use_time),
            IndexedRateExprTerm::GlobSpecCount(y) => {
                species2rule.insert(to_uindex(y));
            }
            IndexedRateExprTerm::SpecificCount(y) => {
                species2rule.extend(y.iter().map(|k| to_uindex(k)));
            }
            IndexedRateExprTerm::GlobalTime() => {
                *use_time = true;
            }
        };
    }
}

#[derive(Clone)]
pub struct SimulationFast {
    //pub model: EnumaratedModel,
    //pub target_time: f64,
    //pub step_size: f64,
    reactions: Vec<EnumeratedIndexedReaction>,
    pub(crate) species_counts: Vec<usize>,
    pub species_lookup: HashMap<EnumeratedSpecies, usize>,

    pub observation_values: Vec<Vec<PrimitiveTypes>>,
    pub obs_reaction_counts: Vec<Vec<usize>>,
    pub dead_observations_to_keep: usize, //the number of observables from old structures, that we keep at zero

    pub observation_names: Vec<String>,
    pub reaction_names: Vec<String>,
    pub reaction_counts: Vec<usize>,

    pub global_values: Vec<PrimitiveTypes>,
    pub obs_global_vals_names: Vec<String>,

    time_dependend_reactions: Vec<Uindex>,

    pub time: f64,
    propensities: Vec<f64>,
    propensity_sum: f64,
    propensitiy_tree: Vec<f64>,
    pub stepcount: usize,
    count_for_next_sort: usize,

    pub(crate) rng: SmallRng,
    pub(crate) last_io: f64,
    pub obs_times: Vec<f64>,
}

impl SimulationFast {
    fn set_propensity(&mut self, idx: Uindex, value: f64) {
        let idx = idx as usize;
        let old_prop_sum = self.propensity_sum;
        let old_propensity = self.propensities[idx];
        self.propensity_sum -= self.propensities[idx];
        self.propensities[idx] = value;
        self.propensity_sum += self.propensities[idx];
        self.update_tree(idx, self.propensities[idx] - old_propensity);
        if old_prop_sum > self.propensity_sum * 100. {
            self.rebuild_tree();
        }
    }
    fn rebuild_tree(&mut self) {
        if USE_SLOW_FIRST_REACTION_METHOD {
            self.propensity_sum = self.propensities.iter().sum();
            return;
        }
        let mut sum = 0.;
        self.propensitiy_tree = vec![0.; self.propensitiy_tree.len()];
        let copyied_ids: Vec<_> = self.propensities.iter().cloned().enumerate().collect();
        for (idx, propensity) in copyied_ids {
            self.update_tree(idx, propensity);
            sum += propensity;
        }
        assert!(sum >= 0.);
        self.propensity_sum = sum;
    }
    fn update_tree(&mut self, mut idx: usize, delta: f64) {
        if USE_SLOW_FIRST_REACTION_METHOD {
            return;
        }
        loop {
            self.propensitiy_tree[idx] += delta;
            if !(self.propensitiy_tree[idx] >= 0.) {
                self.propensitiy_tree[idx] = 0.;
                //self.rebuild_tree();
            }
            if idx == 0 {
                break;
            }
            idx = (idx - 1) / 2; // parent node
        }
    }
    fn select_from_threshold(&mut self, first_try: bool, rand_val: f64) -> Option<usize> {
        if self.propensity_sum <= 0. {
            return None;
        }
        let mut thrshold = rand_val * self.propensity_sum;
        let mut idx = 0;
        loop {
            let y = match self.propensities.get(idx) {
                None => break,
                Some(&y) => y,
            };
            if y >= thrshold {
                return Some(idx);
            } else {
                thrshold -= y;
            }

            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;

            match self.propensitiy_tree.get(left_child) {
                None => break,
                Some(&y) => {
                    if y >= thrshold {
                        idx = left_child;
                    } else {
                        thrshold -= y;
                        idx = right_child;
                    }
                }
            }
        }

        if first_try {
            self.rebuild_tree();
            return self.select_from_threshold(false, rand_val);
        }
        if self.propensitiy_tree.len() < 1000 {
            println!("Internal reaction selection error at {}! Tree problem {}!\n Tree {:?}\n{:?} \n sum={}, rand_val={}", idx, self.propensitiy_tree.len(), self.propensitiy_tree, self.propensities, self.propensity_sum, rand_val);
        }
        println!(
            "Internal reaction selection error at {}! Tree problem {}!\n \n sum={}, rand_val={}",
            idx,
            self.propensitiy_tree.len(),
            self.propensity_sum,
            rand_val
        );
        let rand_val = self.rng.gen_range(0.0, 1.);
        return self.select_from_threshold(false, rand_val);
    }
    fn get_next_reaction_idx_slow(&mut self) -> Option<usize> {
        let propsum: f64 = self.propensities.iter().sum();
        let propensity_threshold = self.rng.gen_range(0.0, propsum);
        match self
            .propensities
            .iter()
            .scan(0.0, |sum, p| {
                *sum += p;
                Some(*sum)
            })
            .enumerate()
            .find(|(_idx, sum)| *sum > propensity_threshold)
        {
            None => None,
            Some((idx, _)) => Some(idx),
        }
    }
    fn get_next_reaction_idx(&mut self) -> Option<usize> {
        if ((self.stepcount % (10000 * self.propensitiy_tree.len())) == 0)
            || (self.propensity_sum < 0.)
        {
            self.rebuild_tree();
        }
        if USE_SLOW_FIRST_REACTION_METHOD {
            match self.get_next_reaction_idx_slow() {
                Some(x) => return Some(x),
                None => {
                    self.rebuild_tree();
                    return self.get_next_reaction_idx_slow();
                }
            }
        }

        let rand_val = self.rng.gen_range(0.0, 1.);
        self.select_from_threshold(false, rand_val)
    }

    pub fn seed(&mut self) {
        self.rng = SmallRng::from_entropy();
    }
    /*fn write_header(&self) {
        let mut file = match File::create(&self.output_file_name) {
            Err(why) => panic!("couldn't open {}: {}", self.output_file_name, why),
            Ok(file) => file,
        };
        write!(file, "Time,").unwrap();
        match &self.model.observations {
            None => self
                .model
                .species
                .iter()
                .for_each(|v| write!(file, "{},", v.string_name()).unwrap()),
            Some(obs) => obs
                .iter()
                .for_each(|(v, _)| write!(file, "{},", v).unwrap()),
        }
        self.model
            .global_values
            .iter()
            .for_each(|(v, _)| write!(file, "{},", v.string_name()).unwrap())
    }*/
    pub fn make_observations(&mut self) {
        self.obs_times.push(self.time);
        /*let mut file = match OpenOptions::new().append(true).open(&self.output_file_name) {
            Err(why) => panic!("couldn't open {}: {}", self.output_file_name, why),
            Ok(file) => file,
        };
        write!(file, "\n{},", *time).unwrap();*/
        self.observation_values.push(
            /*match &self.model.model.observations {
            None => */
            self.species_counts
                .iter()
                .map(|v| PrimitiveTypes::Integer(*v as isize))
                .chain(
                    iter::repeat(PrimitiveTypes::Integer(0)).take(self.dead_observations_to_keep),
                )
                .chain(self.global_values.iter().cloned())
                .collect(),
            /*  Some(obs) => obs
                    .iter()
                    .map(|(_, s)| s.iter().map(|v| spec_counts.get(v).unwrap()).sum())
                    .collect()
            }*/
        );
        assert_eq!(
            self.observation_values.last().unwrap().len(),
            self.observation_names.len()
        );

        {
            let obs = &mut self.obs_reaction_counts;
            let counts = &self.reaction_counts;
            counts.iter().enumerate().for_each(|(n, v)| obs[n].push(*v));
        }
        /*self.model
        .global_values
        .iter()
        .for_each(|(v, _)| write!(file, "{},", global_values.get(v).unwrap()).unwrap())*/
    }

    /*fn get_propensities(
        &self,
        spec_counts: &HashMap<EnumeratedSpecies, usize>,
        global_vals: &HashMap<EnumeratedGlobalValue, PrimitiveTypes>,
        time: f64,
    ) -> Vec<f64> {
        self.model
            .reactions
            .iter()
            .map(|v| v.calc_propensity(spec_counts, global_vals, time))
            .collect()
    }*/

    fn find_dependent_reactions(&mut self) {
        let deps: Vec<_> = self.reactions.iter().map(|v| v.get_involved()).collect();

        let mut species_modif_rules = vec![Vec::new(); self.species_counts.len()];
        let mut globals_modif_rules = vec![Vec::new(); self.global_values.len()];
        for (n, d) in deps.iter().enumerate() {
            let n = to_uindex(&n);
            d.rate_globals.iter().for_each(|v| {
                globals_modif_rules[*v as usize].push(n);
            });
            d.rate_species.iter().for_each(|v| {
                species_modif_rules[*v as usize].push(n);
            });
            if d.time_dependend {
                self.time_dependend_reactions.push(n);
            }
        }
        species_modif_rules.iter_mut().for_each(|k| {
            k.sort();
            let old_len = k.len();
            k.dedup();
            assert_eq!(old_len, k.len())
        });
        globals_modif_rules.iter_mut().for_each(|k| {
            k.sort();
            let old_len = k.len();
            k.dedup();
            assert_eq!(old_len, k.len())
        });

        let mut dep = Vec::new();
        for i in 0..self.reactions.len() {
            dep.clear();
            dep.extend(
                deps[i]
                    .changing_species
                    .iter()
                    .map(|v| species_modif_rules[*v as usize].iter())
                    .flatten(),
            );
            dep.extend(
                deps[i]
                    .changing_globals
                    .iter()
                    .map(|v| globals_modif_rules[*v].iter())
                    .flatten(),
            );
            dep.sort();
            let old_len = dep.len();
            dep.dedup();
            //assert_eq!(old_len, dep.len());
            self.reactions[i].dependend_reactions = dep.iter().copied().collect();
            self.reactions[i].shrink();
        }
    }

    pub fn new(m: &EnumaratedModel) -> SimulationFast {
        let species_lookup: HashMap<EnumeratedSpecies, usize> = m
            .species
            .iter()
            .enumerate()
            .map(|(a, b)| (b.clone(), a))
            .collect();

        let mut species_counts = vec![0; species_lookup.len()];

        for (s, count) in m.initial_species.iter() {
            //println!("I'm loking for {}",s.string_name());
            species_counts[*species_lookup
                .get(s)
                .expect("A species you told me existed (because of amount) doesn't! ")] = *count;
        }

        let global_value_lookup: HashMap<EnumeratedGlobalValue, usize, RandomState> = m
            .global_values
            .iter()
            .enumerate()
            .map(|(n, (v, _k))| (v.clone(), n))
            .collect();

        let mut r = SimulationFast {
            count_for_next_sort: std::cmp::max(m.reactions.len() / 3, 100),
            reactions: m
                .reactions
                .iter()
                .map(|v| EnumeratedIndexedReaction::from(v, &species_lookup, &global_value_lookup))
                .collect(),
            species_counts: species_counts,
            species_lookup,
            observation_values: vec![],
            obs_reaction_counts: vec![vec![]; m.model.transitions.len()],
            dead_observations_to_keep: 0,
            observation_names: m
                .species
                .iter()
                .map(|s| s.string_name())
                .chain(m.global_values.iter().map(|(a, _)| a.string_name()))
                .collect(),
            reaction_names: m
                .model
                .transitions
                .iter()
                .map(|t| t.original_string.clone())
                .collect(),
            reaction_counts: vec![0; m.model.transitions.len()],
            global_values: m.global_values.iter().map(|(_v, a)| a.clone()).collect(),
            obs_global_vals_names: m
                .global_values
                .iter()
                .map(|(a, _)| a.string_name())
                .collect(),
            time_dependend_reactions: vec![],
            time: 0.0,
            propensities: vec![0.; m.reactions.len()],
            propensity_sum: 0.0,
            propensitiy_tree: vec![0.; m.reactions.len()],
            stepcount: 0,
            rng: SmallRng::from_rng(thread_rng()).unwrap(),
            last_io: 0.0,
            obs_times: vec![],
            //model: m,
        };
        r.find_dependent_reactions();
        r.update_all_propensities();
        r
    }

    fn eval_expr(&self, expr: &IndexedRateExpr) -> PrimitiveTypes {
        match &expr.op {
            None => self.eval_term(&expr.left),
            Some((op, right)) => {
                let right = self.eval_term(&right);
                let left = self.eval_term(&expr.left);
                op.apply(&left, &right).unwrap()
            }
        }
    }
    fn eval_term(&self, expr: &IndexedRateExprTerm) -> PrimitiveTypes {
        match expr {
            IndexedRateExprTerm::Primitive(p) => p.clone(),
            IndexedRateExprTerm::GlobAttRef(y) => self.global_values[*y].clone(),
            IndexedRateExprTerm::Expr(x) => self.eval_expr(x),
            IndexedRateExprTerm::GlobSpecCount(idx) => {
                PrimitiveTypes::Integer(self.species_counts[*idx] as isize)
            }
            IndexedRateExprTerm::SpecificCount(idices) => PrimitiveTypes::Integer(
                idices
                    .iter()
                    .map(|v| self.species_counts[*v] as isize)
                    .sum(),
            ),
            IndexedRateExprTerm::GlobalTime() => PrimitiveTypes::Float(self.time),
        }
    }

    fn eval_guard(&self, guard: &IndexedLogicalConstraint) -> bool {
        let l = self.eval_expr(&guard.left);
        let r = self.eval_expr(&guard.right);

        match (l, r) {
            (PrimitiveTypes::Integer(l), PrimitiveTypes::Integer(r)) => guard.op.apply_all(&l, &r),
            (PrimitiveTypes::Float(l), PrimitiveTypes::Float(r)) => guard.op.apply_all(&l, &r),
            (PrimitiveTypes::Float(l), PrimitiveTypes::Integer(r)) => {
                guard.op.apply_all(&l, &(r as f64))
            }
            (PrimitiveTypes::Integer(l), PrimitiveTypes::Float(r)) => {
                guard.op.apply_all(&(l as f64), &r)
            }
            (PrimitiveTypes::Boolean(l), PrimitiveTypes::Boolean(r)) => guard.op.apply_eq(&l, &r),
            (PrimitiveTypes::String(l), PrimitiveTypes::String(r)) => guard.op.apply_eq(&l, &r),
            _ => unreachable!(),
        }
    }

    fn get_propensity_of(&self, idx: Uindex) -> f64 {
        let idx = idx as usize;
        let reac = &self.reactions[idx];
        if reac.guards.iter().any(|g| !self.eval_guard(g)) {
            return 0.0;
        }

        let input_species_faktor = reac
            .species_in_idx
            .iter()
            .zip(reac.species_in_amt.iter())
            .map(|(idx, amt)| {
                let idx = *idx as usize;
                if self.species_counts[idx] >= *amt as usize {
                    match amt {
                        0 => unreachable!("Amt can not be 0"),
                        1 => (self.species_counts[idx]),
                        x => (0..*x)
                            .into_iter()
                            .map(|x| self.species_counts[idx] - x as usize)
                            .fold(1, |a, b| (a * b)),
                    }
                } else {
                    0
                }
            })
            .fold(1, |a, b| a * b);

        let ret = match &reac.rate {
            IndexedEnumeratedRate::MassActionConstant(k) => k * input_species_faktor as f64,
            IndexedEnumeratedRate::MassActionExpression(expr) => {
                self.eval_expr(expr).as_float().unwrap() * input_species_faktor as f64
            }
            IndexedEnumeratedRate::ComplexExpression(expr) => {
                if input_species_faktor == 0 {
                    return 0.;
                } else {
                    self.eval_expr(expr).as_float().unwrap()
                }
            }
        };
        if !ret.is_finite() {
            panic!("Rate is not a normal number!!!!\n This is a model error!\nRULE: {}\nCalculated propensity:{}\n\n",self.reaction_names[reac.reaction_number_of_prototype],ret);
            //return 0.;
        }
        if ret < 0. {
            println!(
                "Negative Rate!!!! This is a model error!\nRULE: {}\nCalculated propensity:{}\n\n",
                self.reaction_names[reac.reaction_number_of_prototype], ret
            );
            return 0.;
        }
        ret
    }
    fn update_all_propensities_time_dep(&mut self) {
        let mut tmp = vec![];
        std::mem::swap(&mut tmp, &mut self.time_dependend_reactions);
        for i in tmp.iter() {
            self.set_propensity(*i, self.get_propensity_of(*i));
        }
        std::mem::swap(&mut tmp, &mut self.time_dependend_reactions);
    }
    fn update_all_propensities(&mut self) {
        for i in 0..to_uindex(&self.reactions.len()) {
            self.set_propensity(i, self.get_propensity_of(i));
        }
        self.propensity_sum = self.propensities.iter().sum();
        self.rebuild_tree();
    }

    fn sort_reactions(&mut self) {
        let mut all_reacs = vec![
            EnumeratedIndexedReaction {
                species_in_idx: vec![],
                species_in_amt: vec![],
                species_out: vec![],
                reaction_number_of_prototype: 0,
                invokes_structural_change: false,
                guards: vec![],
                global_val_changes: vec![],
                rate: IndexedEnumeratedRate::MassActionConstant(-1.),
                dependend_reactions: vec![],
                count: 0
            };
            self.reactions.len()
        ];
        swap(&mut self.reactions, &mut all_reacs);

        let mut histos: Vec<usize> = (0..all_reacs.len()).collect();
        histos.sort_unstable_by_key(|idx| std::cmp::Reverse(all_reacs[*idx].count));

        let mut indices = vec![0; histos.len()];
        for i in histos.iter() {
            indices[histos[*i]] = *i;
        }
        all_reacs.iter_mut().for_each(|reac| {
            reac.dependend_reactions
                .iter_mut()
                .for_each(|i| *i = to_uindex(&indices[*i as usize]))
        });
        /*for i in indices.iter() {
            print!("{}->{},", i, all_reacs[*i].count);
        }
        println!();*/

        for (i, reac) in indices.into_iter().zip(all_reacs.into_iter()) {
            self.reactions[i] = reac;
        }
        self.update_all_propensities();
        //self.debug_print_counts();
    }

    fn fire_reaction(&mut self, idx_of_reaction: usize) -> FireResult {
        self.reaction_counts[self.reactions[idx_of_reaction].reaction_number_of_prototype] += 1;
        self.reactions[idx_of_reaction].count += 1;
        let reac = &self.reactions[idx_of_reaction];

        // if we have a structural change: Abort execution and don't change system
        if self.reactions[idx_of_reaction].invokes_structural_change {
            return FireResult::InvokeStructureChange;
        }

        let counts = &mut self.species_counts;
        reac.species_in_idx
            .iter()
            .zip(reac.species_in_amt.iter())
            .for_each(|(idx, amt)| counts[*idx as usize] -= *amt as usize);
        reac.species_out
            .iter()
            .for_each(|v| counts[*v as usize] += 1);

        let changes: Vec<_> = self.reactions[idx_of_reaction]
            .global_val_changes
            .iter()
            .map(|(idx, c)| (idx, self.eval_term(c)))
            .collect();
        for (idx, val) in changes {
            self.global_values[*idx] = val;
        }

        //self.update_all_propensities();
        let mut tmp = vec![];
        std::mem::swap(
            &mut tmp,
            &mut self.reactions[idx_of_reaction].dependend_reactions,
        );
        for idx in tmp.iter() {
            self.set_propensity(*idx, self.get_propensity_of(*idx));
        }
        std::mem::swap(
            &mut tmp,
            &mut self.reactions[idx_of_reaction].dependend_reactions,
        );

        FireResult::Good
    }

    pub fn debug_print_counts(&self) {
        for r in self.reactions.iter() {
            print!("{},", r.count);
        }
        println!();
    }

    pub fn step(&mut self, step_size: f64) -> StepResult {
        if self.stepcount <= 0 {
            self.update_all_propensities();
        }
        self.stepcount += 1;

        if self.stepcount == self.count_for_next_sort {
            self.sort_reactions();
            self.count_for_next_sort *= 2;
        }

        /*if (self.stepcount % std::cmp::min(1000, self.reactions.len()) == 0)
            || self.propensity_sum < 0.
        {
            self.propensity_sum = self.propensities.iter().sum();
        }*/

        let check_all_values: bool = false;
        if check_all_values {
            let old_propensities = self.propensities.clone();
            let old_propensity_sum = self.propensity_sum;
            self.update_all_propensities();
            for (old, new) in old_propensities.iter().zip(self.propensities.iter()) {
                if (*old < *new * 0.9999) || (*old > *new * 1.0001) {
                    panic!(
                        "Propensity deviates as {} != {}, at t={}, step {}",
                        old, new, self.time, self.stepcount
                    );
                }
            }
            if (old_propensity_sum >= 1.00001 * self.propensity_sum)
                || (old_propensity_sum < 0.99999 * self.propensity_sum)
            {
                panic!(
                    "Propensity sum deviation! {} != {}",
                    old_propensity_sum, self.propensity_sum
                );
            }
        }
        if self.propensity_sum <= 0.0 {
            self.update_all_propensities();
        }

        if self.propensity_sum <= 0.0 {
            //println!("No reaction possible!");
            //self.write_output(&time, &species_counts, &global_attribute_values);
            self.last_io += step_size;
            self.time = self.last_io + step_size * 1e-8;
            self.update_all_propensities_time_dep();
            self.make_observations();
            return StepResult::NoReactionPossible;
        }

        let distr = rand_distr::Exp::new(self.propensity_sum).unwrap();
        let timestep = distr.sample(&mut self.rng);

        if (self.time + timestep > self.last_io + step_size) || (self.stepcount == 1) {
            self.last_io += step_size;
            self.time = self.last_io + step_size * 1e-8;
            self.update_all_propensities_time_dep();
            self.make_observations();
            return StepResult::OnlyObservation;
        }
        self.time += timestep;
        assert!(self.propensity_sum > 0.);

        let idx_of_reaction = match self.get_next_reaction_idx() {
            None => return StepResult::NoReactionPossible,
            Some(y) => y,
        };

        match self.fire_reaction(idx_of_reaction) {
            FireResult::Good => StepResult::AllGood,
            FireResult::InvokeStructureChange => StepResult::StructuralChange { idx_of_reaction },
        }
    }
}

enum FireResult {
    Good,
    InvokeStructureChange,
}

pub enum StepResult {
    AllGood,
    NoReactionPossible,
    StructuralChange { idx_of_reaction: usize },
    OnlyObservation,
}
