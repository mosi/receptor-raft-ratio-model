use itertools::{enumerate, Itertools};
use num_rational::Rational64;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::fmt;
use std::fmt::{Display, Formatter};

use crate::flatten::EnumeratedSpecies;
use sha2::digest::generic_array::GenericArray;
use sha2::{Digest, Sha256};
use std::hash::{Hash, Hasher};

#[derive(Clone, Hash, Eq, PartialEq)]
pub struct LanguageError {
    pub lstart: usize,
    pub lstop: usize,
    pub cstart: usize,
    pub cstop: usize,
    pub what: String,
    pub line: u32,
    pub file: String,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum LogicalOp {
    Equal,
    Less,
    Greater,
    LessOrEqual,
    GreaterOrEqual,
    Unequal,
}

impl fmt::Display for LogicalOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            LogicalOp::Equal => write!(f, "=="),
            LogicalOp::Less => write!(f, "<"),
            LogicalOp::Greater => write!(f, ">"),
            LogicalOp::LessOrEqual => write!(f, "<="),
            LogicalOp::GreaterOrEqual => write!(f, ">="),
            LogicalOp::Unequal => write!(f, "!="),
        }
    }
}

impl LogicalOp {
    pub fn dim_ok(&self, a: &Dimension, b: &Dimension) -> bool {
        if !Dimension::numericals_match(b, b) {
            return false;
        }
        match (&a.non_numerical, &b.non_numerical) {
            (NonNumericalDimension::Integer, NonNumericalDimension::Integer) => true,
            (NonNumericalDimension::Nothing, NonNumericalDimension::Integer) => true,
            (NonNumericalDimension::Integer, NonNumericalDimension::Nothing) => true,
            (NonNumericalDimension::Nothing, NonNumericalDimension::Nothing) => true,
            (_, _) => match self {
                LogicalOp::Equal => true,
                LogicalOp::Less => false,
                LogicalOp::Greater => false,
                LogicalOp::LessOrEqual => false,
                LogicalOp::GreaterOrEqual => false,
                LogicalOp::Unequal => true,
            },
        }
    }
    pub fn apply_eq<T: Eq>(&self, a: &T, b: &T) -> bool {
        match self {
            LogicalOp::Equal => a == b,
            LogicalOp::Unequal => a != b,
            _ => unreachable!(),
        }
    }
    pub fn apply_all<T: PartialEq + PartialOrd>(&self, a: &T, b: &T) -> bool {
        match self {
            LogicalOp::Equal => a == b,
            LogicalOp::Unequal => a != b,
            LogicalOp::Less => a < b,
            LogicalOp::Greater => a > b,
            LogicalOp::LessOrEqual => a <= b,
            LogicalOp::GreaterOrEqual => a >= b,
        }
    }
}
impl LogicalOp {
    pub fn from_str(s: &str) -> Option<LogicalOp> {
        match s {
            "==" => Some(LogicalOp::Equal),
            "<" => Some(LogicalOp::Less),
            ">" => Some(LogicalOp::Greater),
            "<=" => Some(LogicalOp::LessOrEqual),
            ">=" => Some(LogicalOp::GreaterOrEqual),
            "!=" => Some(LogicalOp::Unequal),
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum FloatOp {
    Add,
    Sub,
    Div,
    Mul,
    Pow,
}

impl FloatOp {
    pub fn combine_dimension(&self, a: &Dimension, b: &Dimension) -> Option<Dimension> {
        match &a.non_numerical {
            NonNumericalDimension::Nothing => (),
            NonNumericalDimension::String => return None,
            NonNumericalDimension::Bool => return None,
            NonNumericalDimension::Integer => (),
        }
        match &b.non_numerical {
            NonNumericalDimension::Nothing => {}
            NonNumericalDimension::String => return None,
            NonNumericalDimension::Bool => return None,
            NonNumericalDimension::Integer => {}
        }

        let new_non_num_dim = match (&a.non_numerical, &b.non_numerical) {
            (NonNumericalDimension::Integer, NonNumericalDimension::Integer) => {
                NonNumericalDimension::Integer
            }
            _ => NonNumericalDimension::Nothing,
        };

        match self {
            FloatOp::Add | FloatOp::Sub => match Dimension::numericals_match(a, b) {
                true => {
                    let mut tmp = a.clone();
                    tmp.non_numerical = new_non_num_dim;
                    Some(tmp)
                }
                false => None,
            },
            FloatOp::Div => Some(Dimension {
                length: a.length - b.length,
                time: a.time - b.time,
                weight: a.weight - b.weight,
                current: a.current - b.current,
                temperature: a.temperature - b.temperature,
                amount: a.amount - b.amount,

                non_numerical: NonNumericalDimension::Nothing,
            }),
            FloatOp::Mul => Some(Dimension {
                length: a.length + b.length,
                time: a.time + b.time,
                weight: a.weight + b.weight,
                current: a.current + b.current,
                temperature: a.temperature + b.temperature,
                amount: a.amount + b.amount,

                non_numerical: new_non_num_dim,
            }),
            FloatOp::Pow => match b.non_numerical {
                NonNumericalDimension::Nothing => {
                    if *a == Dimension::dimensionless() && *b == Dimension::dimensionless() {
                        Some(Dimension::dimensionless())
                    } else {
                        None
                    }
                }
                NonNumericalDimension::String => None,
                NonNumericalDimension::Bool => None,
                NonNumericalDimension::Integer => None, //Schould work, is missing implementation
            },
        }
    }
    pub fn apply(&self, a: &PrimitiveTypes, b: &PrimitiveTypes) -> Option<PrimitiveTypes> {
        match a {
            PrimitiveTypes::Float(a) => match b {
                PrimitiveTypes::Float(b) => Some(PrimitiveTypes::Float(self.apply_float(*a, *b))),
                PrimitiveTypes::Integer(b) => {
                    Some(PrimitiveTypes::Float(self.apply_float(*a, *b as f64)))
                }

                PrimitiveTypes::String(_) => None,
                PrimitiveTypes::Boolean(_) => None,
            },
            PrimitiveTypes::Integer(a) => match b {
                PrimitiveTypes::Float(b) => {
                    Some(PrimitiveTypes::Float(self.apply_float(*a as f64, *b)))
                }
                PrimitiveTypes::Integer(b) => Some(PrimitiveTypes::Integer(self.apply_int(*a, *b))),

                PrimitiveTypes::String(_) => None,
                PrimitiveTypes::Boolean(_) => None,
            },

            PrimitiveTypes::String(_) => None,
            PrimitiveTypes::Boolean(_) => None,
        }
    }
    pub fn apply_int(&self, a: isize, b: isize) -> isize {
        match self {
            FloatOp::Add => a + b,
            FloatOp::Sub => a - b,
            FloatOp::Div => a / b,
            FloatOp::Mul => a * b,

            FloatOp::Pow => {
                if b >= 0 {
                    a.pow(b as u32)
                } else {
                    panic!("Negative integer power not implemented!")
                }
            }
        }
    }
    pub fn apply_float(&self, a: f64, b: f64) -> f64 {
        match self {
            FloatOp::Add => a + b,
            FloatOp::Sub => a - b,
            FloatOp::Div => a / b,
            FloatOp::Mul => a * b,
            FloatOp::Pow => a.powf(b),
        }
    }
}

impl std::fmt::Display for FloatOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            FloatOp::Add => write!(f, "+"),
            FloatOp::Sub => write!(f, "-"),
            FloatOp::Div => write!(f, "/"),
            FloatOp::Mul => write!(f, "*"),
            FloatOp::Pow => write!(f, "^"),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct AttributeRef {
    pub species_id: Option<SpeciesLeftIdentifier>,
    pub att_name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SpeciesCount {
    pub idx_on_left: SpeciesLeftIdentifier,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum PrimitiveTypes {
    String(String),
    Boolean(bool),
    Float(f64),
    Integer(isize),
}

impl Hash for PrimitiveTypes {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            PrimitiveTypes::String(v) => v.hash(state),
            PrimitiveTypes::Boolean(v) => v.hash(state),
            PrimitiveTypes::Integer(v) => v.hash(state),
            PrimitiveTypes::Float(v) => Rational64::approximate_float(*v).hash(state),
        }
    }
}

impl PrimitiveTypes {
    pub fn as_float(self) -> Option<f64> {
        match self {
            PrimitiveTypes::Float(f) => Some(f),
            PrimitiveTypes::Integer(i) => Some(i as f64),
            _ => None,
        }
    }
}

impl Display for PrimitiveTypes {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            PrimitiveTypes::String(s) => write!(f, "{}", s),
            PrimitiveTypes::Boolean(s) => write!(f, "{}", s),
            PrimitiveTypes::Float(s) => write!(f, "{}", s.to_string().replace(".", "d")),
            PrimitiveTypes::Integer(s) => write!(f, "{}", s),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum RateExprTerm {
    Primitive(PrimitiveTypes),
    Parameter(String),
    AttRef(AttributeRef),
    GlobAttRef(AttributeRef),
    Expr(Box<RateExpr>),
    SpecCount(SpeciesCount),
    GlobSpecCount(GlobalCount),
    SpecificCount(Vec<EnumeratedSpecies>),
    GlobalTime(),
}

/*impl PartialEq for PrimitiveTypes {
    fn eq(&self, other: &Self) -> bool {
        unimplemented!()
        /*match (self,other){
            (RateExprTerm::Num(a),RateExprTerm::Num(b)) => {
                approx_eq!(f64,a,b,ulps=10)
            }
            (RateExprTerm::Integer(a),RateExprTerm::Num(b)) => {
                approx_eq!(f64,a as f64,b,ulps=10)
            }
            (RateExprTerm::Num(a),RateExprTerm::Integer(b)) => {
                approx_eq!(f64,a,b as f64,ulps=10)
            }


        }*/
    }
}*/
impl Eq for PrimitiveTypes {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RateExpr {
    pub left: RateExprTerm,
    pub op: Option<(FloatOp, RateExprTerm)>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Species {
    pub is_compartment: bool,
    pub name: String,
    pub attributes: HashMap<String, Dimension>,
    pub global_attributes: HashMap<String, Dimension>,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize, Ord, PartialOrd)]
pub struct SpeciesLeftIdentifier {
    //name: String,
    //name_hash: u64,
    name_hash: u128,
}

impl SpeciesLeftIdentifier {
    pub(crate) fn new(name: &str) -> SpeciesLeftIdentifier {
        let mut hasher = Sha256::new(); //DefaultHasher::new();
                                        //name.hash(&mut hasher);
        Digest::update(&mut hasher, name);
        let hash_val: GenericArray<u8, <Sha256 as Digest>::OutputSize> = hasher.finalize();

        let hash_num = enumerate(hash_val.into_iter().take(16))
            .map(|(n, v)| (v as u128) << (n * 8))
            .sum();

        /*if !name.contains("DUMMY") {
            println!(
                "HASH: {}\t{}\t {}",
                name,
                hash_num,
                hash_num as f64 / u128::MAX as f64
            );
        }*/

        SpeciesLeftIdentifier {
            name_hash: hash_num//hash_val.into(),
            //name: name.to_string(),
        }
    }

    pub fn new_dummy() -> SpeciesLeftIdentifier {
        SpeciesLeftIdentifier { name_hash: 0 }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SpeciesLeft {
    pub species_name: String,
    pub id: SpeciesLeftIdentifier,
    //pub extra_name: Option<String>,

    //pub number_in_match: isize,
    pub parent_number: Option<SpeciesLeftIdentifier>, // number of the species on the left side that is my parent (-1 means not assigned)
    pub attribute_constraint: Vec<LogicalConstraint>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SpeciesRight {
    pub species_name: String,
    pub extra_name: Option<String>,
    pub compartment_constraint: isize, // number of the species on the right side that is my parent (-1 means not assigned)
    pub corresponding_on_left: Option<SpeciesLeftIdentifier>,
    pub local_attribute_assignment: Vec<(String, RateExpr)>,
    pub global_attribute_assignment: Vec<(String, RateExpr)>,
    pub amount: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LogicalConstraint {
    pub left: RateExpr,
    pub op: LogicalOp,
    pub right: RateExpr,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum RestSolutionRightTarget {
    ReactionLevel,
    OtherCompartment(SpeciesRight),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum StructuralChange {
    RemoveCompartment(SpeciesLeftIdentifier),
    RemoveRestSolution(RestSolutionLeft),
    MoveRestSolutionTo {
        old: RestSolutionLeft,
        new: RestSolutionRightTarget,
    },
    OtherUnimplemented,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RestSolutionTransition {
    pub(crate) leftsideparent: SpeciesLeftIdentifier,
    pub(crate) idx_right: Option<isize>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Transition {
    pub left: Vec<SpeciesLeft>,
    pub right: Vec<SpeciesRight>,
    pub rate_expr: RateExpr,
    pub guard: Vec<LogicalConstraint>,
    pub is_not_mass_action: bool,
    pub original_string: String,
    pub structural_changes: Vec<StructuralChange>,
    pub rest_solution_transiotions: Vec<RestSolutionTransition>,
}

impl Transition {
    pub fn get_left_by_id(&self, id: &SpeciesLeftIdentifier) -> Option<&SpeciesLeft> {
        self.left.iter().find(|v| v.id == *id)
    }
    pub fn use_names_for_left_right_correspondence(&mut self) {
        let named_right: Vec<_> = self
            .right
            .iter()
            .filter(|v| v.extra_name.is_some())
            .map(|v| v.extra_name.as_ref().clone().unwrap())
            .collect();
        let named_right_set: HashSet<_> = named_right.iter().cloned().collect();
        assert_eq!(
            named_right.len(),
            named_right_set.len(),
            "Double use of :name on right site"
        );

        let count_left = self.left.clone();

        self.right
            .iter_mut()
            .filter(|v| v.extra_name.is_some())
            .for_each(|v| {
                let n = v.extra_name.as_ref().clone().unwrap();
                match count_left
                    .iter()
                    .filter(|l| l.id == SpeciesLeftIdentifier::new(n))
                    .count()
                {
                    0 => panic!("Name {} not used on left side", n),
                    1 => {
                        v.corresponding_on_left = Some(
                            count_left
                                .iter()
                                .find(|l| l.id == SpeciesLeftIdentifier::new(n))
                                .unwrap()
                                .id
                                .clone(),
                        )
                    }
                    x => panic!("Name {} used multiple times ({}x) on left side", x, n),
                }
            });
    }

    pub fn find_left_right_correspondence(&mut self) {
        let count_right = self.right.clone();
        let count_left = self.left.clone();
        self.right
            .iter_mut()
            .filter(|r| r.corresponding_on_left.is_none())
            .filter(|s| {
                count_right
                    .iter()
                    .map(|v| &v.species_name)
                    .filter(|name| **name == s.species_name)
                    .count()
                    == 1
            })
            .filter(|s_right| {
                count_left
                    .iter()
                    .filter(|sl| sl.species_name == s_right.species_name)
                    .count()
                    == 1
            })
            .for_each(|s_right| {
                s_right.corresponding_on_left = Some(
                    count_left
                        .iter()
                        .find(|l| l.species_name == s_right.species_name)
                        .unwrap()
                        .id
                        .clone(),
                )
            });
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum CountingElement {
    _SpecificName(usize),
    GeneralDescription(String, Vec<LogicalConstraint>),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct GlobalCount {
    pub(crate) definition: Vec<CountingElement>,
    pub(crate) name: String,
}

#[derive(Serialize, Deserialize)]
pub struct Settings {
    pub(crate) endtime: Option<f64>,
    pub num_reps: Option<usize>,
}

#[derive(Serialize, Deserialize)]
pub struct Model {
    pub species: HashMap<String, Species>,
    pub transitions: Vec<Transition>,
    pub inital: Vec<SpeciesRight>,
    pub observations: Option<Vec<GlobalCount>>,
    pub parameters: HashMap<String, RateExpr>,
    pub settings: Settings,
}

impl Model {
    /* species that occur exactly once on both sides, can automatically transfer attributes. For this we need to find the corresponding species as done in this function*/

    pub fn _check_left_right_correspondence_for_compartments(&self) {
        let compartments: Vec<_> = self
            .species
            .values()
            .filter(|s| s.is_compartment)
            .cloned()
            .collect();
        compartments.into_iter().for_each(|c| {
            self.transitions.iter().for_each(| t| {
                let count_left = t.left.iter().filter(|v| v.species_name == c.name).count();
                let count_right = t.right.iter().filter(|v| v.species_name == c.name).count();
                if count_left > 1 || count_right > 1 || count_left != count_right {
                    panic!("Compartmeant {} occurs {} on left and {} on right, but it has to be exatly once on both sides",c.name,count_left,count_right);
                }
                if count_right == 1 {
                    let _pos_left = t.left.iter().find_position(|v|v.species_name==c.name).unwrap();
                    assert!(t.right.iter().find(|v|v.species_name==c.name).unwrap().corresponding_on_left.is_some())
                    //t.right.iter_mut().find(|v|v.species_name==c.name).unwrap().corresponding_on_left = Some(pos_left.0);
                }
            })
        });
    }

    pub fn verify_spicies(&self) -> Result<(), String> {
        for (name, spec) in self.species.iter() {
            if *name != spec.name {
                panic!("Illformed species! {} != {}", name, spec.name);
            }
        }
        for s in self.inital.iter() {
            self.species.get(&s.species_name).expect(stringify!(
                "Species Name (in initializiation) {} does not exist",
                s.species_name
            ));
            let testset: HashSet<String> = s
                .local_attribute_assignment
                .iter()
                .map(|(n, _v)| n)
                .cloned()
                .collect();
            if !(s.corresponding_on_left.is_none()) {
                return Err("There is a strange left correspondence here!".to_string());
            }

            if testset
                != self
                    .species
                    .get(&s.species_name)
                    .unwrap()
                    .attributes
                    .keys()
                    .cloned()
                    .collect()
            {
                return Err(format!(
                    "Not all attributes are defined for initialization, Species: {:?}",
                    self.species.get(&s.species_name).unwrap().attributes
                )
                .to_string());
            }
            let test_set2: HashSet<String> = s
                .global_attribute_assignment
                .iter()
                .map(|(v, _)| v)
                .cloned()
                .collect();
            if test_set2.len() != s.global_attribute_assignment.len() {
                return Err("Duplicate star attribute assignment".to_string());
            }
            if test_set2
                != self
                    .species
                    .get(&s.species_name)
                    .unwrap()
                    .global_attributes
                    .keys()
                    .cloned()
                    .collect()
            {
                return Err(format!(
                    "Not all A attributes are defined for initialization, Species: {:?}",
                    self.species.get(&s.species_name).unwrap().attributes
                ));
            }
        }
        for t in self.transitions.iter() {
            for s in t.left.iter() {
                self.species
                    .get(&s.species_name)
                    .expect(stringify!("Species Name {} does not exist", s.species_name));
            }
            for s in t.right.iter() {
                self.species
                    .get(&s.species_name)
                    .expect(stringify!("Species Name {} does not exist", s.species_name));
                let defined: HashSet<String> = s
                    .local_attribute_assignment
                    .iter()
                    .map(|(n, _v)| n)
                    .cloned()
                    .collect();
                if s.corresponding_on_left.is_none() {
                    if defined
                        != self
                            .species
                            .get(&s.species_name)
                            .unwrap()
                            .attributes
                            .keys()
                            .cloned()
                            .collect()
                    {
                        let needed: HashSet<_> = self
                            .species
                            .get(&s.species_name)
                            .unwrap()
                            .attributes
                            .keys()
                            .cloned()
                            .collect();
                        return Err(format!("There is an error in the model!\nNot all needed attributes are defined\nIn this reaction: '{}'\nMissing: {:?}",t.original_string,needed.difference(&defined)));
                    }
                }
            }
        }
        return Ok(());
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Hash, Serialize, Deserialize)]
pub struct RestSolutionLeft {
    pub name: String,
    pub parent_id: SpeciesLeftIdentifier,
}

pub struct RestSolutionRight {
    pub name: String,
    pub compartment_constraint: isize, // number of the species on the right side that is my parent (-1 means not assigned)
}

#[derive(Debug, Eq, PartialEq, Clone, Hash, Serialize, Deserialize)]
pub enum NonNumericalDimension {
    Nothing,
    String,
    Bool,
    Integer,
}

#[derive(Eq, PartialEq, Debug, Clone, Hash, Serialize, Deserialize)]
pub struct Dimension {
    pub length: isize,
    pub time: isize,
    pub weight: isize,
    pub current: isize,
    pub temperature: isize,
    pub amount: isize,

    pub non_numerical: NonNumericalDimension,
}

impl Dimension {
    pub fn _power(&self, p: isize) -> Option<Dimension> {
        match self.non_numerical {
            NonNumericalDimension::Nothing | NonNumericalDimension::Integer => Some(Dimension {
                length: self.length * p,
                time: self.time * p,
                weight: self.weight * p,
                current: self.current * p,
                temperature: self.temperature * p,
                amount: self.amount * p,
                non_numerical: self.non_numerical.clone(),
            }),
            NonNumericalDimension::String | NonNumericalDimension::Bool => None,
        }
    }
    pub fn assignable(to: &Dimension, from: &Dimension) -> bool {
        if !Dimension::numericals_match(to, from) {
            return false;
        }
        if to.non_numerical == from.non_numerical {
            return true;
        }
        if to.non_numerical == NonNumericalDimension::Nothing
            && from.non_numerical == NonNumericalDimension::Integer
        {
            return true;
        }
        return false;
    }
    fn numericals_match(a: &Dimension, b: &Dimension) -> bool {
        a.length == b.length
            && a.time == b.time
            && a.weight == b.weight
            && a.current == b.current
            && a.temperature == b.temperature
            && a.amount == b.amount
    }
    pub const fn dimensionless() -> Dimension {
        Dimension {
            length: 0,
            time: 0,
            weight: 0,
            current: 0,
            temperature: 0,
            amount: 0,
            non_numerical: NonNumericalDimension::Nothing,
        }
    }
    pub fn non_numeric(nn: NonNumericalDimension) -> Dimension {
        let mut k = Dimension::dimensionless();
        k.non_numerical = nn;
        k
    }

    fn exp_if_not_1(v: isize) -> String {
        match v {
            0 => "".to_string(),
            x => format!("^{}", x).to_string(),
        }
    }
}
impl fmt::Display for Dimension {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.non_numerical {
            NonNumericalDimension::Nothing => {}
            NonNumericalDimension::String => write!(f, "<STRING>")?,
            NonNumericalDimension::Bool => write!(f, "<BOOL>")?,
            NonNumericalDimension::Integer => write!(f, "<INTEGER>")?,
        }
        if self.length == 0 && self.weight == 0 && self.time == 0 {
            write!(f, "{{}}")
        } else {
            write!(f, "{{")?;
            if self.length != 0 {
                write!(f, "length{} ", Dimension::exp_if_not_1(self.length))?;
            }
            if self.weight != 0 {
                write!(f, "weight{} ", Dimension::exp_if_not_1(self.weight))?;
            }
            if self.time != 0 {
                write!(f, "time{} ", Dimension::exp_if_not_1(self.time))?;
            }
            if self.amount != 0 {
                write!(
                    f,
                    "molecular_amount{} ",
                    Dimension::exp_if_not_1(self.amount)
                )?;
            }
            if self.current != 0 {
                write!(
                    f,
                    "electric_current{} ",
                    Dimension::exp_if_not_1(self.current)
                )?;
            }
            if self.temperature != 0 {
                write!(
                    f,
                    "temperature{} ",
                    Dimension::exp_if_not_1(self.temperature)
                )?;
            }
            write!(f, "}}")
        }
    }
}
