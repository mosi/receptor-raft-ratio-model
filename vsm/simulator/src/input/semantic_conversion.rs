use crate::input::structures;
use crate::input::structures::{
    AttributeRef, CountingElement, Dimension, GlobalCount, LanguageError, LogicalConstraint, Model,
    NonNumericalDimension, PrimitiveTypes, RateExpr, RateExprTerm, RestSolutionLeft,
    RestSolutionRight, RestSolutionRightTarget, RestSolutionTransition, Settings, Species,
    SpeciesCount, SpeciesLeft, SpeciesLeftIdentifier, SpeciesRight, StructuralChange,
};
use crate::input::syntax::*;
use crate::input::units::UNIT_SECOND;
//use crate::flatten::*;
//use itertools::Itertools;

//use nanoid::nanoid;
use std::collections::{HashMap, HashSet};
use std::fs;
//use std::any::Any;
use crate::flatten::AllOrSingleLeft;
use itertools::Itertools;
use uuid::Uuid;

fn rek_global_count(
    elem: &Option<Box<GlobalCountStx>>,
    all_species: &HashMap<String, Species>,
    all_constants: &HashMap<String, ConstantDefinition>,
) -> Result<Vec<CountingElement>, LanguageError> {
    match elem {
        None => Ok(vec![]),
        Some(x) => {
            let prelim = rek_global_count(&x.inside_of, all_species, all_constants)?;
            if !all_species.contains_key(&x.what.species_name) {
                return Err(LanguageError::from_span(
                    &x.span,
                    &*format!(
                        "The name '{}' is not a declared name for a species",
                        x.what.species_name
                    ),
                    line!(),
                    file!(),
                ));
            }
            let my_species = all_species.get(&x.what.species_name).unwrap();
            let my_elem = CountingElement::GeneralDescription(
                x.what.species_name.clone(),
                x.what
                    .attribute_constraints
                    .iter()
                    .map(|v| v.check_and_convert_on_left_side(my_species, all_constants))
                    .collect::<Result<_, _>>()?,
            );
            let res = vec![my_elem];
            Ok(res.into_iter().chain(prelim.into_iter()).collect())
        }
    }
}

impl GlobalCount {
    fn from_stx(
        d: &GlobalCountStx,
        all_species: &HashMap<String, Species>,
        all_constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<GlobalCount, LanguageError> {
        let definition =
            rek_global_count(&Some(Box::new((*d).clone())), all_species, all_constants)?;
        Ok(GlobalCount {
            definition,
            name: d.string_name.clone(),
        })
    }
}

impl Model {
    pub fn from_string(
        model_string: &String,
        make_parameters_constant: bool,
        allow_dynamic_structures: bool,
    ) -> Result<(Model, String), Vec<LanguageError>> {
        Model::from_string_multiple(
            model_string,
            make_parameters_constant,
            allow_dynamic_structures,
        )
        /* match std::panic::catch_unwind(||Model::from_string_multiple(model_string)){
            Ok(x) => x,
            Err(_y) => Err(vec![LanguageError{
                lstart: 0,
                lstop: 30,
                cstart: 0,
                cstop: 0,
                what: "INTERNAL CODE PANIC".to_string(),
                line: 0,
                file: "".to_string()
            }])
        }*/
    }

    fn from_string_internal(
        model_string: &String,
        make_parameters_constant: bool,
        allow_dynamic_structures: bool,
    ) -> Result<(Model, String), Vec<LanguageError>> {
        let m = ModelParsed::from_string(
            model_string,
            make_parameters_constant,
            allow_dynamic_structures,
        )?;

        /* check all constants */
        for (_, c) in m.constants.iter() {
            match RateExpr::from_tree(
                c.value.clone(),
                &vec![],
                &HashMap::new(),
                &m.constants,
                None,
                false,
            ) {
                Ok(_x) => {}
                Err(y) => return Err(vec![y]),
            }
        }

        let parameters: Vec<std::result::Result<(std::string::String, RateExpr), LanguageError>> =
            m.constants
                .iter()
                .filter(|(_, v)| v.is_param)
                .map(|(n, v)| {
                    Ok((
                        n.clone(),
                        RateExpr::from_tree(
                            v.value.clone(),
                            &vec![],
                            &HashMap::new(),
                            &m.constants,
                            None,
                            false,
                        )?
                        .0,
                    ))
                })
                .collect();
        let parameters: HashMap<_, _> = match parameters.into_iter().collect::<Result<_, _>>() {
            Ok(x) => x,
            Err(y) => return Err(vec![y]),
        };

        let latex = m.make_latex();

        let all_species = match m.validate_and_get_species() {
            Ok(x) => x,
            Err(y) => return Err(vec![y]),
        };

        let observations = match &m.observations {
            None => None,
            Some(x) => Some(
                match x
                    .iter()
                    .map(|v| GlobalCount::from_stx(&v, &all_species, &m.constants))
                    .collect::<Result<_, _>>()
                {
                    Ok(x) => x,
                    Err(x) => return Err(vec![x]),
                },
            ),
        };

        let transitions: Vec<_> = m
            .transitions
            .clone()
            .into_iter()
            .map(|t| {
                ModelParsed::check_and_get_transition(
                    t,
                    &all_species,
                    &m.constants,
                    allow_dynamic_structures,
                )
            })
            .collect();

        let transition_errs: Vec<_> = transitions
            .iter()
            .filter_map(|v| match v {
                Ok(_x) => None,
                Err(x) => Some(x),
            })
            .cloned()
            .collect();
        if !transition_errs.is_empty() {
            return Err(transition_errs);
        }

        let initial_set = match rekursive_right_side2(
            -1,
            Some(m.initializations),
            &all_species,
            &vec![],
            &m.constants,
            true,
            false,
        ) {
            Ok(x) => x.0,
            Err(y) => return Err(vec![y]),
        };

        let transitions = match transitions.into_iter().collect::<Result<_, _>>() {
            Ok(x) => x,
            Err(_x) => unreachable!(),
        };

        let settings = Settings {
            endtime: match m.settings.get(&SettingsVariant::Endtime) {
                None => None,
                Some(x) => match RateExpr::from_tree(
                    x.clone(),
                    &vec![],
                    &HashMap::new(),
                    &m.constants,
                    None,
                    false,
                ) {
                    Ok((xpr, dim)) => {
                        if dim != UNIT_SECOND.unit {
                            return Err(vec![LanguageError::from_span(
                                &x.span,
                                &*format!("unit is not time\n unit:{}", dim.to_string()),
                                line!(),
                                file!(),
                            )]);
                        }
                        match xpr.eval_to_primitive(&AllOrSingleLeft::None, None, None, None) {
                            Ok(x) => x.as_float(),
                            Err(y) => {
                                return Err(vec![LanguageError::from_span(
                                    &x.span,
                                    &y,
                                    line!(),
                                    file!(),
                                )])
                            }
                        }
                    }
                    Err(y) => return Err(vec![y]),
                },
            },
            num_reps: match m.settings.get(&SettingsVariant::NumReps) {
                None => None,
                Some(x) => match RateExpr::from_tree(
                    x.clone(),
                    &vec![],
                    &HashMap::new(),
                    &m.constants,
                    None,
                    false,
                ) {
                    Ok((xpr, dim)) => {
                        let mut ref_dim = Dimension::dimensionless();
                        ref_dim.non_numerical = NonNumericalDimension::Integer;
                        if dim != ref_dim {
                            return Err(vec![LanguageError::from_span(
                                &x.span,
                                &*format!(
                                    "unit is not integer:{} != {}",
                                    dim.to_string(),
                                    ref_dim.to_string()
                                ),
                                line!(),
                                file!(),
                            )]);
                        }
                        match xpr.eval_to_primitive(&AllOrSingleLeft::None, None, None, None) {
                            Ok(y) => match y {
                                PrimitiveTypes::String(_) => unreachable!(),
                                PrimitiveTypes::Boolean(_) => unreachable!(),
                                PrimitiveTypes::Float(_) => unreachable!(),
                                PrimitiveTypes::Integer(y) => {
                                    if y < 1 {
                                        return Err(vec![LanguageError::from_span(
                                            &x.span,
                                            "Zero/negative rep not allowed!",
                                            line!(),
                                            file!(),
                                        )]);
                                    } else {
                                        Some(y as usize)
                                    }
                                }
                            },
                            Err(y) => {
                                return Err(vec![LanguageError::from_span(
                                    &x.span,
                                    &y,
                                    line!(),
                                    file!(),
                                )])
                            }
                        }
                    }
                    Err(y) => return Err(vec![y]),
                },
            },
        };

        let new_m = Model {
            inital: initial_set,
            observations,

            species: all_species,
            transitions,
            parameters: parameters,
            settings,
        };

        match new_m.verify_spicies() {
            Ok(_) => {}
            Err(msg) => {
                return Err(vec![LanguageError {
                    lstart: 0,
                    lstop: 0,
                    cstart: 100,
                    cstop: 100,
                    what: msg,
                    line: line!(),
                    file: file!().parse().unwrap(),
                }])
            }
        }

        Ok((new_m, latex))
    }

    fn from_string_multiple(
        model_string: &String,
        make_parameters_constant: bool,
        allow_dynamic_structures: bool,
    ) -> Result<(Model, String), Vec<LanguageError>> {
        let mut errors = HashSet::new();
        let mut modif_string = model_string.clone() + "\n";

        let mut max_error = 21;

        loop {
            max_error -= 1;
            if max_error == 0 {
                println!("Reached a maximum of 20 errors!");
                return Err(errors.into_iter().collect());
            }
            let res = Model::from_string_internal(
                &modif_string,
                make_parameters_constant,
                allow_dynamic_structures,
            );
            match res {
                Ok(x) => {
                    if errors.is_empty() {
                        return Ok(x);
                    } else {
                        return Err(errors.into_iter().collect());
                    }
                }
                Err(e) => {
                    let old_size = errors.len();
                    errors.extend(e.iter().cloned());
                    //println!("Found new error: {}",errors.last().unwrap().what);
                    if e.len() + old_size != errors.len() {
                        println!("could not recoverer errors");
                        return Err(errors.into_iter().collect());
                    }

                    let first_error_line = match e.last().expect("No first error").lstart {
                        0 => 1,
                        x => x,
                    };
                    let first_error_column = match e.last().expect("No first error").cstart {
                        0 => 1,
                        x => x,
                    };

                    assert!(first_error_line > 0);
                    //println!("the line({}): {}",first_error_line,modif_string.split("\n").skip(first_error_line-1).next().unwrap());
                    let number_of_statements_before = modif_string
                        .split("\n")
                        .take(first_error_line - 1)
                        .join(" ")
                        .matches(";")
                        .count()
                        + match modif_string.split("\n").skip(first_error_line - 1).next() {
                            None => 0,
                            Some(x) => {
                                if x.len() < first_error_column {
                                    0
                                } else {
                                    x[..(first_error_column - 1)].matches(";").count()
                                }
                            }
                        };

                    println!(
                        "Crit Statement: {}\nCritend",
                        modif_string
                            .split(";")
                            .skip(number_of_statements_before)
                            .next()
                            .unwrap()
                    );
                    println!(
                        "All Statement: {}\nAllend",
                        modif_string.split(";").clone().join(";---")
                    );
                    println!("B4 Critical is : {}\n", number_of_statements_before);
                    modif_string = modif_string
                        .split(";")
                        .take(number_of_statements_before)
                        .join(";")
                        + ";"
                        + &*"\n".repeat(
                            modif_string
                                .split(";")
                                .skip(number_of_statements_before)
                                .next()
                                .unwrap()
                                .matches("\n")
                                .count(),
                        )
                        + &*modif_string
                            .split(";")
                            .skip(number_of_statements_before + 1)
                            .join(";");

                    println!("{}", modif_string.replace("\n", "\n##"));
                }
            }
        }
    }

    pub fn from_file(
        filename: String,
        make_parameters_constant: bool,
        allow_dynamic_structures: bool,
    ) -> Result<(Model, String), Vec<LanguageError>> {
        let model_string = fs::read_to_string(&filename).expect(&*format!(
            "Something went wrong reading the file '{}'. Does it exist?",
            filename
        ));

        Model::from_string(
            &model_string,
            make_parameters_constant,
            allow_dynamic_structures,
        )
    }
}

struct AttAsiggnmentsn {
    local: Vec<(String, RateExpr)>,
    global: Vec<(String, RateExpr)>,
}

fn get_attribute_assignments(
    species: &SpeciesRightStx,
    all_species: &HashMap<String, Species>,
    left_match: &Vec<SpeciesLeft>,
    all_constants: &HashMap<String, ConstantDefinition>,
    allow_attribute_assignment_for_comparments: bool,
) -> Result<AttAsiggnmentsn, LanguageError> {
    let names: Vec<_> = species
        .attribute_assignments
        .iter()
        .map(|(n, _a)| n)
        .collect();
    let names_set: HashSet<_> = names.iter().collect();
    if names_set.len() != names.len() {
        return Err(LanguageError::from_span(
            &species.span,
            "An attribute is defined multiple times here",
            line!(),
            file!(),
        ));
    }

    let checked_assignments : Vec<_> = species
        .attribute_assignments
        .iter()
        .map(|(n,a)|{
            if ! ( allow_attribute_assignment_for_comparments || all_species.get(&*species.species_name).unwrap().global_attributes.contains_key(n)){
                if all_species.get(&*species.species_name).unwrap().is_compartment {
                    return Err(LanguageError::from_span(&species.span,
                                    &*format!(
                                    "It is not allowed to change the attributes of this compartment. You can use starred atribute ({} : float*) instead",n,
                                ),line!(),file!()));

                }
            }
            Ok((n,a))
        })
        .map(|val|{
            let (n,a) = val?;
            if  all_species.get(&*species.species_name).unwrap().global_attributes.contains_key(n)
                && !all_species.get(&*species.species_name).unwrap().is_compartment {
                    return Err(LanguageError::from_span(&species.span,
                                    "Starred attributes are not allowed for non compartments",line!(),file!()
                        )
                    )}


            Ok((n,a))
        })
        .map(|val| {
            let  (name, assign) = val?;
            if !(all_species.get(&*species.species_name).unwrap().attributes.contains_key(name) || all_species.get(&*species.species_name).unwrap().global_attributes.contains_key(name)) {
                return Err(LanguageError::from_span(&species.span,&*format!(
                    "The name '{}' is not an attribute for species '{}'",
                    name,
                    species.species_name
                ),line!(),file!()));
            }
            Ok((name, assign))
        }).collect::<Result<_,_>>()?;
    Ok(AttAsiggnmentsn {
        local: checked_assignments
            .iter()
            .filter(|(name, _)| {
                all_species
                    .get(&*species.species_name)
                    .unwrap()
                    .attributes
                    .contains_key(*name)
            })
            .map(|(name, assign)| {
                let (val, dim) = RateExpr::from_tree(
                    (*assign).clone(),
                    left_match,
                    all_species,
                    all_constants,
                    None,
                    false,
                )?;
                if !Dimension::assignable(
                    all_species
                        .get(&*species.species_name)
                        .unwrap()
                        .attributes
                        .get(*name)
                        .unwrap(),
                    &dim,
                ) {
                    return Err(LanguageError::from_span(
                        &assign.span,
                        &*format!(
                            //Todo standarize unit error
                            "Unit or type missmatch ((is) {} != (should){})",
                            dim,
                            *all_species
                                .get(&*species.species_name)
                                .unwrap()
                                .attributes
                                .get(*name)
                                .unwrap()
                        ),
                        line!(),
                        file!(),
                    ));
                }
                Ok(((*name).clone(), val))
            })
            .collect::<Result<_, _>>()?,
        global: checked_assignments
            .iter()
            .filter(|(name, _)| {
                all_species
                    .get(&*species.species_name)
                    .unwrap()
                    .global_attributes
                    .contains_key(*name)
            })
            .map(|(name, assign)| {
                let (val, dim) = RateExpr::from_tree(
                    (*assign).clone(),
                    left_match,
                    all_species,
                    all_constants,
                    None,
                    false,
                )?;
                if !Dimension::assignable(
                    all_species
                        .get(&*species.species_name)
                        .unwrap()
                        .global_attributes
                        .get(*name)
                        .unwrap(),
                    &dim,
                ) {
                    return Err(LanguageError::from_span(
                        &assign.span,
                        &*format!(
                            //Todo standarize unit error
                            "Unit or type missmatch ((is) {} != (should){})",
                            dim,
                            *all_species
                                .get(&*species.species_name)
                                .unwrap()
                                .attributes
                                .get(*name)
                                .unwrap()
                        ),
                        line!(),
                        file!(),
                    ));
                }
                Ok(((*name).clone(), val))
            })
            .collect::<Result<_, _>>()?,
    })
}

fn rekursive_right_side2(
    parent_number: isize,
    solution: Option<SpeciesRightSolution>,
    all_species: &HashMap<String, Species>,
    left_match: &Vec<SpeciesLeft>,
    all_constants: &HashMap<String, ConstantDefinition>,
    allow_attribute_assignment_for_comparments: bool,
    allow_rest_solution: bool,
) -> Result<(Vec<SpeciesRight>, Vec<RestSolutionRight>), LanguageError> {
    let mut res: Vec<SpeciesRight> = vec![];
    let mut res_rest: Vec<RestSolutionRight> = vec![];
    match solution {
        None => {}
        Some(solution) => {
            for species in solution.species_in_solution {
                if species.is_rest_solution {
                    if !allow_rest_solution {
                        return Err(LanguageError::from_span(
                            &species.span,
                            "Rest solutions are not allowed here",
                            line!(),
                            file!(),
                        ));
                    }
                    res_rest.push(RestSolutionRight {
                        name: species.species_name,
                        compartment_constraint: parent_number,
                    });
                } else {
                    if !all_species.contains_key(&species.species_name) {
                        return Err(LanguageError::from_span(
                            &species.span,
                            &*format!(
                                "The name '{}' is not a declared name for a species",
                                species.species_name
                            ),
                            line!(),
                            file!(),
                        ));
                    }

                    let (amt_expr, dim) = RateExpr::from_tree(
                        species.amt.clone(),
                        &vec![],
                        &HashMap::new(),
                        all_constants,
                        None,
                        false,
                    )?;
                    let mut dim_i_want = Dimension::dimensionless();
                    dim_i_want.non_numerical = NonNumericalDimension::Integer;
                    if dim != dim_i_want {
                        return Err(LanguageError::from_span(
                            &species.span,
                            "Expected an integer as amount!",
                            line!(),
                            file!(),
                        ));
                    }
                    let calculated_amt = match amt_expr
                        .eval_to_primitive(&AllOrSingleLeft::None, None, None, None)
                        .unwrap()
                    {
                        PrimitiveTypes::String(_) => unreachable!(),
                        PrimitiveTypes::Boolean(_) => unreachable!(),
                        PrimitiveTypes::Float(_) => unreachable!(),
                        PrimitiveTypes::Integer(y) => y,
                    };
                    if calculated_amt <= 0 {
                        return Err(LanguageError::from_span(
                            &species.span,
                            "A negative or zero amt is not allowed!",
                            line!(),
                            file!(),
                        ));
                    }
                    let calculated_amt = calculated_amt as usize;
                    let new_amt;
                    let reps = match all_species
                        .get(&species.species_name)
                        .unwrap()
                        .is_compartment
                    {
                        true => {
                            new_amt = 1;
                            calculated_amt
                        }
                        false => {
                            new_amt = calculated_amt;
                            1
                        }
                    };
                    for _ in 0..reps {
                        let my_number = parent_number + res.len() as isize + 1;

                        let assignmentns = get_attribute_assignments(
                            &species,
                            all_species,
                            left_match,
                            all_constants,
                            allow_attribute_assignment_for_comparments,
                        )?;

                        res.push(SpeciesRight {
                            species_name: species.species_name.to_string(),
                            extra_name: species.extra_name.clone(),
                            compartment_constraint: parent_number,
                            corresponding_on_left: None,
                            amount: new_amt,
                            local_attribute_assignment: assignmentns.local,
                            global_attribute_assignment: assignmentns.global,
                        });
                        let sub_res = rekursive_right_side2(
                            my_number,
                            species.child_solution.clone(),
                            all_species,
                            left_match,
                            all_constants,
                            allow_attribute_assignment_for_comparments,
                            allow_rest_solution,
                        )?;
                        res.extend(sub_res.0.into_iter());
                        res_rest.extend(sub_res.1.into_iter());
                    }
                }
            }
        }
    }
    Ok((res, res_rest))
}

impl LogicalConstraintStx<'_> {
    fn check_and_convert_on_left_side(
        &self,
        spec: &Species,
        all_constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<LogicalConstraint, LanguageError> {
        let (l, ldim) = RateExpr::from_tree(
            self.left.clone(),
            &Vec::new(),
            &HashMap::new(),
            all_constants,
            Some(spec),
            false,
        )?;
        let (r, rdim) = RateExpr::from_tree(
            self.right.clone(),
            &Vec::new(),
            &HashMap::new(),
            all_constants,
            Some(spec),
            false,
        )?;
        if !self.op.dim_ok(&ldim, &rdim) {
            return Err(LanguageError::from_span(
                &self.span,
                &*format!(
                    "Dimensions don't match for logical comparison {}!={}",
                    ldim, rdim
                ),
                line!(),
                file!(),
            ));
        }
        Ok(LogicalConstraint {
            left: l,
            op: self.op.clone(),
            right: r,
        })
    }
    fn check_and_convert_in_guard(
        &self,
        spec_left: &Vec<SpeciesLeft>,
        all_species: &HashMap<String, Species>,
        all_constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<LogicalConstraint, LanguageError> {
        let (l, ldim) = RateExpr::from_tree(
            self.left.clone(),
            spec_left,
            all_species,
            all_constants,
            None,
            true,
        )?;
        let (r, rdim) = RateExpr::from_tree(
            self.right.clone(),
            spec_left,
            all_species,
            all_constants,
            None,
            true,
        )?;
        if !self.op.dim_ok(&ldim, &rdim) {
            return Err(LanguageError::from_span(
                &self.span,
                &*format!(
                    "Dimensions don't match for logical comparison {}!={}",
                    ldim, rdim
                ),
                line!(),
                file!(),
            ));
        }
        Ok(LogicalConstraint {
            left: l,
            op: self.op.clone(),
            right: r,
        })
    }
}

fn rekursive_left_side2(
    parent_id: Option<SpeciesLeftIdentifier>,
    solution: Option<SpeciesLeftSolution>,
    all_species: &HashMap<String, Species>,
    all_constants: &HashMap<String, ConstantDefinition>,
) -> Result<(Vec<SpeciesLeft>, Vec<RestSolutionLeft>), LanguageError> {
    let mut res: Vec<SpeciesLeft> = vec![];
    let mut res_rest: Vec<RestSolutionLeft> = vec![];
    match solution {
        None => {}
        Some(solution) => {
            for species_stx in solution.species_in_solution {
                if species_stx.is_rest_solution {
                    if parent_id.is_none() {
                        return Err(LanguageError::from_span(
                            &species_stx.span,
                            "Rest solutions are not allowed here (top hirachy level on left side)",
                            line!(),
                            file!(),
                        ));
                    }
                    res_rest.push(RestSolutionLeft {
                        name: species_stx.species_name,
                        parent_id: parent_id.clone().unwrap(),
                    });
                } else {
                    //let my_number = parent_number + res.len() as isize + 1;
                    if !all_species.contains_key(&species_stx.species_name) {
                        return Err(LanguageError::from_span(
                            &species_stx.span,
                            &*format!(
                                "The name '{}' is not a declared name for a species",
                                species_stx.species_name
                            ),
                            line!(),
                            file!(),
                        ));
                    }
                    let my_id = SpeciesLeftIdentifier::new(&*match &species_stx.extra_name {
                        Some(n) => n.clone(),
                        None => Uuid::new_v4().to_string(),
                    });
                    res.push(SpeciesLeft {
                        //extra_name: species_stx.extra_name.clone(),
                        species_name: species_stx.species_name.to_string(),
                        //number_in_match: 0, // TODO
                        id: my_id.clone(),
                        parent_number: parent_id.clone(),
                        attribute_constraint: species_stx
                            .attribute_constraints
                            .iter()
                            .map(|v| {
                                v.check_and_convert_on_left_side(
                                    all_species.get(&species_stx.species_name).unwrap(),
                                    all_constants,
                                )
                            })
                            .collect::<Result<_, _>>()?,
                    });
                    let r = rekursive_left_side2(
                        Some(my_id),
                        species_stx.child_solution,
                        all_species,
                        all_constants,
                    )?;
                    res.extend(r.0.into_iter());
                    res_rest.extend(r.1.into_iter());
                }
            }
        }
    };
    Ok((res, res_rest))
}

impl RateExprTerm {
    fn from_tree(
        tree: ExprTermStx,
        left_match: &Vec<SpeciesLeft>,
        all_species: &HashMap<String, Species>,
        all_constants: &HashMap<String, ConstantDefinition>,
        single_reference_species: Option<&Species>,
        span: &pest::Span,
        allow_time: bool,
    ) -> Result<(RateExprTerm, Dimension), LanguageError> {
        return match tree {
            ExprTermStx::NumUnit(x, _) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::Float(x.value)),
                x.unit,
            )),
            ExprTermStx::ConstantOrLeftAttribute(x) => {
                if single_reference_species.is_some() {
                    match single_reference_species.unwrap().attributes.get(&x) {
                        Some(at) => {
                            return Ok((
                                RateExprTerm::AttRef(AttributeRef {
                                    species_id: None,
                                    /*assuming only one on left, when performing calculation*/
                                    att_name: x,
                                }),
                                at.clone(),
                            ));
                        }

                        None => (),
                    }
                    match single_reference_species.unwrap().global_attributes.get(&x) {
                        Some(_at) => {
                            return  Err(LanguageError::from_span(&span,&*format!("You are using the global (*) attribute '{}' in an expression where it is not currently implemented (e.g. Left side guard). You can get the same effect, by putting this attribute in a reaction rate guard ( @ if '{}' > 12  then)", x,x)
                                                         ,line!(),file!()));
                        }
                        None => (),
                    }
                }

                match all_constants.get(&x) {
                    Some(cd) => {
                        match cd.is_param{
                            false => {
                                let (expr, dim) = RateExpr::from_tree(
                                    cd.clone().value,
                                    left_match,
                                    all_species,
                                    all_constants,
                                    single_reference_species,
                                    false
                                )?;
                                Ok((RateExprTerm::Expr(Box::new(expr)), dim))
                            } true => {
                                let (_expr, dim) = RateExpr::from_tree(
                                    cd.clone().value,
                                    left_match,
                                    all_species,
                                    all_constants,
                                    single_reference_species,
                                    false
                                )?;
                                Ok((RateExprTerm::Parameter(x.clone()), dim))
                            }
                        }

                    }

                    None =>

                         Err(LanguageError::from_span(&span,&*format!("Constant/attribute '{}' not defined.\nIf this has occured in an attribute assignment, maybe try giving the species name\n  (-> use <name>.{} instead of {})", x,x,x)
                        ,line!(),file!()))
                }
            }

            ExprTermStx::DottedAttributeRef(att_ref) => {
                if single_reference_species.is_some() {
                    return Err(LanguageError::from_span(
                        &span,"Global attribute references are not allowed for left side Attribute constraints",line!(),file!()));
                }
                let idx_on_left = match left_match.iter().find(|v| {
                    ((v.species_name == att_ref.spec_name)
                        && left_match
                            .iter()
                            .filter(|v| v.species_name == att_ref.spec_name)
                            .count()
                            == 1)
                        || (v.id == SpeciesLeftIdentifier::new(&att_ref.spec_name))
                }) {
                    Some(n) => n.id.clone(),
                    None => {
                        return Err(LanguageError::from_span(
                            &span,
                            &*format!(
                                "Species name '{}' not found uniquely on left side",
                                att_ref.spec_name
                            ),
                            line!(),
                            file!(),
                        ))
                    }
                };
                match all_species
                    .get(
                        &left_match
                            .iter()
                            .find(|v| v.id == idx_on_left)
                            .unwrap()
                            .species_name,
                    )
                    .unwrap()
                    .attributes
                    .get(&*att_ref.att_name)
                {
                    Some(x) => Ok((
                        RateExprTerm::AttRef(AttributeRef {
                            species_id: Some(idx_on_left.clone()),
                            att_name: att_ref.att_name.clone(),
                        }),
                        (*x).clone(),
                    )),
                    None => match all_species
                        .get(
                            &left_match
                                .iter()
                                .find(|v| v.id == idx_on_left)
                                .unwrap()
                                .species_name,
                        )
                        .unwrap()
                        .global_attributes
                        .get(&*att_ref.att_name)
                    {
                        Some(x) => Ok((
                            RateExprTerm::GlobAttRef(AttributeRef {
                                species_id: Some(idx_on_left.clone()),
                                att_name: att_ref.att_name.clone(),
                            }),
                            (*x).clone(),
                        )),
                        None => Err(LanguageError::from_span(
                            &span,
                            &*format!(
                                "The name '{}' is not an attribute for species '{}'",
                                att_ref.att_name, att_ref.spec_name
                            ),
                            line!(),
                            file!(),
                        )),
                    },
                }
            }
            ExprTermStx::Expr(exr) => {
                let (bx, dim) = RateExpr::from_tree(
                    *exr,
                    left_match,
                    all_species,
                    all_constants,
                    single_reference_species,
                    allow_time,
                )?;
                Ok((RateExprTerm::Expr(Box::new(bx)), dim))
            }
            ExprTermStx::SpeciesCount(count_name) => Ok((
                RateExprTerm::SpecCount(SpeciesCount {
                    idx_on_left: match left_match.iter().find(|v| {
                        ((v.species_name == count_name)
                            && left_match
                                .iter()
                                .filter(|v| v.species_name == count_name)
                                .count()
                                == 1)
                            || (v.id == SpeciesLeftIdentifier::new(&count_name))
                    }) {
                        Some(n) => n.id.clone(),
                        None => {
                            return Err(LanguageError::from_span(
                                &span,
                                &*format!(
                                    "Species name '{}' not found uniquely on left side",
                                    count_name
                                ),
                                line!(),
                                file!(),
                            ))
                        }
                    },
                }),
                Dimension::dimensionless(),
            )),
            ExprTermStx::ActualString(x) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::String(x)),
                Dimension::non_numeric(NonNumericalDimension::String),
            )),
            ExprTermStx::ActualBool(x) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::Boolean(x)),
                Dimension::non_numeric(NonNumericalDimension::Bool),
            )),
            ExprTermStx::ActualInt(x) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::Integer(x)),
                Dimension::non_numeric(NonNumericalDimension::Integer),
            )),
            ExprTermStx::GlobalSpeciesCount(x) => Ok((
                RateExprTerm::GlobSpecCount(GlobalCount::from_stx(&x, all_species, all_constants)?),
                Dimension::non_numeric(NonNumericalDimension::Integer),
            )),
            ExprTermStx::GlobalTime => match allow_time {
                true => Ok((RateExprTerm::GlobalTime(), UNIT_SECOND.unit)),
                false => Err(LanguageError::from_span(
                    &span,
                    "__time__ may only be used in rates or guards",
                    line!(),
                    file!(),
                )),
            },
        };
    }
}

impl RateExpr {
    fn from_tree(
        tree: ExprStx,
        left_match: &Vec<SpeciesLeft>,
        all_species: &HashMap<String, Species>,
        all_constants: &HashMap<String, ConstantDefinition>,
        single_reference_species: Option<&Species>,
        allow_time: bool,
    ) -> Result<(RateExpr, Dimension), LanguageError> {
        let (l, ldim) = RateExprTerm::from_tree(
            tree.left,
            left_match,
            all_species,
            all_constants,
            single_reference_species,
            &tree.span,
            allow_time,
        )?;

        match tree.op {
            None => Ok((RateExpr { left: l, op: None }, ldim)),
            Some((op, term)) => {
                let (r, rdim) = RateExprTerm::from_tree(
                    term,
                    left_match,
                    all_species,
                    all_constants,
                    single_reference_species,
                    &tree.span,
                    allow_time,
                )?;

                let new_dim = match op.combine_dimension(&ldim, &rdim) {
                    None => Err(LanguageError::from_span(
                        &tree.span,
                        &*format!("Unit Missmatch {} != {}", ldim, rdim),
                        line!(),
                        file!(),
                    )),
                    Some(x) => Ok(x),
                }?;

                Ok((
                    RateExpr {
                        left: l,
                        op: Some((op, r)),
                    },
                    new_dim,
                ))
            }
        }
    }
}

impl structures::Transition {
    fn get_structural_changes(
        &self,
        r_on_left: &Vec<RestSolutionLeft>,
        r_on_right: &Vec<RestSolutionRight>,
        all_species: &HashMap<String, Species>,
        allow_dynamic_structures: bool,
    ) -> Result<Vec<StructuralChange>, String> {
        let mut found_changes = vec![];
        let compartment_movement = self.compartments_movement(
            all_species,
            r_on_left,
            r_on_right,
            allow_dynamic_structures,
        );
        let mut compartment_movement = match compartment_movement {
            Ok(x) => x,
            Err(err) => return Err(err),
        };
        if r_on_left.is_empty() && r_on_right.is_empty() && compartment_movement.is_empty() {
            Ok(vec![])
        } else {
            let left_set: HashSet<_> = r_on_left.iter().map(|v| v.name.clone()).collect();

            if r_on_right.len() != r_on_right.iter().map(|r| &r.name).unique().count() {
                return Err("A name is used twice for a restsolution on the right side. Duplication of rest solutions is not implemented".to_string());
            }

            if left_set.len() != r_on_left.len() {
                return Err("A name is used twice for a restsolution".to_string());
            }

            for r in r_on_right.iter() {
                if !left_set.contains(&*r.name) {
                    return Err(format!(
                        "Rest solution \"{}\" does not exist on the left side. We only have {:?}",
                        r.name, left_set
                    )
                    .to_string());
                } else {
                    // is contained on left
                    let lefty = r_on_left
                        .iter()
                        .find(|v| v.name == r.name)
                        .expect("should be on left");
                    if r.compartment_constraint == -1 {
                        if !allow_dynamic_structures {
                            return Err(format!(
                                "Rest solution \"{}\" is moved to higher level. This is not yet supported.",
                                r.name
                            )
                                .to_string());
                        } else {
                            found_changes.push(StructuralChange::MoveRestSolutionTo {
                                old: lefty.clone(),
                                new: RestSolutionRightTarget::ReactionLevel,
                            })
                        }
                    } else {
                        let idx_from_consstraint: usize = r.compartment_constraint as usize;
                        match &self.right[idx_from_consstraint].corresponding_on_left {
                            None => {
                                if !allow_dynamic_structures {
                                    return Err(format!(
                                    "Rest solution \"{}\" is moved to new compartment. This is not yet supported.",
                                    r.name
                                ));
                                } else {
                                    found_changes.push(StructuralChange::MoveRestSolutionTo {
                                        old: lefty.clone(),
                                        new: RestSolutionRightTarget::OtherCompartment(
                                            self.right[idx_from_consstraint].clone(),
                                        ),
                                    })
                                }
                            }
                            Some(left_id) => {
                                if *left_id != lefty.parent_id {
                                    {
                                        if !allow_dynamic_structures {
                                            return Err(format!(
                                            "Rest solution \"{}\" is moved to different (existing) compartment. This is not yet supported.",
                                            r.name
                                        ));
                                        } else {
                                            found_changes.push(
                                                StructuralChange::MoveRestSolutionTo {
                                                    old: lefty.clone(),
                                                    new: RestSolutionRightTarget::OtherCompartment(
                                                        self.right[idx_from_consstraint].clone(),
                                                    ),
                                                },
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                println!("Rest solution {} has no affect", r.name);
            }

            for l in r_on_left {
                match r_on_right.iter().find(|v| v.name == l.name) {
                    None => {
                        if allow_dynamic_structures {
                            found_changes.push(StructuralChange::RemoveRestSolution(l.clone()));
                        } else {
                            return Err(format!(
                                "Rest solution \"{}\" is removed. This is not yet supported.",
                                l.name
                            ));
                        }
                    }
                    Some(_) => {}
                }
            }
            found_changes.append(&mut compartment_movement);
            Ok(found_changes)
        }
    }
    fn compartments_movement(
        &self,
        all_species: &HashMap<String, Species>,
        r_left: &Vec<RestSolutionLeft>,
        _r_right: &Vec<RestSolutionRight>,
        allow_dynamic_structures: bool,
    ) -> Result<Vec<StructuralChange>, String> {
        let mut res = vec![];
        let left_comps: Vec<_> = self
            .left
            .iter()
            .filter(|v| all_species.get(&*v.species_name).unwrap().is_compartment)
            .collect();
        let left_comps: Vec<_> = left_comps
            .into_iter()
            .map(|v| (v, r_left.iter().find(|rest| rest.parent_id == v.id)))
            .collect();

        let right_comps: Vec<_> = self
            .right
            .iter()
            .filter(|v| all_species.get(&*v.species_name).unwrap().is_compartment)
            .collect();

        /*println!(
            "\n{}\n  {:?}\n   {:?}",
            self.original_string, left_comps, right_comps
        );*/
        for (spec, rest) in left_comps.iter() {
            if right_comps
                .iter()
                .find(|r| r.corresponding_on_left == Some(spec.id.clone()))
                .is_none()
            {
                if rest.is_none() {
                    return Err(format!("When you are using dynamic nesting you must specifiy a rest soulution on the left side for each moving/deleted compartment. '{}' has no such rest solution set",spec.species_name).to_string());
                }
                if !allow_dynamic_structures {
                    return Err(
                        format!("Removing '{}' not supported", spec.species_name).to_string()
                    );
                } else {
                    res.push(StructuralChange::RemoveCompartment(spec.id.clone()));
                }
            }
        }

        for r in right_comps.iter() {
            if r.amount != 1 {
                return Err(format!(
                    "Amounts for compartments on the right side need to be 1 (but is {} for {})",
                    r.amount, r.species_name
                ));
            }
            if r.corresponding_on_left.is_none() {
                if !allow_dynamic_structures {
                    return Err("generate new compartment is not supported".to_string());
                } else {
                    res.push(StructuralChange::OtherUnimplemented);
                }
            } else {
                if match self.left.iter().position(|v| {
                    Some(v.id.clone())
                        == self
                            .get_left_by_id(r.corresponding_on_left.as_ref().unwrap())
                            .as_ref()
                            .unwrap()
                            .parent_number
                }) {
                    None => -1,
                    Some(x) => x as isize,
                } != r.compartment_constraint
                {
                    let (left, left_sub) = left_comps
                        .iter()
                        .find(|(v, _b)| v.id == *r.corresponding_on_left.as_ref().unwrap())
                        .unwrap();
                    if left_sub.is_none() {
                        return Err(format!("When you are using dynamic nesting you must specifiy a rest soulution on the left side for each moving compartment. '{}' has no such rest solution set",left.species_name).to_string());
                    }
                    return Err("moving compartment, is currently not allowed!".to_string());
                }
            }
        }

        Ok(res)
    }
}

impl ModelParsed<'_> {
    pub fn check_and_get_transition(
        t: TransitionStx,
        all_species: &HashMap<String, Species>,
        all_constants: &HashMap<String, ConstantDefinition>,
        allow_dynamic_structures: bool,
    ) -> Result<structures::Transition, LanguageError> {
        let left = rekursive_left_side2(None, Some(t.left.clone()), all_species, all_constants)?;
        let (rate_expr, rate_expr_dim) = RateExprTerm::from_tree(
            ExprTermStx::Expr(Box::new(t.rate.clone())),
            &left.0,
            all_species,
            all_constants,
            None,
            &t.rate.span,
            true,
        )?;
        if rate_expr_dim != UNIT_SECOND.my_power(-1).unit {
            return Err(LanguageError::from_span(
                &t.rate.span,
                &*format!("unit is not 1/time\n unit:{}", rate_expr_dim.to_string()),
                line!(),
                file!(),
            ));
        }
        let sol = rekursive_right_side2(
            -1,
            Some(t.right.clone()),
            all_species,
            &left.0,
            all_constants,
            false,
            true,
        )?;
        let mut new_t = structures::Transition {
            original_string: t.original_string,
            rate_expr: RateExpr {
                left: rate_expr,
                op: None,
            },
            guard: t
                .guard
                .iter()
                .map(|v| v.check_and_convert_in_guard(&left.0, all_species, all_constants))
                .collect::<Result<_, _>>()?,
            right: sol.0.clone(),
            left: left.0,
            is_not_mass_action: t.is_not_mass_action,
            structural_changes: vec![],
            rest_solution_transiotions: vec![],
        };

        new_t.use_names_for_left_right_correspondence();
        new_t.find_left_right_correspondence();
        new_t.rest_solution_transiotions = left
            .1
            .iter()
            .map(|left| RestSolutionTransition {
                leftsideparent: left.parent_id.clone(),
                idx_right: match sol.1.iter().find(|r| r.name == left.name) {
                    None => None,
                    Some(right) => Some(right.compartment_constraint),
                },
            })
            .collect();
        new_t.structural_changes = match new_t.get_structural_changes(
            &left.1,
            &sol.1,
            all_species,
            allow_dynamic_structures,
        ) {
            Ok(y) => y,
            Err(e) => return Err(LanguageError::from_span(&t.span, &e, line!(), file!())),
        };

        /*for (c_name, c) in all_species.iter().filter(|(_, s)| s.is_compartment) {
            assert_eq!(*c_name, c.name);
            let count_left = new_t
                .left
                .iter()
                .filter(|v| v.species_name == c.name)
                .count();
            let count_right = new_t
                .right
                .iter()
                .filter(|v| v.species_name == c.name)
                .map(|v| {
                    assert_eq!(v.amount, 1);
                    v
                })
                .count();
            if count_left > 1 || count_right > 1 || count_left != count_right {
                panic!(
                    "{}",
                    Error::<Rule>::new_from_span(
                        ErrorVariant::CustomError {
                            message: format!(
                                "Compartmeant {} occurs {} on left and {} on right, but it has to be exatly once on both sides", c.name, count_left, count_right
                            )
                                .to_string()
                        },
                        t.span.clone()
                    )
                )
            }
            if count_right == 1 {
                let _pos_left = new_t
                    .left
                    .iter()
                    .find_position(|v| v.species_name == c.name)
                    .unwrap();
                assert!(new_t
                    .right
                    .iter()
                    .find(|v| v.species_name == c.name)
                    .unwrap()
                    .corresponding_on_left
                    .is_some())
            }
        }
        */
        Ok(new_t)
    }

    fn get_all_nested_rek_right(sol: &SpeciesRightSolution) -> HashSet<String> {
        sol.species_in_solution
            .iter()
            .filter(|v| v.child_solution.is_some())
            .map(|v| {
                let mut r =
                    ModelParsed::get_all_nested_rek_right(v.child_solution.as_ref().unwrap());
                r.insert(v.species_name.clone());
                r.into_iter()
            })
            .flatten()
            .collect()
    }

    fn get_all_nested_rek_left(sol: &SpeciesLeftSolution) -> HashSet<String> {
        sol.species_in_solution
            .iter()
            .filter(|v| v.child_solution.is_some())
            .map(|v| {
                let mut r =
                    ModelParsed::get_all_nested_rek_left(v.child_solution.as_ref().unwrap());
                r.insert(v.species_name.clone());
                r.into_iter()
            })
            .flatten()
            .collect()
    }

    fn get_all_nested_species(&self) -> HashSet<String> {
        let mut res: HashSet<String> = ModelParsed::get_all_nested_rek_right(&self.initializations)
            .iter()
            .cloned()
            .collect();

        for t in self.transitions.iter() {
            res.extend(
                ModelParsed::get_all_nested_rek_right(&t.right)
                    .iter()
                    .cloned(),
            );
            res.extend(
                ModelParsed::get_all_nested_rek_left(&t.left)
                    .iter()
                    .cloned(),
            );
        }
        res
    }

    fn validate_and_get_species(&self) -> Result<HashMap<String, Species>, LanguageError> {
        let nested_compartments = self.get_all_nested_species();
        //println!("-- compartments: {:?}", nested_compartments);
        let mut used_names = HashMap::new();
        for i in self.species_definitions.iter() {
            //Todo check attributes unique
            match used_names.insert(
                i.name.to_string(),
                Species {
                    is_compartment: nested_compartments.contains(&*i.name.to_string()),
                    name: i.name.to_string(),
                    attributes: i
                        .attributes
                        .iter()
                        .cloned()
                        .filter(|v| v.can_be_expanded)
                        .map(|v| (v.name, v.dim))
                        .collect(),
                    global_attributes: i
                        .attributes
                        .iter()
                        .cloned()
                        .filter(|v| !v.can_be_expanded)
                        .map(|v| (v.name, v.dim))
                        .collect(),
                },
            ) {
                Some(_) => {
                    return Err(LanguageError::from_span(
                        &i.span,
                        &*format!("The name '{}' is not unique", i.name),
                        line!(),
                        file!(),
                    ))
                }
                None => (),
            };
            if (used_names.get(&*i.name).unwrap().attributes.len()
                + used_names.get(&*i.name).unwrap().global_attributes.len()
                != i.attributes.len())
                || (used_names
                    .get(&*i.name)
                    .unwrap()
                    .attributes
                    .keys()
                    .any(|v| {
                        used_names
                            .get(&*i.name)
                            .unwrap()
                            .global_attributes
                            .keys()
                            .filter(|k| *k == v)
                            .count()
                            != 0
                    }))
            {
                return Err(LanguageError::from_span(
                    &i.span,
                    &*format!("Species '{}' has duplicate attribute names", i.name),
                    line!(),
                    file!(),
                ));
            }
        }
        Ok(used_names)
    }
}
