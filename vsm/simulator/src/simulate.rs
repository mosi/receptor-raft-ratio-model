use crate::flatten::*;
use crate::input::structures::PrimitiveTypes;
use rand::prelude::*;
use std::collections::HashMap;
use std::fs::*;
use std::io::*;
use std::time::Instant;

use indicatif::{ProgressBar, ProgressStyle};

pub struct Simulation {
    pub model: EnumaratedModel,
    pub target_time: f64,
    pub step_size: f64,

    pub output_file_name: String,
    pub reaction_count_file_name: Option<String>,
}

impl EnumeratedReaction {
    fn calc_propensity(
        &self,
        spec_counts: &HashMap<EnumeratedSpecies, usize>,
        global_vals: &HashMap<EnumeratedGlobalValue, PrimitiveTypes>,
        time: f64,
    ) -> f64 {
        if self.guard.iter().any(|g| {
            !g.eval(
                &AllOrSingleLeft::All(&self.species_in),
                Some(spec_counts),
                Some(global_vals),
                Some(time),
            )
        }) {
            return 0.0;
        }

        match &self.rate {
            EnumeratedRate::MassActionConstant(x) => self
                .species_in
                .iter()
                .enumerate()
                .map(|(n, (_, v))| {
                    match self
                        .species_in
                        .iter()
                        .take(n)
                        .filter(|(_, s)| *s == v)
                        .count()
                    {
                        0 => *spec_counts.get(v).unwrap(),
                        needed => {
                            let prensent = *spec_counts.get(v).unwrap();
                            if prensent < needed + 1 {
                                return 0;
                            } else {
                                return prensent - (needed);
                            }
                        }
                    }
                })
                .map(|count| count as f64)
                .fold(*x, |a, b| a * b),
            EnumeratedRate::ComplexExpression(expr) => {
                for (_, s) in self.species_in.iter() {
                    let n = self.species_in.iter().filter(|(_, ts)| *ts == s).count();
                    assert!(n > 0);
                    if spec_counts.get(s).unwrap() < &n {
                        return 0.0;
                    }
                }
                expr.eval_to_primitive(
                    &AllOrSingleLeft::All(&self.species_in),
                    Some(spec_counts),
                    Some(global_vals),
                    Some(time),
                )
                .unwrap()
                .as_float()
                .unwrap()
            }
            EnumeratedRate::MassActionExpression(y) => unreachable!(
                "{} Maybe parameters have not been made const?",
                y.eval_for_left_str(&AllOrSingleLeft::None, None, None, None)
            ),
        }
    }

    fn fire_reaction(
        &self,
        spec_counts: &mut HashMap<EnumeratedSpecies, usize>,
        global_values: &mut HashMap<EnumeratedGlobalValue, PrimitiveTypes>,
        time: f64,
    ) {
        /*self.species_in.iter().for_each(|v| {
        println!("{} {}",v.string_name(),spec_counts.get(v).unwrap());
        assert!(*spec_counts.get(v).unwrap() >= 1) });*/
        self.species_in
            .values()
            .for_each(|v| *spec_counts.get_mut(v).unwrap() -= 1);
        self.species_out
            .iter()
            .for_each(|v| *spec_counts.get_mut(v).unwrap() += 1);

        self.global_val_changes.iter().for_each(|(name, change)| {
            let new_val = match change {
                PrimitiveOrExpr::Primitive(x) => (*x).clone(),
                PrimitiveOrExpr::Expr(x) => x
                    .eval_to_primitive(
                        &AllOrSingleLeft::All(&self.species_in),
                        Some(spec_counts),
                        Some(global_values),
                        Some(time),
                    )
                    .unwrap(),
            };
            *global_values.get_mut(name).unwrap() = new_val;
        });

        for (from, to) in self.moving_rest_solutions.iter() {
            match to {
                None => panic!("Not implemented to remove compartment"),
                Some(to) => panic!("Not implemented to move rest solution"),
            }
        }
    }
}

impl Simulation {
    fn write_header(&self) {
        let mut file = match File::create(&self.output_file_name) {
            Err(why) => panic!("couldn't open {}: {}", self.output_file_name, why),
            Ok(file) => file,
        };
        write!(file, "Time,").unwrap();
        match &self.model.observations {
            None => self
                .model
                .species
                .iter()
                .for_each(|v| write!(file, "{},", v.string_name()).unwrap()),
            Some(obs) => obs
                .iter()
                .for_each(|(v, _)| write!(file, "{},", v).unwrap()),
        }
        self.model
            .global_values
            .iter()
            .for_each(|(v, _)| write!(file, "{},", v.string_name()).unwrap())
    }
    fn write_output(
        &self,
        time: &f64,
        spec_counts: &HashMap<EnumeratedSpecies, usize>,
        global_values: &HashMap<EnumeratedGlobalValue, PrimitiveTypes>,
    ) {
        let mut file = match OpenOptions::new().append(true).open(&self.output_file_name) {
            Err(why) => panic!("couldn't open {}: {}", self.output_file_name, why),
            Ok(file) => file,
        };
        write!(file, "\n{},", *time).unwrap();
        match &self.model.observations {
            None => self
                .model
                .species
                .iter()
                .map(|v| spec_counts.get(v).unwrap())
                .for_each(|n| write!(file, "{},", *n).unwrap()),
            Some(obs) => obs
                .iter()
                .map(|(_, s)| s.iter().map(|v| spec_counts.get(v).unwrap()).sum())
                .for_each(|n: usize| write!(file, "{},", n).unwrap()),
        }
        self.model
            .global_values
            .iter()
            .for_each(|(v, _)| write!(file, "{},", global_values.get(v).unwrap()).unwrap())
    }

    fn get_propensities(
        &self,
        spec_counts: &HashMap<EnumeratedSpecies, usize>,
        global_vals: &HashMap<EnumeratedGlobalValue, PrimitiveTypes>,
        time: f64,
    ) -> Vec<f64> {
        self.model
            .reactions
            .iter()
            .map(|v| v.calc_propensity(spec_counts, global_vals, time))
            .collect()
    }

    //#![allow(dead_code)]
    pub fn run(&self) {
        let now = Instant::now();
        // INITIALIZATIONS
        let _reac_countns = match self.reaction_count_file_name {
            Some(_) => vec![0; self.model.model.transitions.len()],
            None => vec![],
        };

        let mut species_counts: HashMap<EnumeratedSpecies, usize> = self
            .model
            .initial_species
            .clone()
            .into_iter()
            .map(|(a, b)| (b, a))
            .collect();
        self.model.species.iter().for_each(|v| {
            if !species_counts.contains_key(v) {
                species_counts.insert(v.clone(), 0);
            }
        });

        let mut global_attribute_values = self.model.global_values.clone();

        // Simulation loop
        let mut time = 0.0;

        let mut rng = thread_rng();

        let mut last_io = 0.0;

        self.write_header();
        self.write_output(&time, &species_counts, &global_attribute_values);

        let pb = ProgressBar::new(10000);
        pb.set_style(
            ProgressStyle::default_bar()
                .template(
                    "{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {percent}% ({eta})",
                )
                .progress_chars("#>-"),
        );
        let mut stepcount: usize = 0;
        while time < self.target_time {
            stepcount += 1;
            pb.set_position((10000. * time / (10000. * self.target_time) * 10000.) as u64);
            let propensities =
                self.get_propensities(&species_counts, &global_attribute_values, time);
            let prop_sum: f64 = propensities.iter().sum();

            if prop_sum <= 0.0 {
                println!("No reaction possible!");
                self.write_output(&time, &species_counts, &global_attribute_values);
                time = self.target_time;
                break;
            }

            let distr = rand_distr::Exp::new(prop_sum).unwrap();
            let timestep = distr.sample(&mut rng);

            if time + timestep > last_io + self.step_size {
                last_io += self.step_size;
                self.write_output(&last_io, &species_counts, &global_attribute_values);
            }
            time += timestep;
            let propensity_threshold = rng.gen_range(0.0, prop_sum);

            let idx_of_reaction = propensities
                .into_iter()
                .scan(0.0, |sum, p| {
                    *sum += p;
                    Some(*sum)
                })
                .position(|p| p > propensity_threshold)
                .expect("No reaction was selected");

            self.model.reactions[idx_of_reaction].fire_reaction(
                &mut species_counts,
                &mut global_attribute_values,
                time,
            );
        }
        pb.finish_with_message("Simulation complete");
        self.write_output(&time, &species_counts, &global_attribute_values);

        let t = now.elapsed().as_secs_f64();
        println!("Output written to '{}'", self.output_file_name);
        println!(
            "Done ({} steps in {}s -> throughput {}/s)",
            stepcount,
            t,
            stepcount as f64 / t
        );
    }
}
