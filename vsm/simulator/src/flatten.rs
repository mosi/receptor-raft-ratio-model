use crate::input::structures::*;
use itertools::Itertools;
//use std::collections::hash_map::DefaultHasher;
use std::collections::HashSet;
use std::collections::{BTreeMap, HashMap};
use std::fmt::Formatter;
//use std::hash::Hasher;
use std::{fmt, iter};

use serde::{Deserialize, Serialize};

use rayon::prelude::*;

#[derive(PartialEq, Eq, Debug, Hash, Clone, Serialize, Deserialize)]
pub struct CompartmentID {
    pub(crate) number: isize,
    pub(crate) species: String,
    pub next_parent: Option<Box<CompartmentID>>,
}
impl fmt::Display for CompartmentID {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.number {
            0 => write!(f, "{}", self.species)?,
            n => write!(f, "{}_{}", self.species, n)?,
        }
        match &self.next_parent {
            Some(v) => write!(f, "_in{}", *v),
            None => write!(f, ""),
        }
    }
}

#[derive(PartialEq, Eq, Debug, Hash, Clone, Serialize, Deserialize)]
pub struct EnumeratedSpecies {
    pub(crate) species_name: String,
    pub(crate) attribute_values: Vec<(String, PrimitiveTypes)>,
    pub parent_id: Option<CompartmentID>,
    pub my_id: Option<CompartmentID>,
    //pub(crate) my_flattend_idx: Option<usize>,
}
impl EnumeratedSpecies {
    pub fn sort(mut self) -> EnumeratedSpecies {
        self.attribute_values.sort_by(|(a, _), (b, _)| a.cmp(b));
        EnumeratedSpecies {
            species_name: self.species_name,
            attribute_values: self.attribute_values,
            parent_id: self.parent_id,
            my_id: self.my_id,
            //my_flattend_idx: None,
        }
    }
    pub fn get_attribute(&self, key: &String) -> PrimitiveTypes {
        assert_eq!(
            self.attribute_values
                .iter()
                .filter(|(k, _v)| *k == *key)
                .count(),
            1
        );
        self.attribute_values
            .iter()
            .filter(|(k, _v)| *k == *key)
            .next()
            .unwrap()
            .1
            .clone()
    }
}

impl RateExprTerm {
    pub fn _to_string_for_name(&self) -> String {
        match self {
            RateExprTerm::AttRef(_) => unreachable!(),
            RateExprTerm::Expr(_) => unreachable!("Expression not collapsed"),
            RateExprTerm::SpecCount(_) => unreachable!(),
            RateExprTerm::GlobSpecCount(_) => unimplemented!("Need to expand global count..."),

            RateExprTerm::SpecificCount(_) => unreachable!(),

            RateExprTerm::Primitive(p) => match p {
                PrimitiveTypes::String(s) => s.clone(),
                PrimitiveTypes::Boolean(b) => b.to_string(),
                PrimitiveTypes::Float(f) => f.to_string().replace(".", "d"),
                PrimitiveTypes::Integer(i) => i.to_string(),
            },
            RateExprTerm::GlobAttRef(_) => unimplemented!(),
            RateExprTerm::GlobalTime() => unreachable!("Time can not be in name!"),
            RateExprTerm::Parameter(x) => x.clone(),
        }
    }
}

pub enum StrOrPrimitive {
    Primitive(PrimitiveTypes),
    Str(String),
}

impl fmt::Display for StrOrPrimitive {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            StrOrPrimitive::Primitive(x) => write!(f, "{}", x),
            StrOrPrimitive::Str(x) => write!(f, "{}", x),
        }
    }
}

impl RateExpr {
    fn get_all_involved(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    ) -> HashSet<EnumeratedSpecies> {
        match &self.op {
            None => self.left.get_all_involved(left_match),
            Some((_, b)) => self
                .left
                .get_all_involved(left_match)
                .into_iter()
                .chain(b.get_all_involved(left_match).into_iter())
                .collect(),
        }
    }
    pub fn eval_for_left_str(
        &self,
        left_match: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
        time: Option<f64>,
    ) -> StrOrPrimitive {
        match &self.op {
            Some((op, term)) => {
                let r = term.eval_for_left_str(left_match, all_spec_counts, global_vals, time);
                match (
                    self.left
                        .eval_for_left_str(left_match, all_spec_counts, global_vals, time),
                    r,
                ) {
                    (StrOrPrimitive::Primitive(l), StrOrPrimitive::Primitive(r)) => {
                        StrOrPrimitive::Primitive(op.apply(&l, &r).unwrap())
                    }
                    (l, r) => StrOrPrimitive::Str(format!("({} {} {})", l, op, r)),
                }
            }

            None => self
                .left
                .eval_for_left_str(left_match, all_spec_counts, global_vals, time),
        }
    }
    pub fn eval_to_primitive(
        &self,
        left_match: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
        time: Option<f64>,
    ) -> Result<PrimitiveTypes, String> {
        match self.eval_for_left_str(left_match, all_spec_counts, global_vals, time) {
            StrOrPrimitive::Primitive(p) => Ok(p),
            StrOrPrimitive::Str(s) => Err(format!(
                "==> {}. That is not a static Constant. Did you mean to use a @@ maybe?",
                s
            )),
        }
    }

    fn eval_partial(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
        global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
    ) -> PrimitiveOrExpr {
        match &self.op {
            Some((op, term)) => {
                match (
                    self.left.eval_partial(left_match, global_vals),
                    term.eval_partial(left_match, global_vals),
                ) {
                    (PrimitiveOrExprTerm::Primitive(l), PrimitiveOrExprTerm::Primitive(r)) => {
                        PrimitiveOrExpr::Primitive(op.apply(&l, &r).unwrap())
                    }
                    (l, r) => PrimitiveOrExpr::Expr(RateExpr {
                        left: match l {
                            PrimitiveOrExprTerm::Primitive(x) => RateExprTerm::Primitive(x),
                            PrimitiveOrExprTerm::ExprTerm(x) => x,
                        },
                        op: Some((
                            op.clone(),
                            match r {
                                PrimitiveOrExprTerm::Primitive(x) => RateExprTerm::Primitive(x),
                                PrimitiveOrExprTerm::ExprTerm(x) => x,
                            },
                        )),
                    }),
                }
            }

            None => match self.left.eval_partial(left_match, global_vals) {
                PrimitiveOrExprTerm::Primitive(x) => PrimitiveOrExpr::Primitive(x),
                PrimitiveOrExprTerm::ExprTerm(x) => {
                    PrimitiveOrExpr::Expr(RateExpr { left: x, op: None })
                }
            },
        }
    }
}

impl RateExprTerm {
    fn get_all_involved(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    ) -> HashSet<EnumeratedSpecies> {
        let mut r = HashSet::new();
        match self {
            RateExprTerm::Primitive(_) => r,
            RateExprTerm::AttRef(_) => unreachable!(),
            RateExprTerm::Expr(y) => y.get_all_involved(left_match),
            RateExprTerm::SpecCount(x) => {
                r.insert(left_match.get(&x.idx_on_left).unwrap().clone());
                r
            }
            RateExprTerm::GlobSpecCount(_) => unreachable!(),
            RateExprTerm::SpecificCount(x) => x.iter().cloned().collect(),
            RateExprTerm::GlobAttRef(x) => {
                r.insert(
                    left_match
                        .get(&x.species_id.as_ref().unwrap().clone())
                        .unwrap()
                        .clone(),
                );
                r
            }
            RateExprTerm::GlobalTime() => r,
            RateExprTerm::Parameter(_) => r,
        }
    }
    fn eval_partial(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
        global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
    ) -> PrimitiveOrExprTerm {
        match self {
            RateExprTerm::Expr(x) => match x.eval_partial(left_match, global_vals) {
                PrimitiveOrExpr::Primitive(x) => PrimitiveOrExprTerm::Primitive(x),
                PrimitiveOrExpr::Expr(x) => {
                    PrimitiveOrExprTerm::ExprTerm(RateExprTerm::Expr(Box::new(x)))
                }
            },
            RateExprTerm::GlobSpecCount(_) => PrimitiveOrExprTerm::ExprTerm(self.clone()),
            RateExprTerm::SpecCount(_) => PrimitiveOrExprTerm::ExprTerm(self.clone()),

            RateExprTerm::Primitive(x) => PrimitiveOrExprTerm::Primitive(x.clone()),

            y => match self.eval_for_left_str(
                &AllOrSingleLeft::All(left_match),
                None,
                global_vals,
                None,
            ) {
                StrOrPrimitive::Primitive(x) => PrimitiveOrExprTerm::Primitive(x),
                StrOrPrimitive::Str(_) => {
                    PrimitiveOrExprTerm::ExprTerm(RateExprTerm::Expr(Box::new(RateExpr {
                        left: y.clone(),
                        op: None,
                    })))
                }
            }, /*RateExprTerm::AttRef(_) => {},
               RateExprTerm::SpecCount(_) => {},
               RateExprTerm::SpecificCount(_) => {},*/
        }
    }
    fn eval_for_left_str(
        &self,
        left_match: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
        time: Option<f64>,
    ) -> StrOrPrimitive {
        match self {
            RateExprTerm::AttRef(attref) => {
                StrOrPrimitive::Primitive(match (&attref.species_id, left_match) {
                    (None, AllOrSingleLeft::SingleLeft(sl)) => sl.get_attribute(&attref.att_name),
                    (Some(v), AllOrSingleLeft::All(hm)) => {
                        hm.get(v).unwrap().get_attribute(&attref.att_name)
                    }
                    _ => unreachable!(),
                })
            }
            RateExprTerm::GlobAttRef(attref) => match global_vals {
                None => StrOrPrimitive::Str(match (&attref.species_id, left_match) {
                    (None, AllOrSingleLeft::SingleLeft(sl)) => EnumeratedGlobalValue {
                        spec: (*sl).clone(),
                        name: attref.att_name.clone(),
                    }
                    .string_name(),
                    (Some(v), AllOrSingleLeft::All(hm)) => {
                        //println!("\n{:?}\n", hm);
                        EnumeratedGlobalValue {
                            spec: hm.get(v).unwrap().clone(),
                            name: attref.att_name.clone(),
                        }
                    }
                    .string_name(),
                    _ => unreachable!(),
                }),
                Some(vals) => StrOrPrimitive::Primitive(
                    vals.get(&match (&attref.species_id, left_match) {
                        (None, AllOrSingleLeft::SingleLeft(sl)) => EnumeratedGlobalValue {
                            spec: (*sl).clone(),
                            name: attref.att_name.clone(),
                        },

                        (Some(v), AllOrSingleLeft::All(hm)) => EnumeratedGlobalValue {
                            spec: hm.get(v).unwrap().clone(),
                            name: attref.att_name.clone(),
                        },
                        _ => unreachable!(),
                    })
                    .unwrap()
                    .clone(),
                ),
            },

            RateExprTerm::Expr(x) => {
                x.eval_for_left_str(left_match, all_spec_counts, global_vals, time)
            }
            RateExprTerm::SpecCount(sc) => match all_spec_counts {
                Some(all_counts) => {
                    StrOrPrimitive::Primitive(PrimitiveTypes::Integer(match left_match {
                        AllOrSingleLeft::All(x) => {
                            *all_counts.get(&x.get(&sc.idx_on_left).unwrap()).unwrap() as isize
                        }

                        AllOrSingleLeft::SingleLeft(_) => unreachable!(),
                        AllOrSingleLeft::None => unreachable!(),
                    }))
                }
                None => match left_match {
                    AllOrSingleLeft::All(x) => StrOrPrimitive::Str(
                        "#".to_string() + &x.get(&sc.idx_on_left).unwrap().string_name(),
                    ),

                    AllOrSingleLeft::SingleLeft(_) => unreachable!(),
                    AllOrSingleLeft::None => unreachable!(),
                },
            },
            RateExprTerm::Primitive(p) => StrOrPrimitive::Primitive(p.clone()),
            RateExprTerm::GlobSpecCount(_) => unreachable!(),
            RateExprTerm::SpecificCount(y) => match all_spec_counts {
                None => match y.is_empty() {
                    true => StrOrPrimitive::Primitive(PrimitiveTypes::Integer(0)),
                    false => StrOrPrimitive::Str(
                        y.iter()
                            .map(|v| "#".to_string() + &v.string_name())
                            .join(" + "),
                    ),
                },
                Some(vect) => StrOrPrimitive::Primitive(PrimitiveTypes::Integer(
                    y.iter().map(|v| vect.get(v).unwrap()).sum::<usize>() as isize,
                )),
            },
            RateExprTerm::GlobalTime() => match time {
                None => StrOrPrimitive::Str("__TIME__".to_string()),
                Some(t) => StrOrPrimitive::Primitive(PrimitiveTypes::Float(t)),
            },
            RateExprTerm::Parameter(x) => StrOrPrimitive::Str(x.clone()),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum EnumeratedRate {
    MassActionConstant(f64),
    MassActionExpression(RateExpr),
    ComplexExpression(RateExpr),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct EnumeratedReaction {
    pub reaction_number_of_prototype: usize,
    pub reaction_parent: Option<CompartmentID>,
    pub rate: EnumeratedRate,
    pub guard: Vec<LogicalConstraint>,
    pub species_in: BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    pub species_out: Vec<EnumeratedSpecies>,
    pub global_val_changes: HashMap<EnumeratedGlobalValue, PrimitiveOrExpr>,
    pub invokes_structural_changes: bool,
    //pub moving_rest_solutions: Vec<(CompartmentID, Option<CompartmentID>)>,
    is_not_mass_action: bool,
}
impl EnumeratedReaction {
    fn partial_guard_eval(mut self) -> Option<EnumeratedReaction> {
        if self
            .guard
            .iter()
            .any(|v| match v.eval_partial(&self.species_in, None) {
                BoolOrConstraint::Bool(x) => match x {
                    true => false,
                    false => true,
                },
                _ => false,
            })
        {
            return None;
        }
        self.guard = self
            .guard
            .iter()
            .filter_map(|v| match v.eval_partial(&self.species_in, None) {
                BoolOrConstraint::Bool(x) => match x {
                    true => None,
                    false => unreachable!("should be filtered"),
                },
                BoolOrConstraint::Constraint(x) => Some(x),
            })
            .collect();
        Some(self)
    }

    /* returns compartments, that are still needed */
    fn remove_unneded_compartments(
        &mut self,
        all_species: &HashMap<String, Species>,
    ) -> HashSet<EnumeratedSpecies> {
        let mut useed: HashSet<_> = self
            .guard
            .iter()
            .map(|x| {
                x.left
                    .get_all_involved(&self.species_in)
                    .into_iter()
                    .chain(x.right.get_all_involved(&self.species_in).into_iter())
            })
            .flatten()
            .collect();

        useed.extend(
            self.species_in
                .iter()
                .map(|(_, v)| v)
                .chain(self.species_out.iter())
                .filter(|v| {
                    !all_species
                        .get(&*v.species_name)
                        .unwrap()
                        .global_attributes
                        .is_empty()
                })
                .cloned(),
        );

        match &self.rate {
            EnumeratedRate::MassActionConstant(_) => {}
            EnumeratedRate::ComplexExpression(x) => {
                useed.extend(x.get_all_involved(&self.species_in).into_iter())
            }
            EnumeratedRate::MassActionExpression(_) => {}
        };

        let mut tmp = BTreeMap::new();
        core::mem::swap(&mut tmp, &mut self.species_in);

        self.species_in = tmp
            .into_iter()
            .filter(|(_, v)| {
                (!all_species.get(&*v.species_name).unwrap().is_compartment) || useed.contains(v)
            })
            .collect();
        self.species_out.retain(|v| {
            (!all_species.get(&*v.species_name).unwrap().is_compartment) || useed.contains(v)
        });

        self.species_in
            .iter()
            .map(|(_, b)| b)
            .filter(|v| all_species.get(&*v.species_name).unwrap().is_compartment)
            .chain(
                self.species_out
                    .iter()
                    .filter(|v| all_species.get(&*v.species_name).unwrap().is_compartment),
            )
            .cloned()
            .collect()

        //unimplemented!()
    }

    fn print_rate(&self) -> String {
        let guard_string = match &self.guard.is_empty() {
            true => "".to_string(),
            false => format!(
                "if ({}) then ",
                self.guard
                    .iter()
                    .map(|x| format!(
                        "{} {} {}",
                        x.left.eval_for_left_str(
                            &AllOrSingleLeft::All(&self.species_in),
                            None,
                            None,
                            None
                        ),
                        x.op,
                        x.right.eval_for_left_str(
                            &AllOrSingleLeft::All(&self.species_in),
                            None,
                            None,
                            None
                        )
                    ))
                    .join(" && ")
            ),
        };
        match &self.rate {
            EnumeratedRate::MassActionConstant(k) => format!(
                "{}{} {}",
                guard_string,
                k,
                self.species_in
                    .values()
                    .map(|v| " * #".to_string() + &*v.string_name())
                    .join("")
            )
            .to_string(),
            EnumeratedRate::ComplexExpression(y) => {
                guard_string
                    + y.eval_for_left_str(&AllOrSingleLeft::All(&self.species_in), None, None, None)
                        .to_string()
                        .as_str()
            }
            EnumeratedRate::MassActionExpression(ex) => format!(
                "{}{} {}",
                guard_string,
                ex.eval_for_left_str(&AllOrSingleLeft::All(&self.species_in), None, None, None)
                    .to_string()
                    .as_str(),
                self.species_in
                    .values()
                    .map(|v| " * #".to_string() + &*v.string_name())
                    .join("")
            )
            .to_string(),
        }
    }
    fn string_name(&self) -> String {
        //println!(" REACTION: {:#?}", self);
        let side_effect;
        if self.global_val_changes.is_empty() {
            side_effect = "".to_string();
        } else {
            side_effect = self
                .global_val_changes
                .iter()
                .map(|(a, b)| {
                    format!(
                        "| {} = {} ",
                        a.string_name(),
                        match b {
                            PrimitiveOrExpr::Primitive(x) =>
                                StrOrPrimitive::Primitive((*x).clone()),
                            PrimitiveOrExpr::Expr(x) => x.eval_for_left_str(
                                &AllOrSingleLeft::All(&self.species_in),
                                None,
                                None,
                                None
                            ),
                        }
                    )
                })
                .join("");
        }
        format!(
            "{} -> {} {}@ {} {};",
            self.species_in
                .values()
                .map(|v| v.string_name())
                .join(" + "),
            self.species_out.iter().map(|v| v.string_name()).join(" + "),
            side_effect,
            self.print_rate(),
            match self.invokes_structural_changes {
                true => ">>INVOKE STRUCTURAL CHANGES<<",
                false => "",
            },
        )
    }
}

fn get_id_in_initial(initial: &Vec<SpeciesRight>, idx: usize) -> CompartmentID {
    let species_looking_for = &initial[idx].species_name;
    let count = initial
        .iter()
        .take(idx)
        .filter(|v2| v2.species_name == *species_looking_for)
        .count() as isize;
    CompartmentID {
        number: count,
        species: species_looking_for.clone(),
        next_parent: match initial[idx].compartment_constraint {
            -1 => None,
            x => Some(Box::new(get_id_in_initial(initial, x as usize))),
        },
    }
}

impl EnumeratedSpecies {
    pub fn string_name(&self) -> String {
        format!(
            "{}{}{}{}{}",
            self.species_name.clone(),
            match &self.my_id {
                None => "".to_string(),
                Some(x) => format!(
                    "{}",
                    match x.number {
                        0 => "".to_string(),
                        n => format!("_{}", n).to_string(),
                    }
                ),
            },
            match self.attribute_values.is_empty() {
                true => format!(""),
                false => format!("_"),
            },
            self.attribute_values
                .iter()
                .map(|(name, val)| format!("_{}_{}", name, val.to_string()))
                .join("_"),
            match &self.parent_id {
                None => "".to_string(),
                Some(x) => format!("_in{}", x),
            }
        )
    }
    fn single_from_species_right_initial(
        v: &SpeciesRight,
        model: &Model,
        compartment_counter: &mut HashMap<String, usize>,
    ) -> EnumeratedSpecies {
        {
            let parent_id = match v.compartment_constraint {
                -1 => None,
                x => {
                    assert!(
                        model
                            .species
                            .get(&*model.inital[x as usize].species_name)
                            .unwrap()
                            .is_compartment
                    );
                    Some(get_id_in_initial(&model.inital, x as usize))
                }
            };
            //println!("{:?} -> {:?}",parent_id,v);
            let boxed_parent = match &parent_id {
                Some(c) => Some(Box::new(c.clone())),
                None => None,
            };
            EnumeratedSpecies {
                //my_flattend_idx: None,
                species_name: v.species_name.clone(),
                attribute_values: v
                    .local_attribute_assignment
                    .iter()
                    .map(|(n, v)| {
                        (
                            n.clone(),
                            v.eval_to_primitive(&AllOrSingleLeft::None, None, None, None)
                                .unwrap(),
                        )
                    })
                    .collect(),
                parent_id: parent_id.clone(),
                my_id: match model.species.get(&*v.species_name).unwrap().is_compartment {
                    true => match compartment_counter.get_mut(&*v.species_name) {
                        None => {
                            compartment_counter.insert(v.species_name.clone(), 1);
                            Some(CompartmentID {
                                number: 0,
                                species: v.species_name.clone(),
                                next_parent: boxed_parent,
                            })
                        }
                        Some(k) => {
                            *k += 1;
                            Some(CompartmentID {
                                number: *k as isize - 1,
                                species: v.species_name.clone(),
                                next_parent: boxed_parent,
                            })
                        }
                    },
                    false => None,
                },
            }
            .sort()
        }
    }
}

pub enum AllOrSingleLeft<'a> {
    All(&'a BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>),
    SingleLeft(&'a EnumeratedSpecies),
    None,
}

enum BoolOrConstraint {
    Bool(bool),
    Constraint(LogicalConstraint),
}

#[derive(Debug, Serialize, Deserialize)]
pub enum PrimitiveOrExpr {
    Primitive(PrimitiveTypes),
    Expr(RateExpr),
}

enum PrimitiveOrExprTerm {
    Primitive(PrimitiveTypes),
    ExprTerm(RateExprTerm),
}

impl LogicalConstraint {
    pub fn eval(
        &self,
        all_or_single_left: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
        time: Option<f64>,
    ) -> bool {
        let l = self
            .left
            .eval_to_primitive(all_or_single_left, all_spec_counts, global_vals, time)
            .unwrap();
        let r = self
            .right
            .eval_to_primitive(all_or_single_left, all_spec_counts, global_vals, time)
            .unwrap();

        match (l, r) {
            (PrimitiveTypes::Integer(l), PrimitiveTypes::Integer(r)) => self.op.apply_all(&l, &r),
            (PrimitiveTypes::Float(l), PrimitiveTypes::Float(r)) => self.op.apply_all(&l, &r),
            (PrimitiveTypes::Float(l), PrimitiveTypes::Integer(r)) => {
                self.op.apply_all(&l, &(r as f64))
            }
            (PrimitiveTypes::Integer(l), PrimitiveTypes::Float(r)) => {
                self.op.apply_all(&(l as f64), &r)
            }
            (PrimitiveTypes::Boolean(l), PrimitiveTypes::Boolean(r)) => self.op.apply_eq(&l, &r),
            (PrimitiveTypes::String(l), PrimitiveTypes::String(r)) => self.op.apply_eq(&l, &r),
            _ => unreachable!(),
        }
    }

    fn eval_partial(
        &self,
        matched_left: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
        global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
    ) -> BoolOrConstraint {
        let l = self.left.eval_partial(matched_left, global_vals);
        let r = self.right.eval_partial(matched_left, global_vals);
        match (l, r) {
            (PrimitiveOrExpr::Primitive(l), PrimitiveOrExpr::Primitive(r)) => {
                BoolOrConstraint::Bool(match (l, r) {
                    (PrimitiveTypes::Integer(l), PrimitiveTypes::Integer(r)) => {
                        self.op.apply_all(&l, &r)
                    }
                    (PrimitiveTypes::Float(l), PrimitiveTypes::Float(r)) => {
                        self.op.apply_all(&l, &r)
                    }
                    (PrimitiveTypes::Float(l), PrimitiveTypes::Integer(r)) => {
                        self.op.apply_all(&l, &(r as f64))
                    }
                    (PrimitiveTypes::Integer(l), PrimitiveTypes::Float(r)) => {
                        self.op.apply_all(&(l as f64), &r)
                    }
                    (PrimitiveTypes::Boolean(l), PrimitiveTypes::Boolean(r)) => {
                        self.op.apply_eq(&l, &r)
                    }
                    (PrimitiveTypes::String(l), PrimitiveTypes::String(r)) => {
                        self.op.apply_eq(&l, &r)
                    }
                    _ => unreachable!(),
                })
            }
            (l, r) => BoolOrConstraint::Constraint(LogicalConstraint {
                left: match l {
                    PrimitiveOrExpr::Primitive(x) => RateExpr {
                        left: RateExprTerm::Primitive(x),
                        op: None,
                    },
                    PrimitiveOrExpr::Expr(x) => x,
                },
                op: self.op.clone(),
                right: match r {
                    PrimitiveOrExpr::Primitive(x) => RateExpr {
                        left: RateExprTerm::Primitive(x),
                        op: None,
                    },
                    PrimitiveOrExpr::Expr(x) => x,
                },
            }),
        }
    }
}

#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct EnumeratedGlobalValue {
    pub spec: EnumeratedSpecies,
    pub name: String,
}

impl EnumeratedGlobalValue {
    pub fn string_name(&self) -> String {
        self.spec.string_name() + "_" + &self.name
    }
}

#[derive(Serialize, Deserialize)]
pub struct EnumaratedModel {
    pub model: Model,
    pub species: Vec<EnumeratedSpecies>,
    pub reactions: Vec<EnumeratedReaction>,
    pub initial_species: HashMap<EnumeratedSpecies, usize>,
    pub observations: Option<Vec<(String, Vec<EnumeratedSpecies>)>>,
    pub global_values: HashMap<EnumeratedGlobalValue, PrimitiveTypes>,
}

pub struct NewRightAttributes {
    pub(crate) local: Vec<(String, PrimitiveTypes)>,
    pub global: Vec<(String, PrimitiveOrExpr)>,
}

pub fn new_right_attributes(
    s_right: &SpeciesRight,
    left_side: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    all_species: &HashMap<String, Species>,
) -> NewRightAttributes {
    let mut ret: Vec<(String, PrimitiveTypes)> = s_right
        .local_attribute_assignment
        .iter()
        .map(|(n, expr)| {
            (
                n.clone(),
                expr.eval_to_primitive(&AllOrSingleLeft::All(left_side), None, None, None)
                    .unwrap(),
            )
        })
        .collect();
    if ret.len()
        != all_species
            .get(&*s_right.species_name)
            .unwrap()
            .attributes
            .len()
    {
        for att in all_species
            .get(&*s_right.species_name)
            .unwrap()
            .attributes
            .keys()
        {
            if ret
                .iter()
                .map(|(a, _b)| a)
                .filter(|name| *name == att)
                .count()
                == 0
            {
                //print!("\nleft: {:#?}",left_side);
                ret.push((
                    att.clone(),
                    left_side
                        .get(s_right.corresponding_on_left.as_ref().unwrap())
                        .unwrap()
                        .get_attribute(att),
                ))
            }
        }
        assert!(
            ret.len()
                == all_species
                    .get(&*s_right.species_name)
                    .unwrap()
                    .attributes
                    .len()
        );
    }

    let global_asign: Vec<_> = s_right
        .global_attribute_assignment
        .iter()
        .map(|(n, expr)| (n.clone(), expr.eval_partial(left_side, None)))
        .collect();

    NewRightAttributes {
        local: ret,
        global: global_asign,
    }
}

impl GlobalCount {
    fn get_species_to_count(
        &self,
        possible: &Vec<EnumeratedSpecies>,
        //left_side: Option<&Vec<EnumeratedSpecies>>,
    ) -> Vec<EnumeratedSpecies> {
        assert!(!self.definition.is_empty());
        let num_of_last = self.definition.len() - 1;
        let mut def: Vec<_> = self
            .definition
            .iter()
            .enumerate()
            .map(|(n, k)| {
                let parent_id;
                if n == num_of_last {
                    parent_id = None;
                } else {
                    parent_id = Some(SpeciesLeftIdentifier::new(&*format!("__{}", n + 1)));
                }
                match k {
                    CountingElement::_SpecificName(_) => unreachable!(),
                    CountingElement::GeneralDescription(a, b) => SpeciesLeft {
                        species_name: a.clone(),
                        id: SpeciesLeftIdentifier::new(&*format!("__{}", n)),
                        parent_number: parent_id,
                        attribute_constraint: b.clone(),
                    },
                }
            })
            .collect();
        def.reverse();

        match find_matches(&def, possible) {
            None => vec![],
            Some(x) => x.into_iter().fold(vec![], |mut acc, x| {
                assert_eq!(x.len(), self.definition.len());
                acc.push(
                    x.get(&SpeciesLeftIdentifier::new(&*format!("__{}", 0)))
                        .unwrap()
                        .clone(),
                );
                acc
            }),
        }
    }
}
pub fn replace_global_counts_in_rate_term(
    all_species: &Vec<EnumeratedSpecies>,
    ex: RateExprTerm,
) -> RateExprTerm {
    match ex {
        RateExprTerm::Expr(x) => RateExprTerm::Expr(Box::new(replace_global_counts_in_rate_expr(
            all_species,
            *x,
        ))),
        RateExprTerm::GlobSpecCount(y) => {
            RateExprTerm::SpecificCount(y.get_species_to_count(all_species))
        }
        _ => return ex,
    }
}

pub fn replace_global_counts_in_rate_expr(
    all_species: &Vec<EnumeratedSpecies>,
    ex: RateExpr,
) -> RateExpr {
    RateExpr {
        left: replace_global_counts_in_rate_term(all_species, ex.left),
        op: match ex.op {
            Some((a, b)) => Some((a, replace_global_counts_in_rate_term(all_species, b))),
            None => None,
        },
    }
}

fn matches_description(
    to_test: &EnumeratedSpecies,
    description: &SpeciesLeft,
    partial_result: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    desci: &Vec<SpeciesLeft>,
) -> bool {
    /*|to_test|*/
    match &description.parent_number {
        None => match partial_result.get(
            &desci
                .iter()
                .find(|v| v.parent_number.is_none())
                .as_ref()
                .unwrap()
                .id,
        ) {
            Some(x) => x.parent_id == to_test.parent_id,
            None => true, // To_test is the first with parent id
        },
        Some(p) => match to_test.parent_id.as_ref() {
            Some(pid) => {
                *pid == *partial_result
                    .get(p)
                    .expect("not in partial")
                    .my_id
                    .as_ref()
                    .expect("no my id")
            }
            None => false,
        },
    }
}

fn find_matches(
    desci: &Vec<SpeciesLeft>,
    available: &Vec<EnumeratedSpecies>,
) -> Option<Vec<BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>>> {
    if desci.len() == 0 {
        return Some(vec![Default::default()]);
    }
    let matching_wo_nesting: Vec<Vec<&EnumeratedSpecies>> = desci
        .par_iter()
        .map(|lf| {
            available
                .par_iter()
                // Check Spezies name
                .filter(move |k| {
                    (k.species_name == lf.species_name)
                        && lf
                            .attribute_constraint
                            .iter()
                            .all(|v| v.eval(&AllOrSingleLeft::SingleLeft(k), None, None, None))
                })
                .collect()
        })
        .collect();

    let all_result = desci.iter().zip(matching_wo_nesting).fold(
        vec![Default::default()],
        |matches_found_so_far: Vec<BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>>,
         (description, iters)| {
            matches_found_so_far
                .into_par_iter()
                .map(|partial_result| {
                    let my_res: Vec<_> = iters
                        .iter()
                        .filter(|to_test| {
                            matches_description(to_test, description, &partial_result, desci)
                        })
                        .map(|gm| {
                            let mut my_partial_result = partial_result.clone();
                            assert!(my_partial_result
                                .insert(description.id.clone(), (*gm).clone())
                                .is_none());
                            //res.push(my_partial_result);
                            my_partial_result
                        })
                        .collect();
                    my_res.into_iter()
                })
                .flatten_iter() //_iter()
                .collect()
        },
    );
    match all_result.is_empty() {
        true => None,
        false => Some(all_result),
    }
}

impl EnumaratedModel {
    pub fn from_model(model: Model) -> EnumaratedModel {
        //println!("Species: {:#?}", model.inital);
        let mut compartment_counter = HashMap::new();
        let species: Vec<(usize, EnumeratedSpecies)> = model
            .inital
            .iter()
            .map(|v| {
                (
                    v.amount,
                    EnumeratedSpecies::single_from_species_right_initial(
                        v,
                        &model,
                        &mut compartment_counter,
                    ),
                )
            })
            .collect();
        let mut global_values = HashMap::new();

        model
            .inital
            .iter()
            .zip(species.iter())
            .for_each(|(m, (_amt, en))| {
                assert_eq!(m.species_name, en.species_name);
                m.global_attribute_assignment.iter().for_each(|(n, v)| {
                    assert!(model.species.get(&*m.species_name).unwrap().is_compartment);
                    assert!(global_values
                        .insert(
                            EnumeratedGlobalValue {
                                spec: en.clone(),
                                name: n.clone(),
                            },
                            v.eval_to_primitive(&AllOrSingleLeft::None, None, None, None)
                                .unwrap(),
                        )
                        .is_none());
                });
            });
        EnumaratedModel {
            initial_species: species
                .iter()
                .map(|(num, spec)| (spec.clone(), *num))
                .collect(),
            species: species.iter().map(|(_, b)| b).cloned().collect(),
            observations: None, // can only be prepared once the model is expanded
            reactions: vec![],
            model: model,
            global_values: global_values,
        }
    }

    pub fn replace_global_counts(&mut self) {
        let all_spec = self.species.clone();
        self.reactions.iter_mut().for_each(|r| {
            r.guard = r
                .guard
                .iter()
                .map(|x| LogicalConstraint {
                    left: replace_global_counts_in_rate_expr(&all_spec, x.left.clone()),
                    op: x.op.clone(),
                    right: replace_global_counts_in_rate_expr(&all_spec, x.right.clone()),
                })
                .collect();
        });
    }

    pub fn expantion_step(&mut self) {
        let reac_prototypes = self.model.transitions.clone();

        self.reactions = self
            .model
            .transitions
            .par_iter()
            .map(|t| find_matches(&t.left, &self.species))
            .enumerate()
            .filter(|(_n, k)| k.is_some())
            // find matches based on species and attributes
            .map(|(n, matches)| {
                matches
                    .unwrap()
                    .into_par_iter()
                    .map(move |v| (v, n))
                    .filter_map(|(matched_left, n2)| {
                        let reaction_parent = match matched_left.get(
                            &self.model.transitions[n2]
                                .left
                                .iter()
                                .map(|v| &v.id)
                                .next()
                                .unwrap_or(&SpeciesLeftIdentifier::new_dummy()), //new("__DUMMY !! DUMMY")),
                        ) {
                            None => None,
                            Some(x) => x.parent_id.clone(),
                        };
                        assert_eq!(matched_left.len(), reac_prototypes[n2].left.len());
                        let mut global_assign = HashMap::new();
                        EnumeratedReaction {
                            reaction_number_of_prototype: n2,
                            reaction_parent: reaction_parent.clone(),
                            species_out: if reac_prototypes[n2].structural_changes.is_empty() {
                                reac_prototypes[n2]
                                    .right
                                    .iter()
                                    .map(|v| {
                                        let attrs = new_right_attributes(
                                            v,
                                            &matched_left,
                                            &self.model.species,
                                        );
                                        let enum_spec = EnumeratedSpecies {
                                            species_name: v.species_name.clone(),
                                            attribute_values: attrs.local,
                                            parent_id: match v.compartment_constraint {
                                                -1 => reaction_parent.clone(),
                                                x => Some(
                                                    matched_left
                                                        .get(
                                                            &reac_prototypes[n2].right[x as usize]
                                                                .corresponding_on_left
                                                                .as_ref()
                                                                .expect(
                                                                    "Corresponding left not found!",
                                                                ),
                                                        )
                                                        .as_ref()
                                                        .unwrap()
                                                        .my_id
                                                        .as_ref()
                                                        .expect("Corresponding had no ID")
                                                        .clone(),
                                                ),
                                            },

                                            my_id: match &v.corresponding_on_left {
                                                None => None,
                                                Some(left_idx) => matched_left
                                                    .get(left_idx)
                                                    .as_ref()
                                                    .expect("not found on left")
                                                    .my_id
                                                    .clone(), // may be none, for not compartment
                                            },
                                            //my_flattend_idx: None,
                                        }
                                        .sort();
                                        global_assign.extend(attrs.global.into_iter().map(
                                            |(n, v)| {
                                                (
                                                    EnumeratedGlobalValue {
                                                        spec: enum_spec.clone(),
                                                        name: n,
                                                    },
                                                    v,
                                                )
                                            },
                                        ));
                                        iter::repeat(enum_spec).take(v.amount)
                                    })
                                    .flatten()
                                    .collect()
                            } else {
                                vec![]
                            },

                            rate: match !reac_prototypes[n2].is_not_mass_action {
                                true => {
                                    match reac_prototypes[n2].rate_expr.eval_to_primitive(
                                        &AllOrSingleLeft::All(&matched_left),
                                        None,
                                        None,
                                        None,
                                    ) {
                                        Ok(r) => {
                                            let r = r.as_float().unwrap();
                                            assert!(r >= 0.);
                                            EnumeratedRate::MassActionConstant(r)
                                        }
                                        Err(_y) => EnumeratedRate::MassActionExpression(
                                            reac_prototypes[n2].rate_expr.clone(),
                                        ),
                                    }
                                }
                                false => EnumeratedRate::ComplexExpression(
                                    match reac_prototypes[n2]
                                        .rate_expr
                                        .clone()
                                        .eval_partial(&matched_left, None)
                                    {
                                        PrimitiveOrExpr::Primitive(k) => RateExpr {
                                            left: RateExprTerm::Primitive(k),
                                            op: None,
                                        },
                                        PrimitiveOrExpr::Expr(x) => x,
                                    },
                                ),
                            },
                            guard: reac_prototypes[n2].guard.clone(),
                            invokes_structural_changes: !reac_prototypes[n2]
                                .structural_changes
                                .is_empty(),
                            species_in: matched_left,
                            is_not_mass_action: reac_prototypes[n2].is_not_mass_action,
                            global_val_changes: global_assign,
                        }
                        .partial_guard_eval()
                    })
            })
            .flatten()
            .collect();

        let species_set: HashSet<&EnumeratedSpecies> = self
            .reactions
            .par_iter()
            .map(|v| v.species_in.values().chain(v.species_out.iter()))
            .flatten_iter()
            .chain(self.species.par_iter())
            .collect();
        self.species = species_set.into_iter().cloned().collect();
    }

    pub fn flatten_finalize(&mut self) {
        self.replace_global_counts();
        let the_species = self.model.species.clone();
        /*self.species
            .retain(|v| !the_species.get(&*v.species_name).unwrap().is_compartment);

        self.initial_species
            .retain(|(_, v)| !the_species.get(&*v.species_name).unwrap().is_compartment);
        */

        if self
            .model
            .transitions
            .iter()
            .all(|reac| reac.structural_changes.is_empty())
        {
            let used_compartments: HashSet<_> = self
                .reactions
                .iter_mut()
                .map(|v| v.remove_unneded_compartments(&the_species))
                .flatten()
                .collect();

            self.species.retain(|v| {
                (!the_species.get(&*v.species_name).unwrap().is_compartment)
                    || used_compartments.contains(v)
            });

            self.initial_species.retain(|spec, _num| {
                (!the_species.get(&*spec.species_name).unwrap().is_compartment)
                    || used_compartments.contains(spec)
            });
        }

        self.species
            .sort_by(|a, b| a.species_name.partial_cmp(&b.species_name).unwrap());

        /*self.reactions
            .iter_mut()
            .for_each(|r| r.remove_all_compartments());

        self.reactions.sort_by(|a, b| {
            a.reaction_number_of_prototype
                .partial_cmp(&b.reaction_number_of_prototype)
                .unwrap()
        });*/

        self.observations = match &self.model.observations {
            None => None,
            Some(o) => Some(
                o.iter()
                    .map(|v| (v.name.clone(), v.get_species_to_count(&self.species)))
                    .collect(),
            ),
        };
    }

    pub fn flatten_a(&mut self, max_steps: usize) {
        println!("-- Expanding the system");
        /*println!(
            "--Initial: {:?}",
            self.species
                .iter()
                .map(|v| v.string_name())
                .collect::<Vec<_>>()
        );*/
        /* println!(
            "--Initial: {:#?}",
            self.species
                .iter()
                .map(|v| format!("{:?}",v))
                .collect::<Vec<_>>()
        );*/
        assert!(max_steps >= 2);
        for i in 1..=max_steps {
            let old_length = self.species.len();
            self.expantion_step();
            /*println!(
                "\nIteration {}: {:?}", // : {:?}",
                i,
                self.species
                    .iter()
                    .map(|v| v.string_name())
                    .collect::<Vec<_>>(),
                //self.reactions
            );*/
            
            println!(
                "   Iteration {}: {} species, {} reactions",
                i,
                self.species.len(),
                self.reactions.len()
            );

            if old_length == self.species.len() {
                println!("-- Expantion successfull");
                break;
            }
            if i == max_steps {
                println!("WARNING: Model Expantion not complete after {} steps. You can try more steps, but the Simulation will not be correct as is!!",max_steps);
            }
        }
        self.flatten_finalize();
    }

    pub fn print_model(&self) -> String {
        let initial_names: HashSet<_> = self.initial_species.keys().collect();
        let spec1 = self
            .initial_species
            .iter()
            .map(|(v, k)| v.string_name() + "(" + &*k.to_string() + ");\n")
            .join("");
        /*println!("{:?}\n{:?}",initial_names,self
        .species
        .iter()
        .filter(|n| !initial_names.contains(n)));*/

        /*let spec = self
        .species
        .iter()
        .filter(|n| !initial_names.contains(n))
        .map(|v| format!("{}();\n", v.string_name()))
        .join("");*/
        let mut spec = "".to_string();
        for (s, _s_def) in self.model.species.iter() {
            spec += &*format!(
                "\n// {}\n{}",
                s,
                self.species
                    .iter()
                    .filter(|n| n.species_name == *s)
                    .map(|v| format!(
                        "{}{}();\n",
                        match initial_names.contains(&v) {
                            true => "// (is initial) ",
                            false => "",
                        },
                        v.string_name()
                    ))
                    .join("")
            );
        }

        let mut reactions = "".to_string();
        for i in 0..self.model.transitions.len() {
            reactions += &*format!(
                "\n\n// Reactions based on rule {}\n// {}\n{}",
                i + 1,
                self.model.transitions[i].original_string,
                self.reactions
                    .iter()
                    .filter(|v| v.reaction_number_of_prototype == i)
                    .map(|v| v.string_name())
                    .join("\n")
            );
        }
        let obs = match &self.observations {
            None => "".to_string(),
            Some(v) => {
                "\nOBSERVATIONS\n".to_string()
                    + &*v
                        .iter()
                        .map(|(n, list)| {
                            format!(
                                "\"{}\" = {}",
                                n.replace("\"", ""),
                                list.iter().map(|s| s.string_name()).join(" + ")
                            )
                        })
                        .join("\n")
            }
        };
        let glob_var;
        if self.global_values.is_empty() {
            glob_var = "".to_string();
        } else {
            glob_var = "\nVARIABLES\n".to_string()
                + &self
                    .global_values
                    .iter()
                    .map(|(a, b)| format!("{}  = {};\n", a.string_name(), b))
                    .join("")
        }
        let constant_defs = self
            .model
            .parameters
            .iter()
            .map(|(n, v)| {
                format!(
                    "const {} = {};",
                    n,
                    v.eval_for_left_str(&AllOrSingleLeft::None, None, None, None)
                )
            })
            .join("\n");
        format!(
            "\nCONSTANTS\n////some constants are already replaced\n{}\n{}\nSPECIES\n{}\n\n{}\nRULES{}\n{}\n",
            constant_defs,
            glob_var,spec1, spec, reactions, obs
        )
    }
}
