use crate::flatten::{new_right_attributes, CompartmentID, EnumaratedModel, EnumeratedSpecies};
use crate::input::structures::{PrimitiveTypes, StructuralChange};
use crate::simulate_fast::SimulationFast;
use std::collections::{HashMap, HashSet};
use std::mem;

pub fn _remove_incl_children<F>(
    species_vector: &mut Vec<EnumeratedSpecies>,
    initial: &mut HashMap<EnumeratedSpecies, usize>,
    f: F,
) -> usize
where
    F: Fn(&EnumeratedSpecies) -> bool,
{
    let mut tmp: Vec<EnumeratedSpecies> = vec![];
    mem::swap(&mut tmp, species_vector);
    let (res_left, removed): (Vec<EnumeratedSpecies>, Vec<EnumeratedSpecies>) =
        tmp.into_iter().partition(|v| !f(v));

    species_vector.extend(res_left.into_iter());

    for i in removed.iter() {
        initial.remove(&i);
    }

    let mut num_removed = removed.len();
    let removed_compartments = removed.into_iter().filter(|v| v.my_id.is_some());
    let removed_compartments = removed_compartments.inspect(
        |_k| {}, // TODO check for global/star attributes
    );
    removed_compartments.for_each(|v| {
        let looking_for_id = v.my_id.as_ref().unwrap();
        num_removed += _remove_incl_children2(species_vector, initial, |k| match &k.parent_id {
            None => false,
            Some(parent_id) => *parent_id == *looking_for_id,
        })
    });

    num_removed
}

pub fn _remove_incl_children2<F>(
    species_vector: &mut Vec<EnumeratedSpecies>,
    initial: &mut HashMap<EnumeratedSpecies, usize>,
    f: F,
) -> usize
where
    F: Fn(&EnumeratedSpecies) -> bool,
{
    let mut tmp: Vec<EnumeratedSpecies> = vec![];
    mem::swap(&mut tmp, species_vector);
    let (res_left, removed): (Vec<EnumeratedSpecies>, Vec<EnumeratedSpecies>) =
        tmp.into_iter().partition(|v| !f(v));

    species_vector.extend(res_left.into_iter());

    for i in removed.iter() {
        initial.remove(&i);
    }

    let mut num_removed = removed.len();
    let removed_compartments = removed.into_iter().filter(|v| v.my_id.is_some());
    let removed_compartments = removed_compartments.inspect(
        |_k| {}, // TODO check for global/star attributes
    );
    removed_compartments.for_each(|v| {
        let looking_for_id = v.my_id.as_ref().unwrap();
        num_removed += _remove_incl_children3(species_vector, initial, |k| match &k.parent_id {
            None => false,
            Some(parent_id) => *parent_id == *looking_for_id,
        })
    });

    num_removed
}
pub fn _remove_incl_children3<F>(
    species_vector: &mut Vec<EnumeratedSpecies>,
    initial: &mut HashMap<EnumeratedSpecies, usize>,
    f: F,
) -> usize
where
    F: Fn(&EnumeratedSpecies) -> bool,
{
    let mut tmp: Vec<EnumeratedSpecies> = vec![];
    mem::swap(&mut tmp, species_vector);
    let (res_left, removed): (Vec<EnumeratedSpecies>, Vec<EnumeratedSpecies>) =
        tmp.into_iter().partition(|v| !f(v));

    species_vector.extend(res_left.into_iter());

    for i in removed.iter() {
        initial.remove(&i);
    }

    let num_removed = removed.len();
    let removed_compartments = removed.into_iter().filter(|v| v.my_id.is_some());
    let removed_compartments = removed_compartments.inspect(
        |_k| {}, // TODO check for global/star attributes
    );
    removed_compartments.for_each(|_v| panic!("Nesting to deep!"));

    num_removed
}

impl EnumaratedModel {
    fn _check_all_initial_species(&self) {
        let all_spec: HashSet<_> = self.species.iter().collect();
        for (s, _) in self.initial_species.iter() {
            if !all_spec.contains(s) {
                panic!("{} is gone", s.string_name());
            }
        }
    }
    fn _any_parent_is(
        &self,
        spec: &EnumeratedSpecies,
        looking_for_parent_id: &CompartmentID,
    ) -> bool {
        match &spec.parent_id {
            None => false,
            Some(y) => {
                if y == looking_for_parent_id {
                    true
                } else {
                    match self.species.iter().find(|k| match &k.my_id {
                        None => false,
                        Some(next_level_parent) => *next_level_parent == *y,
                    }) {
                        None => unreachable!("Could not find parent? But it must exist!"),
                        Some(parent_spec) => {
                            self._any_parent_is(parent_spec, looking_for_parent_id)
                        }
                    }
                }
            }
        }
    }
}

pub fn change_structure_and_reexpand(
    mut model: EnumaratedModel,
    idx_of_reaction: usize,
    max_steps: usize,
) -> EnumaratedModel {
    let old_number_of_species = model.species.len();
    let enum_reac = &model.reactions[idx_of_reaction];
    let prototype = &model.model.transitions[enum_reac.reaction_number_of_prototype];
    /*println!(
        "BEFORE {}",
        model.species.iter().map(|v| v.string_name()).join(" & ")
    );*/

    model.initial_species.retain(|_, n| *n > 0);
    assert!(model.initial_species.iter().all(|(_, n)| *n > 0));

    for (s, _) in model.initial_species.iter() {
        assert!(model.species.contains(s));
    }
    // removing left ones

    // Adding all new ones
    let mut ids_of_newly_added: Vec<Option<CompartmentID>> = vec![None; prototype.right.len()];
    for (num, r) in prototype
        .right
        .iter()
        .enumerate()
        .filter(|(_, k)| k.corresponding_on_left.is_none())
    {
        let parent_id = match r.compartment_constraint {
            -1 => enum_reac.reaction_parent.clone(),
            x => Some(
                match &prototype.right[x as usize].corresponding_on_left.as_ref() {
                    None => {
                        assert!((x as usize) < num);
                        ids_of_newly_added[x as usize].as_ref().unwrap().clone()
                    }
                    Some(cores_left_id) => enum_reac
                        .species_in
                        .get(*cores_left_id)
                        .as_ref()
                        .unwrap()
                        .my_id
                        .as_ref()
                        .expect("Corresponding had no ID")
                        .clone(),
                },
            ),
        };
        let attrs = new_right_attributes(r, &enum_reac.species_in, &model.model.species);
        let enum_spec = EnumeratedSpecies {
            species_name: r.species_name.clone(),
            attribute_values: attrs.local,
            parent_id: parent_id.clone(),

            my_id: match &r.corresponding_on_left {
                None => {
                    if model
                        .model
                        .species
                        .get(&r.species_name)
                        .unwrap()
                        .is_compartment
                    {
                        // TODO: this is VERY slow and inefficent. Better to store total numbers
                        let used_ids: HashSet<_> = model
                            .initial_species
                            .iter()
                            .filter(|(k, _)| k.species_name == r.species_name)
                            .filter_map(|(k, _)| k.my_id.as_ref())
                            .map(|k| k.number)
                            .collect();

                        // find first idnot used in model
                        let new_id = (0..=used_ids.len() as isize)
                            .find(|i| !used_ids.contains(i))
                            .unwrap();

                        Some(CompartmentID {
                            number: new_id as isize,
                            species: r.species_name.clone(),
                            next_parent: match &parent_id {
                                None => None,
                                Some(y) => Some(Box::new(y.clone())),
                            },
                        })
                    } else {
                        None
                    }
                }
                Some(left_idx) => enum_reac
                    .species_in
                    .get(left_idx)
                    .as_ref()
                    .expect("not found on left")
                    .my_id
                    .clone(), // may be none, for not compartment
            },
            //my_flattend_idx: None,
        }
        .sort();

        ids_of_newly_added[num] = enum_spec.my_id.clone();

        // change global values
        for (glob_name, _glob_change) in attrs.global {
            unimplemented!("Not implemented to change {}", glob_name);
        }

        assert!(r.amount >= 1);
        match model.initial_species.get_mut(&enum_spec) {
            None => {
                //println!("ADD NEW SPECIES {:?}", enum_spec);
                model.initial_species.insert(enum_spec, r.amount);
            }
            Some(y) => {
                *y += r.amount;
            }
        }
    }
    let mut new_inital_species = model.initial_species.clone();

    // Do the Rest (move or delete all restsolution stuff)

    //TODO maybe need to add compartments again or something, in case they are now removed.

    model.initial_species.into_iter().for_each(|(spec, amt)| {
        let mut parent = spec.parent_id.clone();

        while parent.is_some() {
            if enum_reac
                .species_in
                .iter()
                .filter_map(|(_, k)| k.my_id.as_ref())
                .find(|k| **k == *parent.as_ref().unwrap())
                .is_some()
            {
                // a parent of this species is on the left side of reaction!
                let occur_outside_rest = enum_reac
                    .species_in
                    .values()
                    .filter(|k| **k == spec)
                    .count();

                // If I'm part of rest solution: do what happens with rest solution
                assert!(parent.is_some());
                let (left_id_of_parent, _) = enum_reac
                    .species_in
                    .iter()
                    .find(|(_k, v)| v.my_id == parent)
                    .unwrap();
                //TODO check that there is only one rest solution under each compartment on left
                let my_rest_transtion = prototype
                    .rest_solution_transiotions
                    .iter()
                    .find(|k| k.leftsideparent == *left_id_of_parent);

                match my_rest_transtion {
                    None => {
                        unimplemented!("Not found in rest solution, but parent is on left")
                    }
                    Some(rt) => match rt.idx_right {
                        None => {
                            if occur_outside_rest == 0 {
                                assert!(new_inital_species.remove(&spec).is_some());
                            } else {
                                assert!(
                                    *new_inital_species.get(&spec).unwrap() >= occur_outside_rest
                                );
                                *new_inital_species.get_mut(&spec).unwrap() = occur_outside_rest;
                            }
                            return;
                        }
                        Some(idx_parent_right) => {
                            let mut new_spec = spec.clone();
                            if idx_parent_right < 0 {
                                new_spec.parent_id = enum_reac.reaction_parent.clone();
                            } else {
                                new_spec.parent_id = Some(
                                    match &prototype.right[idx_parent_right as usize]
                                        .corresponding_on_left
                                        .as_ref()
                                    {
                                        None => ids_of_newly_added[idx_parent_right as usize]
                                            .as_ref()
                                            .unwrap()
                                            .clone(),
                                        Some(cores_left_id) => enum_reac
                                            .species_in
                                            .get(*cores_left_id)
                                            .as_ref()
                                            .unwrap()
                                            .my_id
                                            .as_ref()
                                            .expect("Corresponding had no ID")
                                            .clone(),
                                    },
                                );
                            }
                            assert!(new_spec.my_id.is_none());

                            assert_eq!(*new_inital_species.get(&spec).unwrap(), amt);
                            if occur_outside_rest == 0 {
                                new_inital_species.remove(&spec);
                            } else {
                                *new_inital_species.get_mut(&spec).unwrap() = occur_outside_rest;
                            }
                            match new_inital_species.get_mut(&new_spec) {
                                None => {
                                    assert!(amt >= occur_outside_rest);
                                    new_inital_species.insert(new_spec, amt - occur_outside_rest);
                                }

                                Some(old_amt) => {
                                    assert!(*old_amt >= occur_outside_rest);
                                    *old_amt += amt - occur_outside_rest;
                                }
                            }
                            return;
                        }
                    },
                }
            //uncommented to avoid warning
            //unreachable!("Species needs potentially dynamic handeling")

            // else check what happens to my parent. If it is not on right side, remove!
            // If it is on right side, make sure it is on right side all the way up!
            } else {
                let tmp = &parent.as_ref().unwrap().next_parent;
                parent = match tmp {
                    None => None,
                    Some(y) => Some(y.as_ref().clone()),
                };
            }
        }

        return; // species can stay as is
    });

    // Remove if no corresponding on right
    for (l_id, spec) in enum_reac.species_in.iter() {
        if prototype
            .right
            .iter()
            .filter(|k| k.corresponding_on_left.as_ref() == Some(l_id))
            .count()
            == 0
        {
            // todo remember this to not throw away later
            match new_inital_species.get_mut(spec) {
                None => {}
                Some(0) => {
                    unreachable!()
                }
                Some(1) => {
                    new_inital_species.remove(spec);
                }
                Some(k) => {
                    *k -= 1;
                }
            }
        }
    }

    model.initial_species = new_inital_species;
    let mut species_set: HashSet<_> = model.species.into_iter().collect();
    for (s, _amt) in model.initial_species.iter() {
        species_set.insert(s.clone());
    }
    model.species = species_set.into_iter().collect();
    if old_number_of_species < model.species.len() {
        println!("# A structural change (new species) has triggered a new expansion.");
        model.flatten_a(max_steps);
    }
    model
}

pub fn _change_structure_and_reexpand_old(
    mut model: EnumaratedModel,
    idx_of_reaction: usize,
    max_steps: usize,
) -> EnumaratedModel {
    let enum_reac = &model.reactions[idx_of_reaction];
    let prototype = &model.model.transitions[enum_reac.reaction_number_of_prototype];
    /*println!(
        "BEFORE {}",
        model.species.iter().map(|v| v.string_name()).join(" & ")
    );*/
    for s in prototype.structural_changes.iter() {
        match s {
            StructuralChange::RemoveCompartment(comp) => {
                // the rest solution has olreaady been handled
                let enumerated = enum_reac.species_in.get(comp).unwrap().clone();
                _remove_incl_children(&mut model.species, &mut model.initial_species, |k| {
                    *k == enumerated
                });
            }
            StructuralChange::RemoveRestSolution(rest) => {
                let reference_parent = enum_reac
                    .species_in
                    .get(&rest.parent_id)
                    .as_ref()
                    .unwrap()
                    .my_id
                    .clone()
                    .unwrap();
                let some_reference_parent = Some(reference_parent.clone());
                let species_named_next_to_rest: HashSet<_> = enum_reac
                    .species_in
                    .iter()
                    .filter(|(_id, spec)| model._any_parent_is(&spec, &reference_parent))
                    .map(|(_a, b)| b.clone())
                    .collect();

                let species_named_next_to_rest_and_on_right_side: HashSet<_> =
                    species_named_next_to_rest
                        .into_iter()
                        .filter(|k| enum_reac.species_out.contains(k))
                        .collect();

                let species_in_rest: HashSet<_> = model
                    .species
                    .iter()
                    .filter(|k| k.parent_id == some_reference_parent)
                    .cloned()
                    .collect();
                let species_to_remove: HashSet<_> = species_in_rest
                    .difference(&species_named_next_to_rest_and_on_right_side)
                    .collect();
                _remove_incl_children(&mut model.species, &mut model.initial_species, |k| {
                    species_to_remove.contains(k)
                });
                for s in species_named_next_to_rest_and_on_right_side {
                    let number_on_right_side =
                        enum_reac.species_out.iter().filter(|k| **k == s).count();
                    assert!(number_on_right_side > 0);
                    assert!(model.species.contains(&s));
                    model
                        .initial_species
                        .insert(s.clone(), number_on_right_side);
                }
            }
            /* StructuralChange::MoveRestSolutionTo { old, new } => {
                /*
                let old_parent = enum_reac
                    .species_in
                    .get(&old.parent_id)
                    .as_ref()
                    .unwrap()
                    .my_id
                    .clone()
                    .unwrap();
                let new_parent = match new{
                    RestSolutionRightTarget::ReactionLevel => {
                        match &enum_reac.reaction_parent {
                            None => panic!("Not implemented to move a rest solution to global scope"),
                            Some(x) => x
                        }
                    }
                    RestSolutionRightTarget::OtherCompartment(y) => y.
                };*/

                unimplemented!("not done")
            }*/
            _ => unimplemented!(),
        }
    }
    /*println!(
        "After {}",
        model.species.iter().map(|v| v.string_name()).join(" & ")
    );*/
    assert!(max_steps > 1);

    //TODO propperly fire rest of reaction
    model.flatten_a(max_steps);
    model
}

impl SimulationFast {
    pub fn take_current_state_as_initial(&self, model: &mut EnumaratedModel) {
        model.initial_species = self
            .species_lookup
            .iter()
            //todo remove this -> expensive!
            .inspect(|(en, _idx)| assert!(model.species.contains(en)))
            .map(|(en, idx)| (en.clone(), self.species_counts[*idx]))
            .collect();
    }

    pub fn reset_to_changed_model(self, model: &mut EnumaratedModel) -> SimulationFast {
        let mut new_sim = SimulationFast::new(&model);

        /*let idx_old_to_new = vec![None; self.species_lookup.len()] ;
        for (en,new_idx) in r.species_lookup.iter(){
            match self.species_lookup.get(en){
                None => {/* new species created -> nothing to do*/}
                Some(old_idx) => {
                    idx_old_to_new[old_idx] = Some(new_idx);
                }
            }
        }*/

        new_sim.time = self.time;

        /* Convert observations in 3 steps */
        // 1. find names
        let new_names: Vec<String> = new_sim.observation_names.iter().cloned().collect();
        //println!("N1: {:?}", new_sim.observation_names);
        let num_new_names = new_names.len();
        new_sim.observation_names = new_names
            .into_iter()
            .chain(
                self.observation_names
                    .iter()
                    .filter(|v| !new_sim.observation_names.contains(v))
                    .cloned(),
            )
            .collect();
        //println!("N2: {:?}", new_sim.observation_names);

        // 2. make space
        new_sim.observation_values =
            vec![
                vec![PrimitiveTypes::Integer(0); new_sim.observation_names.len()];
                self.obs_times.len()
            ];
        new_sim.dead_observations_to_keep = new_sim.observation_names.len() - num_new_names;
        //println!(" we have {} new names ( {} + {})", r.observation_values.len(), self.observation_names.len(), num_new_names);

        // 3. transfer old data
        let obs_conv: HashMap<String, usize> = new_sim
            .observation_names
            .iter()
            .cloned()
            .enumerate()
            .map(|(a, b)| (b, a))
            .collect();
        //assert_eq!(self.observation_names.len(),self.observation_values.first().unwrap().len());
        /* println!(
            "NAMES: {:?}\n{:?}\n{:?}",
            self.observation_names,
            self.observation_values.last(),
            self.observation_values.first()
        );*/
        self.observation_names
            .iter()
            .enumerate()
            .for_each(|(old_idx, v)| match obs_conv.get(v) {
                None => unreachable!("Can not happen"),
                Some(new_idx) => {
                    assert_eq!(
                        self.observation_values.len(),
                        new_sim.observation_values.len()
                    );
                    assert_eq!(obs_conv.len(), new_sim.observation_names.len());
                    new_sim
                        .observation_values
                        .iter_mut()
                        .zip(self.observation_values.iter())
                        .map(|(a, b)| {
                            assert!(a.len() >= b.len());
                            (a, b)
                        })
                        .for_each(|(new_vec, old_vec)| new_vec[*new_idx] = old_vec[old_idx].clone())
                }
            });

        // true for no extra species created
        //println!("r{:?}\ns{:?}", new_sim.observation_names, self.observation_names);
        //assert_eq!(new_sim.observation_names.len(), self.observation_names.len());
        //r.observation_values = self.observation_values;
        assert_eq!(new_sim.reaction_names.len(), self.reaction_names.len());
        new_sim.obs_reaction_counts = self.obs_reaction_counts;

        if new_sim.obs_global_vals_names == self.obs_global_vals_names {
            new_sim.global_values = self.global_values
        } else {
            panic!("Global value changes not yet compatible with structural value changes")
        }
        new_sim.stepcount = self.stepcount;
        new_sim.rng = self.rng;
        new_sim.last_io = self.last_io;
        new_sim.obs_times = self.obs_times;

        assert_eq!(new_sim.obs_times.len(), new_sim.observation_values.len(),);
        new_sim
    }
}
