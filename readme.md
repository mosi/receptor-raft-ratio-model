# SI_RECEPTOR_RAFT_RATIO
Supporting information for the paper by F. Haack, T. Köster, and A. M. Uhrmacher: "[Receptor/raft ratio is a determinant for LRP6 phosphorylation and WNT/beta-catenin signaling]".

## General information
This README briefly explains the content of the subdirectories and how to replicate all experiments and obtain the figures shown in the paper mentioned above.

## Requirements for Replicating all Experiments

1. Install [Python3](https://python.org) and all additional libraries specified in requirements.txt.
2. Install rust and cargo via [rustup](rustup.rs)
3. Run "experiment.py". (This initiates the simulation and the plotting of the simuation results))
4. All plots are created within the same directory.

### Subdirectory *vsm*
This directory contains the simulation setup to reproduce Supplementary Figure 1 of the manuscript. Here, all simulations of the validation experiments VSM1 - VSM3 (cf. Fig. 2) are bundled and executed in this script. Note, that in the process of model building and verification, as indicated in Fig. 2, these simulation experiments were executed seperately. 
The requirements 1-3 are fulfilled, when the simulations yield the following results: For all receptor/raft ratios: 1. The Ratio of LRP6 within and outside Rafts is constant at about 25-30%; 2. WNT-LRP6 binding is constant (despite some minor deviations); 3. beta-catenin fold change is constant at 1 (despite some minor deviations). 

### Subdirectory *asm*
This directory contains the simulation setup to reproduce Figure 1 of the manuscript. Here, all simulations of the simulation experiments ASM1 - ASM3 (cf. Fig. 2) are bundled and executed in this script. 


