# Nested Rules Expander

This is a commandline tool to read attributed (statically) nested ML-Rules like models and generate a static reaction network based on them.
It also has a basic (slow) simulator integrated.

## Getting started

Firstly, clone this repo (`git clone git@git.informatik.uni-rostock.de:tk3811/ssa-code-gen-2.git` or `git clone https://git.informatik.uni-rostock.de/tk3811/ssa-code-gen-2.git`)

To compile the software you need the rust compiler.
It's best installed via the [rustup installer](https://rustup.rs/).

To compile the software, go into the directory of the cloned repo and execute the `cargo build --release` command.
This should build the software.

To run you can use the `cargo run --release` command.
If you want to use any flags, they need to be separated by double dash `--`.

## Usage
To run you can use the `cargo run --release` command.
If you want to use any flags, they need to be separated by double dash `--`.
For example to build the included pred-prey model:

```
cargo run --release -- pp.mlr
```
This will create the file `pp.mlr.reaction_model` that contains the expanded network.

If you also want to simulate, you need to pass the `--simulate` flag, and indicate until what time (modeltime in seconds) to simulate and how many observations you would like to make:
```
cargo run --release -- pp.mlr --simulate 12.5 1000
```
In additon to writing the expanded reaction model, this will create the `output.csv` file, that contains the observations of all species at the specified times as comma separated values.

### Errors
Most errors will indicate where they came from giving the line number (here line 46):
```
thread 'main' panicked at '  --> 46:10
   |
46 | B -> A + C(Newcount = 1) @ 3.{Hz};␊
   |          ^
   |
   = The name 'Newcount' is not an attribute for species 'C''
```
But this functionality is not implemented for all errors yet.

## DSL
Our basic pred-prey model looks like this:
```
/* Species definitions*/
wolf();
sheep();

/* Initial state */
Initial:  1000 wolf + 1000 sheep;

/* Rules */
wolf + sheep -> 2 wolf @ 0.0001{Hz};
sheep -> 2 sheep @ 1.01{Hz};
wolf ->  @ 1{Hz};
```
You can put the lines in arbitrary order.
Are more complex model can be found in the root directory under the name [testmodel.mlr](https://git.informatik.uni-rostock.de/tk3811/ssa-code-gen-2/-/blob/master/testmodel.mlr)

### Units
All rates need to have a Dimension of `1/Time`.
To indicate a unit of a value use curly braces.
Possible unit expressions look like: `{Hz}`,`{1/h}`, `{h^-1 m^2 / m^3 s}` etc.

Currently, the following units are known to the tool:

* *1, centi, milli, micro, µ, m, cm, mm, nm, s, Hz, min, h, kg, g, A, V, K, mol*

but more can be added.

### Constants

Constants can be assigned via,
```
const k = 23{Hz} + 3{1/h};
```

### Attributes
When declaring a species, you can also give that species attributes with a name and a specific type
```
F( k : int, jabel : string, mee : float{m^2});
```
Here F has an attribute k of type int and an attribute jabel of type string.
mee is a float that is measuerd in square meters.

On the left side of rules, attributes can be restricted to match only specific entities of a species, e.g.
```
F( k + 4 >= 23) -> F(mee = 23{m^2} - 1 {cm^2}) @ ...
```
On the right side of a rule, the can be assigned.

### Left-right correspondence
There is no need to explicitly name all attributes for a species on the right side of a reaction.
For example:
```
A(x : int);
B();

A(x==4) -> A + B @ ...;
```
Here A on the right side will automatically correspond to the A on the left side and have x set to 4 as well.
```
A -> A + B@ ...;
```
This is also ok, here the A on the right side will find its corresponding entity of the left automatically and copy its attribute values.

If there are multiple species on left or right side, you neeed to give them names explicitly using a colon:
```
A:a + A(x==3) -> A:a + B@ ...;
```

### Rates
Rates with one `@` are implicitly mass action and there is no need (and it is not possible), to explicitly count a species via `#`.
If you want to count species yourself (and are sure of the implications), use two at symbols like 
```
A -> B @@ #A*#A * 3{Hz};
``` 
Using explicit names is also possible here:
```
A:a -> B @@ #a*#a * 3{Hz};
```
One should be carefull using this type of rate. 
It's propensity will be multiplied by the number of compartment level (i.e. excluding amounts) matches of the left side in the system.

#### Guards
Sometimes a reaction has a specific constraint to the amount of something.
You can use a guarding if.
Only when the if is true, the reaction will happen.
```
A -> B @ if #A > 12 then 32{Hz}; 
```
You can also use constants or reference attributes from the left side of the rule in the guard.


You can also chain multiple of these Expressions or use parenthesis:

*this is not yet implemented, but a design draft*
```
A -> B @ if #A > 12 && (#A <= 30) then 32{Hz}; 
```
Guards are not considered when expanding the rules into a reaction network.

#### Time dependent Expressions
The current simulation time can be accessed via `__time__` within rate expressions or the guards of rates.
Of cause this time also has a unit of 1/Time.

### Counting in subsolution
Sometimes you need the count of a species, that is not directly involved in the reaction.
To to that you can use a *global count*, denoted by two counting symbols (##).

You can use these counts in guards, rules and other places, where numbers are needed.
For example
```
A(v==2) -> B @ if ##A > 10 then 1{Hz}
```
means, that an A with v Equal 2 will turn into a B only when the total number of As in the system (independent of their attribute), is Greater than 10.

*this is not yet implemented for specific names, but works globally, but a design draft*
If you want to count the number of entities in a particular compartment, use the *in* keyword.
Write:
```
C:c -> A  @ if ##(A in c) > 3 then 0.4 {Hz}; 
```

### Observations
If none are given, all species are observed into the csv file, using their expanded names.
To observe the amount of a specific entiy add this line:
```
observe A(f =="test");
```

You can also observe particular nested Species like:
```
observe K(f == 4) in A;
```
This will count all the A that are in compartments of type K that have f set to 4.

## Dynamic structure
It is possible for Compartments to be created or removed from the system, or for the contents of the compartents (referenced by the so-called *rest solution*) to be (re-)moved.
Below are some examples of such reactions.
These reactions incure a relativly high runtime cost, every time they fire.
```
Compartment[ A + ?] -> Compartment[A] + ? @@ 0.04{Hz};
Compartment[ ? + ] -> @ 0.1 {Hz};
 -> Compartment[3 A] + C @ 1{Hz};
Compartment[ ?one ] + Compartment[ ?two ] ->  ?one + ?two  @ 1{Hz};
Compartment[ ?one ] + Compartment[ ?two ] -> Compartment [ ?one + ?two ] @ 1{Hz};
Compartment[ ? ]  -> Compartment [  ]  + ? @ 1{Hz};
```
To enable dynamic reactions pass the `--dynamic` flag to the simulator.

Rest solutions need to be specified for every compartment in such reactions.
Rest solution names start with *?* and *?* is already a valid name, however these names need to be unique.
Currently dynamic structure support is very rudimentary, but can be extended upon request.

## To slow?
There is a significantly (orders of magnitude) faster simulator the reaction networks using code generation to be found at ... 