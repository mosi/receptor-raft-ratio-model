use crate::input::structures::Dimension;
use crate::input::syntax::ValueWithUnit;

const UNIT_1: ValueWithUnit = ValueWithUnit {
    unit: Dimension::dimensionless(),
    value: 1.0,
};
const UNIT_METER: ValueWithUnit = ValueWithUnit {
    unit: {
        let mut u = Dimension::dimensionless();
        u.length = 1;
        u
    },
    value: 1.0,
};

pub const UNIT_SECOND: ValueWithUnit = ValueWithUnit {
    unit: {
        let mut u = Dimension::dimensionless();
        u.time = 1;
        u
    },
    value: 1.0,
};

const UNIT_AMPERE: ValueWithUnit = ValueWithUnit {
    unit: {
        let mut u = Dimension::dimensionless();
        u.current = 1;
        u
    },
    value: 1.0,
};

const UNIT_KG: ValueWithUnit = ValueWithUnit {
    unit: {
        let mut u = Dimension::dimensionless();
        u.weight = 1;
        u
    },
    value: 1.0,
};

const UNIT_MOLE: ValueWithUnit = ValueWithUnit {
    unit: {
        let mut u = Dimension::dimensionless();
        u.amount = 1;
        u
    },
    value: 1.0,
};

const UNIT_KELVIN: ValueWithUnit = ValueWithUnit {
    unit: {
        let mut u = Dimension::dimensionless();
        u.temperature = 1;
        u
    },
    value: 1.0,
};

pub fn match_string_to_unit(s: &str) -> Option<ValueWithUnit> {
    match s {
        "1" => Some(UNIT_1),
        "centi" => Some(UNIT_1 * 1e-2),
        "milli" => Some(UNIT_1 * 1e-3),
        "micro" => Some(UNIT_1 * 1e-6),
        "µ" => Some(UNIT_1 * 1e-6),

        "m" => Some(UNIT_METER),
        "cm" => Some(UNIT_METER * 0.01),
        "dm" => Some(UNIT_METER * 0.1),
        "mm" => Some(UNIT_METER * 1e-3),
        "nm" => Some(UNIT_METER * 1e-9),

        "L" => Some((UNIT_METER * 0.1).my_power(3)),

        "s" => Some(UNIT_SECOND),
        "Hz" => Some(UNIT_SECOND.my_power(-1)),
        "min" => Some(UNIT_SECOND * 60.),
        "h" => Some(UNIT_SECOND * 60. * 60.),

        "kg" => Some(UNIT_KG),
        "J" => Some(UNIT_KG * UNIT_METER.my_power(2) * UNIT_SECOND.my_power(-2)),
        "g" => Some(UNIT_KG * 1e-3),

        "A" => Some(UNIT_AMPERE),
        "C" => Some(UNIT_AMPERE * UNIT_SECOND),
        "V" => Some(
            UNIT_KG * UNIT_METER.my_power(2) * UNIT_SECOND.my_power(-3) * UNIT_AMPERE.my_power(-1),
        ),

        "K" => Some(UNIT_KELVIN),

        "mol" => Some(UNIT_MOLE),

        _ => None,
    }
}
