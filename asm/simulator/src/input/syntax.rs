#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct MLRgrammar;
use crate::input::structures::{
    Dimension, FloatOp, LanguageError, LogicalOp, NonNumericalDimension,
};

use askama::Template;
use pest::error::{Error, ErrorVariant, LineColLocation};
use pest::iterators::{Pair, Pairs};
use pest::prec_climber::*;
use pest::{Parser, Span};
use std::collections::{HashMap, HashSet};
use std::fmt::Formatter;
use std::ops;

#[derive(Hash, Eq, PartialEq, Debug)]
pub enum SettingsVariant {
    Endtime,
    NumReps,
}

use crate::input::units::match_string_to_unit;
use itertools::Itertools;
use lazy_static::*;

impl LanguageError {
    pub fn pest(x: pest::error::Error<Rule>, line: u32, file: &str) -> LanguageError {
        let (l1, l2, c1, c2) = match x.line_col {
            LineColLocation::Pos((l, c)) => (l, l, c, c + 2),
            LineColLocation::Span((l1, c1), (l2, c2)) => (l1, l2, c1, c2),
        };
        assert!(l2 >= l1);
        assert!(c2 >= c1);
        LanguageError {
            lstart: l1,
            lstop: l2,
            cstart: c1,
            cstop: c2,
            what: x.to_string(),
            line,
            file: file.to_string(),
        }
    }
    pub fn from_span(span: &pest::Span, message: &str, line: u32, file: &str) -> LanguageError {
        LanguageError::pest(
            Error::<Rule>::new_from_span(
                ErrorVariant::CustomError {
                    message: message.to_string(),
                },
                span.clone(),
            ),
            line,
            file,
        )
    }
}

lazy_static! {
    static ref PREC_CLIMBER: PrecClimber<Rule> = {
        use Assoc::*;
        use Rule::*;

        PrecClimber::new(vec![
            Operator::new(add, Left) | Operator::new(subtract, Left),
            Operator::new(multiply, Left) | Operator::new(divide, Left),
            Operator::new(power, Right),
        ])
    };
}

#[derive(Debug, Clone)]
pub struct AttributeConstraints<'a> {
    pub name: String,
    pub constraint: String,
    pub span: Span<'a>,
}

/*fn att_val_to_unit_float(val: AttributeValues) -> Option<ValueWithUnit> {
    match val {
        AttributeValues::Integer(x) => Some(ValueWithUnit::dimensionless(x as f64)),
        AttributeValues::String(_) => None,
        AttributeValues::Float(val) => Some(ValueWithUnit::dimensionless(
            *val.numer() as f64 / *val.denom() as f64,
        )),
        AttributeValues::UnitFloat((dim, val)) => Some(ValueWithUnit {
            unit: dim,
            value: *val.numer() as f64 / *val.denom() as f64,
        }),
        AttributeValues::Bool(_) => None,
    }
}*/

/*fn evaluate_value_w_unit_expr(
    pair_in: Pairs<Rule>,
    constants: &HashMap<String, ConstantDefinition>,
) -> ValueWithUnit {
    //assert_eq!(pair_in.as_rule(), Rule::expr_float_w_unit);
    PREC_CLIMBER.climb(
        pair_in,
        |pair: Pair<Rule>| match pair.as_rule() {
            Rule::float_w_unit => ValueWithUnit::from_pair(pair,true),
            Rule::expr_float_w_unit => evaluate_value_w_unit_expr(pair.into_inner(), constants),
            Rule::name => match constants.get(pair.as_str()) {
                Some(x) => match att_val_to_unit_float(x.clone().value) {
                    Some(x) => x,
                    None => panic!(
                        "{}",
                        Error::<Rule>::new_from_span(
                            ErrorVariant::CustomError {
                                message: format!("Constant is not a number",).to_string(),
                            },
                            pair.as_span().clone()
                        )
                    ),
                },
                None => panic!(
                    "{}",
                    Error::<Rule>::new_from_span(
                        ErrorVariant::CustomError {
                            message: format!("Is not a defined constant",).to_string(),
                        },
                        pair.as_span().clone()
                    )
                ),
            },
            Rule::attribute_reference => unimplemented!(),
            _ => unreachable!(),
        },
        |lhs: ValueWithUnit, op: Pair<Rule>, rhs: ValueWithUnit| match op.as_rule() {
            Rule::add => match lhs.try_add(&rhs) {
                Some(x) => x,
                None => panic!(
                    "{}",
                    Error::<Rule>::new_from_span(
                        ErrorVariant::CustomError {
                            message: format!(
                                "Addition not possible, unit missmatch {} != {}",
                                lhs.unit, rhs.unit
                            )
                            .to_string(),
                        },
                        op.as_span().clone()
                    )
                ),
            },
            Rule::subtract => match lhs.try_add(&(rhs.clone() * -1.)) {
                Some(x) => x,
                None => panic!(
                    "{}",
                    Error::<Rule>::new_from_span(
                        ErrorVariant::CustomError {
                            message: format!(
                                "Addition not possible, unit missmatch {} != {}",
                                lhs.unit, rhs.unit
                            )
                            .to_string(),
                        },
                        op.as_span().clone()
                    )
                ),
            },
            Rule::multiply => lhs * rhs,
            Rule::divide => match lhs.try_div(rhs) {
                Some(x) => x,
                None => panic!(
                    "{}",
                    Error::<Rule>::new_from_span(
                        ErrorVariant::CustomError {
                            message: "Division not possible (div by 0?)".to_string(),
                        },
                        op.as_span().clone()
                    )
                ),
            },
            Rule::power => unimplemented!(),
            _ => unreachable!(),
        },
    )
}*/

#[derive(Clone, Debug, PartialEq)]
pub struct ValueWithUnit {
    pub unit: Dimension,
    pub value: f64,
}
impl ops::Mul<ValueWithUnit> for ValueWithUnit {
    type Output = ValueWithUnit;
    fn mul(self, rhs: ValueWithUnit) -> Self::Output {
        ValueWithUnit {
            unit: Dimension {
                length: self.unit.length + rhs.unit.length,
                time: self.unit.time + rhs.unit.time,
                weight: self.unit.weight + rhs.unit.weight,
                current: self.unit.current + rhs.unit.current,
                temperature: self.unit.temperature + rhs.unit.temperature,
                amount: self.unit.amount + rhs.unit.amount,
                non_numerical: NonNumericalDimension::Nothing,
            },
            value: self.value * rhs.value,
        }
    }
}

impl ops::Mul<f64> for ValueWithUnit {
    type Output = ValueWithUnit;
    fn mul(mut self, rhs: f64) -> Self::Output {
        self.value *= rhs;
        self
    }
}

impl ValueWithUnit {
    pub fn dimensionless(v: f64) -> ValueWithUnit {
        ValueWithUnit {
            unit: Dimension::dimensionless(),
            value: v,
        }
    }
    /*fn try_add(&self, rhs: &ValueWithUnit) -> Option<ValueWithUnit> {
        match self.unit == rhs.unit {
            false => None,
            true => Some(ValueWithUnit {
                unit: self.unit.clone(),
                value: self.value + rhs.value,
            }),
        }
    }

    fn try_div(self, rhs: ValueWithUnit) -> Option<ValueWithUnit> {
        if rhs.value == 0.0 {
            return None;
        }
        Some(ValueWithUnit {
            unit: Dimension {
                length: self.unit.length - rhs.unit.length,
                time: self.unit.time - rhs.unit.time,
                weight: self.unit.weight - rhs.unit.weight,
                current: self.unit.current - rhs.unit.current,
                temperature: self.unit.temperature - rhs.unit.temperature,
                amount: self.unit.amount - rhs.unit.amount,

                non_numerical: NonNumericalDimension::Nothing,
            },
            value: self.value / rhs.value,
        })
    }*/

    pub fn my_power(self, p: i32) -> ValueWithUnit {
        //assert_eq!(self.value,1.0);
        let my_p = p as isize;
        ValueWithUnit {
            unit: Dimension {
                length: self.unit.length * my_p,
                time: self.unit.time * my_p,
                weight: self.unit.weight * my_p,
                current: self.unit.current * my_p,
                temperature: self.unit.temperature * my_p,
                amount: self.unit.amount * my_p,
                non_numerical: NonNumericalDimension::Nothing,
            },
            value: (self.value).powi(p),
        }
    }
    fn from_pair_list(
        pair: pest::iterators::Pair<Rule>,
    ) -> Result<ValueWithUnit, Vec<LanguageError>> {
        assert_eq!(pair.as_rule(), Rule::unit_list);
        pair.into_inner()
            .map(|u| {
                assert_eq!(u.as_rule(), Rule::unit_single);
                let inner = u.clone().into_inner().next().unwrap();
                let (unit_str, pow) = match inner.as_rule() {
                    Rule::unit_single_reg => (u.as_str(), 1),
                    Rule::unit_single_exp => {
                        let mut k = inner.into_inner();
                        let unit_name = k.next().unwrap().as_str();
                        let exp: i32 = k.next().unwrap().as_str().parse().unwrap();
                        (unit_name, exp)
                    }
                    _ => panic!("Invalid type"),
                };
                let unit = match_string_to_unit(unit_str);
                if unit.is_none() {
                    return Err(vec![LanguageError::from_span(
                        &u.as_span(),
                        format!("The name '{}' is not a known unit", unit_str).as_str(),
                        line!(),
                        file!(),
                    )]);
                }
                Ok(unit.unwrap().my_power(pow))
            })
            .fold(Ok(match_string_to_unit("1").unwrap()), |a, b| {
                match (a, b) {
                    (Ok(a), Ok(b)) => Ok(a * b),
                    (Err(x), _) => Err(x),
                    (_, Err(x)) => Err(x),
                }
            })
    }
    pub fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        with_prefix: bool, //constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<(ValueWithUnit, String), Vec<LanguageError>> {
        let mut pair2;
        let prefix_value;
        if with_prefix {
            assert_eq!(pair.as_rule(), Rule::float_w_unit);
            pair2 = pair.into_inner();

            let prefix_pair = pair2.next().unwrap();
            prefix_value = prefix_pair.as_str().parse().unwrap();
        } else {
            assert!(
                (pair.as_rule() == Rule::atn_float_unit)
                    || (pair.as_rule() == Rule::atn_starred_float_unit)
            );
            pair2 = pair.into_inner();
            prefix_value = 1.0;
        }

        let unit_expr = pair2.next();

        if unit_expr.is_none() {
            return Ok((ValueWithUnit::dimensionless(prefix_value), "".to_string()));
        }

        let mut unit_expr = unit_expr.unwrap().into_inner();

        let top_list = unit_expr.next().unwrap();
        let top_unit = ValueWithUnit::from_pair_list(top_list.clone())? * prefix_value;
        let buttom_list = unit_expr.next();
        match buttom_list {
            Some(v) => Ok((
                top_unit * (ValueWithUnit::from_pair_list(v.clone())?.my_power(-1)),
                format!(
                    "{}/{}",
                    top_list.as_str().to_string(),
                    v.as_str().to_string()
                ),
            )),
            None => Ok((top_unit, top_list.as_str().to_string())),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct AttributeRefStx {
    pub spec_name: String,
    pub att_name: String,
}

impl AttributeRefStx {
    pub fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        //constants: &HashMap<String, ConstantDefinition>,
    ) -> AttributeRefStx {
        assert_eq!(pair.as_rule(), Rule::attribute_reference);

        let mut pair = pair.into_inner();
        AttributeRefStx {
            spec_name: pair.next().unwrap().as_str().to_string(),
            att_name: pair.next().unwrap().as_str().to_string(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum ExprTermStx<'a> {
    NumUnit(ValueWithUnit, String),
    ConstantOrLeftAttribute(String),
    DottedAttributeRef(AttributeRefStx),
    Expr(Box<ExprStx<'a>>),
    SpeciesCount(String),
    GlobalSpeciesCount(GlobalCountStx<'a>),
    ActualString(String),
    ActualInt(isize),
    ActualBool(bool),
    GlobalTime,
}

impl<'a> ExprTermStx<'a> {
    fn render_latex(&self, may_need_braces: bool) -> String {
        match self {
            ExprTermStx::NumUnit(x, str) => format!("{} \\text{{{}}}", x.value, str),
            ExprTermStx::ConstantOrLeftAttribute(y) => format!("\\text{{{}}}", y.clone()),
            ExprTermStx::DottedAttributeRef(x) => {
                format!("\\text{{{}}}.\\text{{{}}}", x.spec_name, x.att_name)
            }
            ExprTermStx::Expr(y) => format!("{}", y.render_latex(may_need_braces)),
            ExprTermStx::SpeciesCount(y) => format!("\\text{{\\#{} }}", y),
            ExprTermStx::GlobalSpeciesCount(y) => format!("\\text{{\\#\\#{}}}", y.string_name),
            ExprTermStx::ActualString(y) => format!("\\text{{\"\\emph{{{}}}\"}}", y),
            ExprTermStx::ActualInt(y) => format!("{}", y),
            ExprTermStx::ActualBool(y) => format!("\\text{{ {} }}", y),
            ExprTermStx::GlobalTime => format!("\\StopWatchStart"),
        }
    }
}

fn get_expr_tree(
    pair_in: Pairs<Rule>,
    allow_counts: bool,
    allow_attributes: bool,
    allow_global_counts: bool,
) -> Result<ExprStx, Vec<LanguageError>> {
    //TODO check why this is needed
    /*if pair_in.clone().into_iter().next().is_none() {
        return Err(vec![]);
    }*/
    //assert_eq!(pair_in.as_rule(), Rule::expr_float_w_unit);
    PREC_CLIMBER.climb(
        pair_in,
        |pair: Pair<Rule>| match pair.as_rule() {
            Rule::integer => Ok(ExprStx {
                left: ExprTermStx::ActualInt(pair.as_str().parse().unwrap()),
                op: None,
                span: pair.as_span(),
            }),
            Rule::string_value => Ok(ExprStx {
                left: ExprTermStx::ActualString(
                    pair.clone()
                        .into_inner()
                        .next()
                        .unwrap()
                        .as_str()
                        .to_string(),
                ),
                op: None,
                span: pair.as_span(),
            }),
            Rule::boolean_value => Ok(ExprStx {
                left: ExprTermStx::ActualBool(pair.as_str().parse().unwrap()),
                op: None,
                span: pair.as_span(),
            }),
            Rule::float => Ok(ExprStx::from_val_w_unit(
                ValueWithUnit::dimensionless(pair.as_str().parse().unwrap()),
                pair.as_span(),
                "".to_string(),
            )),
            Rule::float_w_unit => {
                let (v, unit_str) = ValueWithUnit::from_pair(pair.clone(), true)?;
                Ok(ExprStx::from_val_w_unit(v, pair.as_span(), unit_str))
            }
            Rule::expr_float_w_unit => get_expr_tree(
                pair.into_inner(),
                allow_counts,
                allow_attributes,
                allow_global_counts,
            ),
            Rule::name => Ok(ExprStx {
                left: ExprTermStx::ConstantOrLeftAttribute(pair.as_str().to_string()),
                op: None,
                span: pair.as_span(),
            }),
            Rule::attribute_reference => Ok(match allow_attributes {
                true => ExprStx {
                    left: ExprTermStx::DottedAttributeRef(AttributeRefStx::from_pair(pair.clone())),
                    op: None,
                    span: pair.as_span(),
                },
                false => {
                    return Err(vec![LanguageError::from_span(
                        &pair.as_span(),
                        "Attributes not possible here",
                        line!(),
                        file!(),
                    )])
                }
            }),
            Rule::species_count => Ok(match allow_counts {
                true => ExprStx {
                    left: ExprTermStx::SpeciesCount(
                        pair.clone()
                            .into_inner()
                            .next()
                            .unwrap()
                            .as_str()
                            .to_string(),
                    ),
                    op: None,
                    span: pair.as_span(),
                },
                false => {
                    return Err(vec![LanguageError::from_span(
                        &pair.as_span(),
                        "Species Counts (#) are not allowed here",
                        line!(),
                        file!(),
                    )])
                }
            }),
            Rule::global_species_count => Ok(match allow_global_counts {
                true => ExprStx {
                    span: pair.as_span(),
                    left: ExprTermStx::GlobalSpeciesCount(GlobalCountStx::from_pair(pair)?),
                    op: None,
                },
                false => {
                    return Err(vec![LanguageError::from_span(
                        &pair.as_span(),
                        "Global Species Counts (##) are not allowed here",
                        line!(),
                        file!(),
                    )])
                }
            }),
            Rule::global_time => Ok(ExprStx {
                left: ExprTermStx::GlobalTime,
                op: None,
                span: pair.as_span().clone(),
            }),
            x => unreachable!("is {:?}", x),
        },
        |lhs: Result<ExprStx, _>, op: Pair<Rule>, rhs: Result<ExprStx, _>| {
            let lhs = lhs?;
            let rhs = rhs?;
            match op.as_rule() {
                Rule::add => Ok(ExprStx {
                    left: ExprTermStx::Expr(Box::new(lhs)),
                    op: Some((FloatOp::Add, ExprTermStx::Expr(Box::new(rhs)))),
                    span: op.as_span(),
                }),
                Rule::subtract => Ok(ExprStx {
                    left: ExprTermStx::Expr(Box::new(lhs)),
                    op: Some((FloatOp::Sub, ExprTermStx::Expr(Box::new(rhs)))),
                    span: op.as_span(),
                }),
                Rule::multiply => Ok(ExprStx {
                    left: ExprTermStx::Expr(Box::new(lhs)),
                    op: Some((FloatOp::Mul, ExprTermStx::Expr(Box::new(rhs)))),
                    span: op.as_span(),
                }),
                Rule::divide => Ok(ExprStx {
                    left: ExprTermStx::Expr(Box::new(lhs)),
                    op: Some((FloatOp::Div, ExprTermStx::Expr(Box::new(rhs)))),
                    span: op.as_span(),
                }),
                Rule::power => Ok(ExprStx {
                    left: ExprTermStx::Expr(Box::new(lhs)),
                    op: Some((FloatOp::Pow, ExprTermStx::Expr(Box::new(rhs)))),
                    span: op.as_span(),
                }),
                _ => unreachable!(),
            }
        },
    )
}

#[derive(Debug, Clone)]
pub struct ExprStx<'a> {
    pub left: ExprTermStx<'a>,
    pub op: Option<(FloatOp, ExprTermStx<'a>)>,
    pub span: Span<'a>,
}

impl ExprStx<'_> {
    fn from_val_w_unit(v: ValueWithUnit, s: Span, unit_str: String) -> ExprStx {
        ExprStx {
            left: ExprTermStx::NumUnit(v, unit_str),
            op: None,
            span: s,
        }
    }
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        allow_counts: bool,
        allow_attributes: bool,
        allow_global_counts: bool,
    ) -> Result<ExprStx, Vec<LanguageError>> {
        get_expr_tree(
            pair.into_inner(),
            allow_counts,
            allow_attributes,
            allow_global_counts,
        )
    }
    pub fn render_latex_rate(&self) -> String {
        self.render_latex(false)
    }
    pub fn render_latex(&self, may_need_braces: bool) -> String {
        let (brace_left, brace_right) = match may_need_braces {
            true => ("\\left(", "\\right)"),
            false => ("", ""),
        };
        match &self.op {
            None => self.left.render_latex(may_need_braces),
            Some((op, right)) => {
                /*match right{
                    ExprTermStx::NumUnit(a, b) => {
                        if *op == FloatOp::Mul && *a == ValueWithUnit::dimensionless(1.){
                            return self.left.render_latex(may_need_braces);
                        }
                    }
                    _ => {}
                }*/

                match op {
                    FloatOp::Add => format!(
                        "{}{} + {}{}",
                        brace_left,
                        self.left.render_latex(false),
                        right.render_latex(false),
                        brace_right
                    ),
                    FloatOp::Sub => format!(
                        "{}{} - {}{}",
                        brace_left,
                        self.left.render_latex(false),
                        right.render_latex(false),
                        brace_right
                    ),
                    FloatOp::Div => format!(
                        "\\dfrac{{\\scriptsize {} }}{{\\scriptsize {} }}",
                        self.left.render_latex(false),
                        right.render_latex(false)
                    ),
                    FloatOp::Mul => format!(
                        "{} \\cdot {}",
                        self.left.render_latex(true),
                        right.render_latex(true)
                    ),
                    FloatOp::Pow => format!(
                        "\\left({}\\right) ^{{ {} }}",
                        self.left.render_latex(false),
                        right.render_latex(false)
                    ),
                }
            }
        }
    }
}
impl<'a> std::fmt::Display for ExprStx<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self.op {
            Some((op, right)) => write!(f, "({:?}) {} ({:?})", self.left, op, right),
            None => write!(f, "{:?}", self.left),
        }
    }
}

#[derive(Debug, Clone)]
pub struct TransitionStx<'a> {
    pub left: SpeciesLeftSolution<'a>,
    pub right: SpeciesRightSolution<'a>,
    pub guard: Vec<LogicalConstraintStx<'a>>,
    pub rate: ExprStx<'a>,
    pub span: Span<'a>,
    pub is_not_mass_action: bool,
    pub original_string: String,
}

impl<'a> TransitionStx<'a> {
    fn vec_from_pair(
        pair: Vec<pest::iterators::Pair<'a, Rule>>,
        constants: HashMap<String, ConstantDefinition<'a>>,
    ) -> Vec<Result<TransitionStx<'a>, Vec<LanguageError>>> {
        //assert_eq!(pair.as_rule(), Rule::transitions);
        pair.into_iter()
            .map(|p| TransitionStx::from_pair(p, constants.clone()))
            .collect()
    }

    fn from_pair(
        pair: pest::iterators::Pair<'a, Rule>,
        _constants: HashMap<String, ConstantDefinition>,
    ) -> Result<TransitionStx<'a>, Vec<LanguageError>> {
        assert_eq!(pair.as_rule(), Rule::transition);
        let mut original_string = pair
            .as_str()
            .to_string()
            .replace("\n", " ")
            .replace("\r", "");
        while original_string.contains("  ") {
            original_string = original_string.replace("  ", " ");
        }
        let span = pair.as_span().clone();
        let mut pair_inner = pair.into_inner();

        let left = SpeciesLeftSolution::from_pair(pair_inner.next().unwrap());
        let right = SpeciesRightSolution::from_pair(pair_inner.next().unwrap());

        let transition_guard = pair_inner.next().unwrap();
        let transition_guard_inner = transition_guard.into_inner().next();

        let rate_pair_outer = pair_inner.next().unwrap();

        let allow_counts = match rate_pair_outer.as_rule() {
            Rule::transition_rate_FULL => true,
            Rule::transition_rate_MA => false,
            x => {
                unreachable!("{:?}", x);
            }
        };

        let rate = ExprStx::from_pair(
            rate_pair_outer.clone().into_inner().next().unwrap().clone(),
            allow_counts,
            true,
            true,
        );

        let rate = rate?;

        let guard = match transition_guard_inner {
            None => vec![],
            Some(x) => x
                .into_inner()
                .map(|v| LogicalConstraintStx::from_pair(v, true))
                .collect(),
        };
        let mut errs = vec![];
        let guard = guard
            .into_iter()
            .filter_map(|v| match v {
                Ok(x) => Some(x),
                Err(x) => {
                    errs.extend(x.into_iter());
                    None
                }
            })
            .collect();
        if !errs.is_empty() {
            return Err(errs);
        }

        let left = left?;
        let right = right?;
        Ok(TransitionStx {
            left,
            right,
            guard,
            rate,
            span,
            is_not_mass_action: allow_counts,
            original_string,
        })
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesAttribute<'a> {
    pub name: String,
    pub dim: Dimension,
    pub can_be_expanded: bool,
    pub span: Span<'a>,
}

#[derive(Debug)]
pub struct SpeciesDefinition<'a> {
    pub name: String,
    pub attributes: Vec<SpeciesAttribute<'a>>,
    pub span: Span<'a>,
}

#[derive(Debug, Clone)]
pub struct LogicalConstraintStx<'a> {
    pub left: ExprStx<'a>,
    pub op: LogicalOp,
    pub right: ExprStx<'a>,
    pub span: Span<'a>,
}

impl LogicalConstraintStx<'_> {
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        allow_counts: bool,
    ) -> Result<LogicalConstraintStx, Vec<LanguageError>> {
        assert_eq!(pair.as_rule(), Rule::logical_constraint_outer);
        let span = pair.as_span();
        let mut att_assign = pair.into_inner();
        Ok(LogicalConstraintStx {
            left: ExprStx::from_pair(att_assign.next().unwrap(), allow_counts, true, allow_counts)?,
            op: LogicalOp::from_str(att_assign.next().unwrap().as_str()).unwrap(),
            right: ExprStx::from_pair(
                att_assign.next().unwrap(),
                allow_counts,
                true,
                allow_counts,
            )?,
            span,
        })
    }
}

impl Dimension {
    pub fn from_pair_definiton(pair: Pair<Rule>) -> Result<(Dimension, bool), Vec<LanguageError>> {
        match pair.as_rule() {
            Rule::atn_int => Ok((Dimension::non_numeric(NonNumericalDimension::Integer), true)),
            Rule::atn_str => Ok((Dimension::non_numeric(NonNumericalDimension::String), true)),
            Rule::atn_bool => Ok((Dimension::non_numeric(NonNumericalDimension::Bool), true)),
            Rule::atn_float => Ok((Dimension::dimensionless(), true)),
            Rule::atn_float_unit => Ok((ValueWithUnit::from_pair(pair, false)?.0.unit, true)),
            Rule::atn_starred_float => Ok((Dimension::dimensionless(), false)),
            Rule::atn_starred_float_unit => {
                Ok((ValueWithUnit::from_pair(pair, false)?.0.unit, false))
            }

            _ => unreachable!(),
        }
    }
}

impl SpeciesDefinition<'_> {
    fn from_pair(
        pair: Vec<pest::iterators::Pair<Rule>>,
    ) -> Result<Vec<SpeciesDefinition>, Vec<LanguageError>> {
        //assert_eq!(pair.as_rule(), Rule::species_definitions);
        let mut err = vec![];
        let res = pair
            .into_iter()
            .map(|att_asign| {
                assert_eq!(att_asign.as_rule(), Rule::species_definition);
                att_asign
            })
            .map(|p| p.into_inner())
            .map(|mut p| {
                let name_pair = p.next().unwrap();
                SpeciesDefinition {
                    name: name_pair.as_str().to_string(),
                    span: name_pair.as_span(),
                    attributes: p
                        .filter_map(|att_def| {
                            let mut att_def_inner = att_def.clone().into_inner();
                            let name = att_def_inner.next().unwrap().as_str().to_string();
                            let dim =
                                match Dimension::from_pair_definiton(att_def_inner.next().unwrap())
                                {
                                    Ok(x) => x,
                                    Err(x) => {
                                        err.extend(x.into_iter());
                                        return None;
                                    }
                                };

                            Some(SpeciesAttribute {
                                name,
                                dim: dim.0,
                                can_be_expanded: dim.1,
                                span: att_def.as_span(),
                            })
                            /*  ,
                                ,
                            */
                        })
                        .collect(),
                }
            })
            .collect();
        if !err.is_empty() {
            Err(err)
        } else {
            Ok(res)
        }
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesLeftStx<'a> {
    pub species_name: String,
    pub is_rest_solution: bool,
    pub extra_name: Option<String>,
    pub span: Span<'a>,
    pub attribute_constraints: Vec<LogicalConstraintStx<'a>>,
    pub child_solution: Option<SpeciesLeftSolution<'a>>,
}

impl SpeciesLeftStx<'_> {
    pub fn render_latex(&self) -> String {
        let sub = match &self.child_solution {
            None => "".to_string(),
            Some(y) => format!("\\left[{}\\right]", y.render_latex()),
        };
        let mut att_const = self
            .attribute_constraints
            .iter()
            .map(|ac| {
                format!(
                    " {} {} {}",
                    ac.left.render_latex_rate(),
                    ac.op.to_string(),
                    ac.right.render_latex_rate()
                )
            })
            .join(" \\&\\& ");
        if att_const != "" {
            att_const = format!("\\left({}\\right)", att_const)
        }
        let my_text = format!("\\text{{{}}}{}", self.species_name, att_const);
        if sub != "".to_string() {
            format!("\\underbrace{{ {} }}_{}", sub, my_text)
        } else {
            my_text
        }
    }
    fn from_pair(pair: pest::iterators::Pair<Rule>) -> Result<SpeciesLeftStx, Vec<LanguageError>> {
        if pair.as_rule() == Rule::rest_solution {
            return Ok(SpeciesLeftStx {
                species_name: pair.as_str().to_string(),
                is_rest_solution: true,
                extra_name: None,
                span: pair.as_span(),
                attribute_constraints: vec![],
                child_solution: None,
            });
        }
        assert_eq!(pair.as_rule(), Rule::species_left);
        let mut pair = pair.into_inner();
        let mut species_def = pair.next().unwrap().into_inner();
        let extra_name = pair.next().unwrap();
        let subsol = pair.next();
        let spec_name = species_def.next().unwrap();

        let mut errs = vec![];
        let res = SpeciesLeftStx {
            species_name: spec_name.as_str().to_string(),
            is_rest_solution: false,
            extra_name: match extra_name.into_inner().next() {
                Some(x) => Some(x.as_str().to_string()),
                None => None,
            },
            span: spec_name.as_span(),
            attribute_constraints: match species_def.next() {
                Some(x) => x
                    .into_inner()
                    .filter_map(|att_assign| {
                        match LogicalConstraintStx::from_pair(att_assign, false) {
                            Ok(x) => Some(x),
                            Err(x) => {
                                errs.extend(x.into_iter());
                                None
                            }
                        }
                    })
                    .collect(),
                None => vec![],
            },
            child_solution: match subsol {
                Some(x) => Some(SpeciesLeftSolution::from_pair(x)?),
                None => None,
            },
        };
        if errs.is_empty() {
            Ok(res)
        } else {
            Err(errs)
        }
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesRightStx<'a> {
    pub species_name: String,
    pub amt: ExprStx<'a>,
    pub is_rest_solution: bool,
    pub child_solution: Option<SpeciesRightSolution<'a>>,
    pub attribute_assignments: Vec<(String, ExprStx<'a>)>,
    pub extra_name: Option<String>,
    pub span: Span<'a>,
}
impl SpeciesRightStx<'_> {
    pub fn render_latex(&self) -> String {
        let sub = match &self.child_solution {
            None => "".to_string(),
            Some(y) => format!("\\left[{}\\right]", y.render_latex()),
        };
        let mut att_const = self
            .attribute_assignments
            .iter()
            .map(|(n, val)| format!(" \\text{{ {}}}= {}", n, val.render_latex_rate()))
            .join(" ,\\; ");
        if att_const != "" {
            att_const = format!("\\left({}\\right)", att_const)
        }
        let my_text = format!("\\text{{{}}}{}", self.species_name, att_const);
        if sub != "".to_string() {
            format!("\\underbrace{{ {} }}_{}", sub, my_text)
        } else {
            my_text
        }
    }
    fn from_pair(
        pair_in: pest::iterators::Pair<Rule>,
    ) -> Result<SpeciesRightStx, Vec<LanguageError>> {
        if pair_in.as_rule() == Rule::rest_solution {
            return Ok(SpeciesRightStx {
                species_name: pair_in.as_str().to_string(),
                amt: ExprStx {
                    left: ExprTermStx::ActualInt(1),
                    op: None,
                    span: pair_in.as_span(),
                },
                is_rest_solution: true,
                child_solution: None,
                attribute_assignments: vec![],
                extra_name: None,
                span: pair_in.as_span(),
            });
        }
        assert_eq!(pair_in.as_rule(), Rule::species_right);
        let mut pair = pair_in.clone().into_inner();
        let mut species_def = pair.next().unwrap().into_inner();
        let extra_name = pair.next().unwrap();
        let subsol = pair.next();
        let amt_pair = species_def.next().unwrap();
        let amt;
        let name_pair = match amt_pair.as_rule() {
            Rule::name => {
                amt = ExprStx {
                    left: ExprTermStx::ActualInt(1),
                    op: None,
                    span: pair_in.as_span(),
                };
                amt_pair
            }
            Rule::expr_float_w_unit => {
                println!("AMT: {:?}", amt_pair.as_str());
                amt = ExprStx::from_pair(amt_pair, false, false, false)?;
                species_def.next().unwrap()
            }
            _ => unreachable!(),
        };

        Ok(SpeciesRightStx {
            amt: amt,
            extra_name: match extra_name.into_inner().next() {
                Some(x) => Some(x.as_str().to_string()),
                None => None,
            },
            species_name: name_pair.as_str().to_string(),
            attribute_assignments: match species_def.next() {
                Some(x) => x
                    .into_inner()
                    .map(|att_assign| att_assign.into_inner())
                    .map(|mut att_assign| {
                        let r: Result<(String, ExprStx), Vec<LanguageError>> = Ok((
                            att_assign.next().unwrap().as_str().to_string(),
                            ExprStx::from_pair(att_assign.next().unwrap(), false, true, false)?,
                        ));
                        return r;
                    })
                    .collect::<Result<_, _>>()?,
                None => vec![],
            },

            child_solution: match subsol {
                Some(x) => Some(SpeciesRightSolution::from_pair(x)?),
                None => None,
            },
            span: name_pair.as_span(),
            is_rest_solution: false,
        })
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesLeftSolution<'a> {
    pub species_in_solution: Vec<SpeciesLeftStx<'a>>,
}
impl SpeciesLeftSolution<'_> {
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
    ) -> Result<SpeciesLeftSolution, Vec<LanguageError>> {
        assert_eq!(pair.as_rule(), Rule::species_left_solution);
        Ok(SpeciesLeftSolution {
            species_in_solution: pair
                .into_inner()
                .map(|v| SpeciesLeftStx::from_pair(v))
                .collect::<Result<_, _>>()?,
        })
    }
    pub fn render_latex(&self) -> String {
        format!(
            "{}",
            self.species_in_solution
                .iter()
                .map(|v| v.render_latex())
                .join(" + ")
        )
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesRightSolution<'a> {
    pub species_in_solution: Vec<SpeciesRightStx<'a>>,
}
impl SpeciesRightSolution<'_> {
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
    ) -> Result<SpeciesRightSolution, Vec<LanguageError>> {
        assert_eq!(pair.as_rule(), Rule::species_right_solution);
        Ok(SpeciesRightSolution {
            species_in_solution: pair
                .into_inner()
                .filter(|v| v.as_rule() != Rule::species_right_zero)
                .map(|v| SpeciesRightStx::from_pair(v))
                .collect::<Result<_, _>>()?,
        })
    }
    pub fn render_latex(&self) -> String {
        format!(
            "{}",
            self.species_in_solution
                .iter()
                .map(|v| v.render_latex())
                .join(" + ")
        )
    }
}

#[derive(Debug, Clone)]
pub struct ConstantDefinition<'a> {
    pub value: ExprStx<'a>,
    pub is_param: bool,
    pub comment: String,
}

impl ConstantDefinition<'_> {
    fn vec_from_pair(
        pair: Vec<(pest::iterators::Pair<Rule>, String)>,
        is_param: bool,
    ) -> Result<HashMap<String, ConstantDefinition>, Vec<LanguageError>> {
        //let mut errs = vec![];
        //assert_eq!(pair.as_rule(), Rule::constant_definitions);
        let itm: Vec<_> = pair
            .into_iter()
            .map(|(v, comment)| {
                match ConstantDefinition::from_pair(v.clone(), comment.clone(), is_param) {
                    Ok(def) => Ok((def, v)),
                    Err(x) => Err(x),
                }
            })
            .collect::<Result<_, _>>()?;
        itm.into_iter()
            .fold(Ok(HashMap::new()), |map, ((name, def), pair)| match map {
                Ok(mut map) => {
                    if map.contains_key(&*name) {
                        return Err(vec![LanguageError::from_span(
                            &pair.into_inner().next().unwrap().as_span(),
                            "Redundant constant name",
                            line!(),
                            file!(),
                        )]);
                    } else {
                        map.insert(name, def);
                        return Ok(map);
                    };
                }

                Err(x) => Err(x),
            })
    }

    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        comment: String,

        is_param: bool,
    ) -> Result<(String, ConstantDefinition), Vec<LanguageError>> {
        assert!(
            pair.as_rule() == Rule::constant_definition
                || pair.as_rule() == Rule::parameter_definition
        );
        let mut pair2 = pair.clone().into_inner();
        let name = pair2.next().unwrap().as_str().to_string();
        let expr = pair2.next().unwrap();

        let val = ExprStx::from_pair(expr, false, false, false);

        Ok((
            name,
            ConstantDefinition {
                value: val?,
                is_param,
                comment: texify_comment(comment),
            },
        ))
    }
}

fn texify_comment(s: String) -> String {
    s.replace("//", "")
        .replace("/*", "")
        .replace("*/", "")
        .replace(";", "\n")
        .replace("\n", "\n\n")
}

#[derive(Debug, Clone)]
pub struct GlobalCountStx<'a> {
    pub what: SpeciesLeftStx<'a>,
    pub inside_of: Option<Box<GlobalCountStx<'a>>>,
    pub string_name: String,
    pub span: Span<'a>,
}

impl GlobalCountStx<'_> {
    fn from_pair(pair: pest::iterators::Pair<Rule>) -> Result<GlobalCountStx, Vec<LanguageError>> {
        let mut pairs = match pair.as_rule() {
            Rule::global_species_count => pair.clone().into_inner().next().unwrap().into_inner(),
            Rule::global_count => pair.clone().into_inner(),
            _ => unreachable!(),
        };
        let first = pairs.next().unwrap();
        assert_eq!(first.as_rule(), Rule::species_left_definition);
        let mut first = first.into_inner();

        let second = pairs.next();
        let spec_name = first.next().unwrap();
        let what = SpeciesLeftStx {
            is_rest_solution: false,
            species_name: spec_name.as_str().to_string(),
            extra_name: None,
            span: spec_name.as_span(),
            attribute_constraints: match first.next() {
                Some(x) => x
                    .into_inner()
                    .map(|att_assign| LogicalConstraintStx::from_pair(att_assign, false))
                    .collect::<Result<_, _>>()?,
                None => vec![],
            },
            child_solution: None,
        };
        Ok(GlobalCountStx {
            what: what,
            inside_of: match second {
                None => None,
                Some(n) => Some(Box::new(GlobalCountStx::from_pair(n)?)),
            },
            string_name: pair.as_str().to_string(),
            span: pair.as_span(),
        })
    }
}

fn set_expr_from_pair(
    pair: pest::iterators::Pair<Rule>,
) -> Result<(SettingsVariant, ExprStx), Vec<LanguageError>> {
    assert_eq!(pair.as_rule(), Rule::set_statement);
    let mut pair = pair.into_inner();
    Ok((
        match pair.next().unwrap().as_rule() {
            Rule::set_value_end_time => SettingsVariant::Endtime,
            Rule::set_value_num_reps => SettingsVariant::NumReps,
            _ => unreachable!(),
        },
        ExprStx::from_pair(pair.next().unwrap(), false, false, false)?,
    ))
}

#[derive(Debug, Template)]
#[template(path = "latex.txt")]
pub struct ModelParsed<'a> {
    pub species_definitions: Vec<SpeciesDefinition<'a>>,
    pub initializations: SpeciesRightSolution<'a>,
    pub transitions: Vec<TransitionStx<'a>>,
    pub constants: HashMap<String, ConstantDefinition<'a>>,

    pub observations: Option<Vec<GlobalCountStx<'a>>>,

    pub species_names: HashSet<String>,
    pub settings: HashMap<SettingsVariant, ExprStx<'a>>,
}

impl<'a> ModelParsed<'a> {
    pub fn make_latex(&self) -> String {
        self.render().unwrap()
    }
    pub fn from_string(
        model_string: &'a String,
        make_parameters_const: bool,
        _allow_dynamic_structures: bool,
    ) -> Result<ModelParsed<'a>, Vec<LanguageError>> {
        let res: Result<Pairs<_>, pest::error::Error<_>> =
            MLRgrammar::parse(Rule::model, &**model_string);

        let mut model: Pairs<Rule> = match res {
            Ok(k) => k,
            Err(x) => {
                return Err(vec![LanguageError::pest(x, line!(), file!())]);
            }
        };
        println!("-- Model syntactically correct");

        let model2 = model.next().unwrap();
        assert_eq!(model2.as_rule(), Rule::model);
        let mut constant_defs = vec![];

        let mut parameter_defs = vec![];
        let mut initializations = vec![];
        let mut transitions = vec![];
        let mut species_defs = vec![];
        let mut observations = vec![];

        let mut last_comment_line = 0;

        let mut set_expr = vec![];

        model2
            .into_inner()
            .filter(|v| v.as_rule() != Rule::EOI)
            .map(|state| {
                assert_eq!(state.as_rule(), Rule::statement);
                state.into_inner().next().unwrap()
            })
            .map(|v| {
                let old_last = last_comment_line;
                last_comment_line = v.as_span().end();
                let start = v.as_span().start();
                (v, (old_last, start))
            })
            .for_each(|(pair, (start, stop))| match pair.as_rule() {
                Rule::constant_definition => {
                    constant_defs.push((pair, model_string[start..stop].to_string()))
                }
                Rule::parameter_definition => {
                    parameter_defs.push((pair, model_string[start..stop].to_string()))
                }
                Rule::initalization => initializations.push(pair),
                Rule::transition => transitions.push(pair),
                Rule::species_definition => species_defs.push(pair),
                Rule::observation => observations.push(pair),
                Rule::set_statement => set_expr.push(pair),
                _ => unreachable!("found: {:?}", pair.as_rule()),
            });

        println!(
            "-- {} Rules, {} Species and {} + {} constants",
            transitions.len(),
            species_defs.len(),
            constant_defs.len(),
            parameter_defs.len(),
        );

        match initializations.len() {
            0 => {
                return Err(vec![LanguageError {
                    lstart: 0,
                    lstop: 10,
                    cstart: 0,
                    cstop: 05,
                    what: "Missing initalization (starting with Initial: ".to_string(),
                    line: line!(),
                    file: file!().to_string(),
                }])
            }
            1 => (),
            _x => {
                return Err(vec![LanguageError::from_span(
                    &transitions.last().unwrap().as_span(),
                    "Multiple Initial sections",
                    line!(),
                    file!(),
                )])
            }
        }

        let mut set_statements: HashMap<SettingsVariant, ExprStx> = HashMap::new();
        for pair in set_expr {
            let (key, val) = set_expr_from_pair(pair.clone())?;
            match set_statements.insert(key, val) {
                None => {}
                Some(_x) => {
                    return Err(vec![LanguageError::from_span(
                        &pair.as_span(),
                        "Set can be used only once",
                        line!(),
                        file!(),
                    )]);
                }
            }
        }

        let species_definitions = SpeciesDefinition::from_pair(species_defs)?;
        let initializations = SpeciesRightSolution::from_pair(
            initializations[0].clone().into_inner().next().unwrap(),
        )?;
        let mut constants = ConstantDefinition::vec_from_pair(constant_defs, false)?;
        for (key, val) in ConstantDefinition::vec_from_pair(parameter_defs, !make_parameters_const)?
        {
            if constants.contains_key(&key) {
                return Err(vec![LanguageError::from_span(
                    &val.value.span,
                    &*format!("The name {} is allready used for a constant", key),
                    line!(),
                    file!(),
                )]);
            }
            assert!(constants.insert(key, val).is_none());
        }

        let observations: Vec<_> = observations
            .into_iter()
            .map(|v| GlobalCountStx::from_pair(v.into_inner().next().unwrap()))
            .collect::<Result<_, _>>()?;
        let observations = match observations.len() {
            0 => None,
            _ => Some(observations),
        };
        let transitions = TransitionStx::vec_from_pair(transitions, constants.clone())
            .into_iter()
            .collect::<Result<_, _>>()?;
        let res = ModelParsed {
            species_names: species_definitions.iter().map(|v| v.name.clone()).collect(),
            species_definitions,
            initializations,
            transitions,
            constants,
            observations,
            settings: set_statements,
        };
        Ok(res)
    }
}
