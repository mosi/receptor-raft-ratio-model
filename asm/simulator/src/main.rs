extern crate pest;
#[macro_use]
extern crate pest_derive;

mod flatten;
//mod parsing;
mod input;
//mod simulate;
mod dynamic_expansion;
mod simulate_fast;

use crate::flatten::EnumaratedModel;
use input::structures::Model;
use std::fs::File;
use std::io::prelude::*;
use std::time::{Duration, Instant};

use indicatif::{ProgressBar, ProgressStyle};

//use crate::input::structures::LanguageError;
use crate::dynamic_expansion::change_structure_and_reexpand;
use crate::input::structures::PrimitiveTypes;
use crate::simulate_fast::StepResult;
use clap::{App, Arg};
use itertools::Itertools;
use sha2::{Digest, Sha512};
use std::fs;
use std::io::BufReader;
use std::path::Path;

fn main() {
    let matches = App::new("mlr expander")
        .version("0.1")
        .author("Till Köster")
        .about("Expanding ML-Rules models into reaction networks (and some basic execution functionality")

        .arg(Arg::with_name("INPUT FILE")
            .help("Sets the input file to read the model from (.mlr)")
            .required(true)
            .index(1))
        .arg(Arg::with_name("dynamic").long("dynamic").help("(experimental) allow dynamic restructuring/nesting").takes_value(false))
        .arg(Arg::with_name("simulate").long("simulate").help("Runs a basic(slow) Simulation. Until is the model time to run to in seconds. IO Stepsize stecifies the time between snapshots").value_names(&["UNTIL","IO STEPSIZE"]))
        .arg(Arg::with_name("keep_parameters").long("keep_parameters").help("Keep the parameters (defined with param instead of const) for the expanded model."))
       .get_matches();

    let filename = matches.value_of("INPUT FILE").unwrap();

    let model_file_hash;
    {
        let mut file = fs::read(filename)
            .expect("File not found!")
            /*.parse()
            .expect(("could not make string?"))*/;
        let mut hasher = Sha512::new();
        //hasher.input(&file);
        hasher.update(&mut file);
        let model_file_hash_dat = hasher.finalize();
        model_file_hash = model_file_hash_dat.iter().join("")
    }

    let mut m2: EnumaratedModel;
    let hash_file_name = format!("cache_{}.rule_cache", model_file_hash);
    let hash_file_name_parent = format!("../cache_{}.rule_cache", model_file_hash);
    if Path::exists(Path::new(&hash_file_name)) {
        println!(
            "-- this model has been read before, reusing cache file '{}'",
            hash_file_name
        );
        let file = File::open(Path::new(&hash_file_name)).unwrap();
        let reader = BufReader::new(file);
        m2 = bincode::deserialize_from(reader).expect("Error deserializeing");
        println!(
            "-- reading successfull ({} reactions, {} species)",
            m2.reactions.len(),
            m2.species.len()
        );
    } else if Path::exists(Path::new(&hash_file_name_parent)) {
        println!(
            "-- this model has been read before, reusing cache (parent level) file '{}'",
            hash_file_name_parent
        );
        let file = File::open(Path::new(&hash_file_name_parent)).unwrap();
        let reader = BufReader::new(file);
        m2 = bincode::deserialize_from(reader).expect("Error deserializeing");
        println!(
            "-- reading successfull ({} reactions, {} species)",
            m2.reactions.len(),
            m2.species.len()
        );
    } else {
        let (m, latex) = match Model::from_file(
            filename.to_string(),
            !matches.is_present("keep_parameters"),
            matches.is_present("dynamic"),
        ) {
            Ok(x) => x,
            Err(y) => {
                println!(
                    "There are Errors: \n{}",
                    y.into_iter()
                        .enumerate()
                        .map(|(n, v)| format!("ERROR {}\n{}", n + 1, v.what))
                        .join("\n")
                );
                return;
            }
        };

        println!("-- Model specification OK");
        println!("-- Writing Latex");
        {
            let mut file = File::create(filename.to_string() + ".tex").unwrap();
            write!(file, "{}", latex).unwrap();
        }

        //println!("{:?}", m.species);

        //println!("{:?}", m.transitions);
        let start_expand = Instant::now();
        m2 = EnumaratedModel::from_model(m);
        m2.flatten_a(100);
        println!("Expansion took {}s", start_expand.elapsed().as_secs_f64());

        {
            //println!("{}", m2.print_model());
            let mut file = File::create(filename.to_string() + ".reaction_model")
                .expect("could not open output file");
            write!(file, "{}", m2.print_model()).unwrap();
        }
        println!("-- Reaction network written");

        if m2.reactions.len() + m2.species.len() > 500 {
            // no need to store small models
            let buffer = File::create(Path::new(&hash_file_name)).expect("could not create file");
            bincode::serialize_into(buffer, &m2).expect("error writing");
            println!("-- Writing cache file '{}'", hash_file_name)
        }
    }

    match matches.values_of("simulate") {
        None => (),
        Some(mut x) => {
            if matches.value_of("keep_parameters").is_some() {
                panic!("You can not simulate AND keep parameters")
            }
            match &m2.model.settings.endtime {
                None => {}
                Some(y) => {
                    println!("Overwriting model defined end time of: {}", y);
                }
            }
            let until = x.next().unwrap();
            let until : f64 = until.parse().expect("the UNTIL value is invalid. It should be a floatingpoint number (e.g. 1., 1e7, 0.123, ...)");
            let step = x.next().unwrap();
            let step: usize = step
                .parse()
                .expect("the IO STEP value is invalid. It should be a positive number (e.g. 100)");

            if step < 5 {
                panic!("Choose a larger number of outputs (at least 5)");
            }

            run_in_term(m2, until, step); /*{
                                              model: m2,
                                              target_time: until,
                                              step_size: until / step as f64,
                                              output_file_name: "output.csv".to_string(),
                                              reaction_count_file_name: None,
                                          };*/
            /*println!("{:?}", sim.observation_values);
            println!("glob: {:?}", sim.obs_global_vals_names);
            println!("glob: {:?}", sim.global_values);
            println!("{:?}", sim.obs_times);*/
        }
    }
}

fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Clone,
{
    assert!(!v.is_empty());
    (0..v[0].len())
        .map(|i| v.iter().map(|inner| inner[i].clone()).collect::<Vec<T>>())
        .collect()
}

pub fn run_in_term(mut m: EnumaratedModel, target_time: f64, step_num: usize) {
    println!("Start Terminal Execution!");
    let total_time = Instant::now();

    let mut time_extra_expansion = Duration::new(0, 0);
    let mut expansion_counter = 0 as usize;

    let step_size = target_time / (step_num as f64 + 1.);
    //let now = Instant::now();

    assert!(step_size < target_time);

    let mut fast_sim = simulate_fast::SimulationFast::new(&m);
    let exec_time = Instant::now();
    println!("Sucessfully generated fast simulator");

    //self.write_header();
    //self.make_observations();

    let pb = ProgressBar::new(10000);
    pb.set_style(
        ProgressStyle::default_bar()
            .template(
                "{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {percent}% ({eta})",
            )
            .progress_chars("#>-"),
    );
    //let mut stepcount: usize = 0;
    while fast_sim.time < target_time {
        if fast_sim.stepcount % 1000 == 0 {
            pb.set_position((10000. * fast_sim.time / (10000. * target_time) * 10000.) as u64);
        }
        match fast_sim.step(step_size) {
            StepResult::AllGood => {
                //println!("Good Step")
            }
            StepResult::NoReactionPossible => {
                println!("Early termination, because no more reactions possible!")
            }
            StepResult::StructuralChange {
                idx_of_reaction: idx,
            } => {
                let start_expand = Instant::now();
                expansion_counter += 1;
                fast_sim.take_current_state_as_initial(&mut m);
                m = change_structure_and_reexpand(m, idx, 20);
                fast_sim = fast_sim.reset_to_changed_model(&mut m);
                time_extra_expansion += start_expand.elapsed();
            }
            StepResult::OnlyObservation => {
                //println!("Obs only")
            }
        }
    }
    pb.finish_with_message("Simulation complete");
    fast_sim.make_observations();

    //fast_sim.debug_print_counts();

    let mut file = File::create("output.csv").unwrap();
    writeln!(
        file,
        "time[s],{}",
        fast_sim.observation_names.iter().join(",")
    )
    .expect("file write error");
    /*fast_sim
    .observation_values
    .iter()
    .zip(fast_sim.obs_times.iter())
    .for_each(|(v, time)| {
        writeln!(
            file,
            "{},{}",
            time,
            v.iter()
                .map(|x| match x {
                    PrimitiveTypes::String(y) => y.clone(),
                    PrimitiveTypes::Boolean(y) => format!("{}", y),
                    PrimitiveTypes::Float(y) => format!("{}", y),
                    PrimitiveTypes::Integer(y) => format!("{}", y),
                })
                .join(",")
        )
        .expect("file write error")
    });*/

    assert_eq!(fast_sim.obs_times.len(), fast_sim.observation_values.len());
    fast_sim
        .observation_values
        .iter()
        .zip(fast_sim.obs_times.iter())
        .for_each(|(v, time)| {
            writeln!(
                file,
                "{},{}",
                time,
                v.iter()
                    .map(|x| match x {
                        PrimitiveTypes::String(y) => y.clone(),
                        PrimitiveTypes::Boolean(y) => format!("{}", y),
                        PrimitiveTypes::Float(y) => format!("{}", y),
                        PrimitiveTypes::Integer(y) => format!("{}", y),
                    })
                    .join(",")
            )
            .expect("file write error")
        });

    //todo make niceer iteration to save space
    let obs_reaction_counts_transposed = transpose(fast_sim.obs_reaction_counts.clone());

    let mut file = File::create("rule_counts.csv").unwrap();
    writeln!(
        file,
        "time[s],{}",
        fast_sim
            .reaction_names
            .iter()
            .map(|k| k.replace(",", " "))
            .join(",")
    )
    .expect("file write error");
    obs_reaction_counts_transposed
        .iter()
        .zip(fast_sim.obs_times.iter())
        .for_each(|(v, time)| {
            writeln!(
                file,
                "{},{}",
                time,
                v.iter().map(|x| format!("{}", x)).join(",")
            )
            .expect("file write error")
        });

    let t = total_time.elapsed().as_secs_f64();
    //println!("Output written to '{}'", self.output_file_name);
    println!(
        "Done ({} steps in {}s ({} graph finding) -> throughput {}/s or {}/s)",
        fast_sim.stepcount,
        t,
        exec_time.elapsed().as_secs_f64(),
        fast_sim.stepcount as f64 / t,
        fast_sim.stepcount as f64 / exec_time.elapsed().as_secs_f64()
    );
    if expansion_counter > 0 {
        println!(
            "  -- {} structure changing steps in {}s -> throughput {}/s",
            expansion_counter,
            time_extra_expansion.as_secs_f64(),
            expansion_counter as f64 / time_extra_expansion.as_secs_f64()
        );
        println!(
            "  -- {} regular steps in {}s -> throughput {}/s",
            fast_sim.stepcount - expansion_counter,
            t - time_extra_expansion.as_secs_f64(),
            (fast_sim.stepcount - expansion_counter) as f64
                / (t - time_extra_expansion.as_secs_f64())
        );
    }
}
/*=======


impl simulate_fast::SimulationFast {
    pub fn run_term(&mut self) {
        let now = Instant::now();

        assert!(self.step_size < self.target_time);

        //self.write_header();
        //self.make_observations();

        let pb = ProgressBar::new(10000);
        pb.set_style(
            ProgressStyle::default_bar()
                .template(
                    "{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {percent}% ({eta})",
                )
                .progress_chars("#>-"),
        );
        //let mut stepcount: usize = 0;
        while self.time < self.target_time {
            self.step();
        }
        pb.finish_with_message("Simulation complete");
        self.make_observations();

        let mut file = File::create("output.csv").unwrap();
        writeln!(file, "time[s],{}", self.observation_names.iter().join(",")).expect("file write error");
        self.observation_values.iter().zip(self.obs_times.iter()).for_each(|(v,time)| {
            writeln!(file, "{},{}",time, v.iter().map(|x| match x{
                PrimitiveTypes::String(y) => y.clone(),
                PrimitiveTypes::Boolean(y) => format!("{}",y),
                PrimitiveTypes::Float(y) => format!("{}",y),
                PrimitiveTypes::Integer(y) => format!("{}",y),
            }).join(","))
                .expect("file write error")
        });

        //todo make niceer iteration to save space
        let obs_reaction_counts_transposed = transpose(self.obs_reaction_counts.clone());

        let mut file = File::create("rule_counts.csv").unwrap();
                writeln!(file, "time[s],{}", self.reaction_names.iter().join(",")).expect("file write error");
                obs_reaction_counts_transposed.iter().zip(self.obs_times.iter()).for_each(|(v,time)| {
                    writeln!(file, "{},{}",time, v.iter().map(|x| format!("{}",x)).join(","))
                        .expect("file write error")
                });

        let t = now.elapsed().as_secs_f64();
        //println!("Output written to '{}'", self.output_file_name);
        println!(
            "Done ({} steps in {}s -> throughput {}/s)",
            self.stepcount,
            t,
            self.stepcount as f64 / t
        );
    }
>>>>>>> master
}
*/
