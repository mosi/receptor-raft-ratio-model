import os
import shutil
import string
import subprocess
import random
import multiprocessing
import glob

import matplotlib
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.ticker as mtick
import colorcet
from tqdm import tqdm
import matplotlib.pylab as pl
from matplotlib.ticker import ScalarFormatter

PLOT_DETAIL = 1 # 0 - use old data, 1 fast, 2 medium , 3 slow, 4 all in

def initial_setup():
    subprocess.check_output("cargo build --release --manifest-path=simulator/Cargo.toml",shell=True)

initial_setup()


def generate_random_name(k: int = 10):
    return "z" + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(k))


def get_lrp6_counts(df):
    lrp6_tot = 0
    lrp6_p = 0
    lrp6_b = 0
    lrp6_inLR = 0

    LRP6__bind_false__phos_false = 0
    LRP6__bind_true__phos_true_inLR = 0
    LRP6__bind_true__phos_false = 0
    LRP6__bind_false__phos_false_inLR = 0
    LRP6__bind_true__phos_true = 0
    LRP6__bind_true__phos_false_inLR = 0
    AxinLrp6__phos_true = 0
    AxinLrp6__phos_false = 0

    for c in df.columns:
        if c.startswith("AxinLrp6"):
            val = int(df[c].iloc[-1])
            lrp6_tot += val
            lrp6_inLR += val
            phos = "phos_true" in c
            if "phos_true" in c:
                AxinLrp6__phos_true += val
                lrp6_p += val

            else:
                assert("phos_false" in c)
                AxinLrp6__phos_false += val

            assert(not c.startswith("LRP6"))

        if c.startswith("LRP6"):

            val = int(df[c].iloc[-1])

            lrp6_tot += val

            phos = "phos_true" in c
            bind = "bind_true" in c
            inLR = "_inLR" in c

            if phos:
                lrp6_p += val
            if bind:
                lrp6_b += val
            if inLR:
                lrp6_inLR += val

            if inLR:
                if not bind:
                    assert (not phos)
                    LRP6__bind_false__phos_false_inLR += val
                if bind:
                    if not phos:
                        LRP6__bind_true__phos_false_inLR += val
                    if phos:
                        LRP6__bind_true__phos_true_inLR += val

            if not inLR:
                if not bind:
                    LRP6__bind_false__phos_false += val
                if bind:
                    if not phos:
                        LRP6__bind_true__phos_false += val
                    if phos:
                        LRP6__bind_true__phos_true += val

    return {"p_LRP6_in_rafts": lrp6_inLR / lrp6_tot, "p_LRP6_p": lrp6_p / lrp6_tot, "p_LRP6_b": lrp6_b / lrp6_tot,
            "#LRP6": lrp6_tot, "LRP6__bind_false__phos_false": LRP6__bind_false__phos_false,
            "LRP6__bind_true__phos_true_inLR": LRP6__bind_true__phos_true_inLR,
            "LRP6__bind_true__phos_false": LRP6__bind_true__phos_false,
            "LRP6__bind_false__phos_false_inLR": LRP6__bind_false__phos_false_inLR,
            "LRP6__bind_true__phos_true": LRP6__bind_true__phos_true,
            "LRP6__bind_true__phos_false_inLR": LRP6__bind_true__phos_false_inLR,"AxinLrp6__phos_true":AxinLrp6__phos_true, "AxinLrp6__phos_false":AxinLrp6__phos_false}

def execute_model(setup):
    # Read setup data
    nLR = int(setup["nLR"])
    try:
        target_time = setup["target_time"]
        simulate_str = " --simulate {} 500".format(target_time)
    except:
        target_time = None
        simulate_str = " "
    kWsyn = float(setup["kWsyn"])
    kLAxinBind = float(setup["kLAxinBind"])

    with open('WntModel_2021.mlrj', 'r') as file:
        model = file.read()
    model = model.replace("##nLR##",str(nLR))
    model = model.replace("##kLAxinBind##", str(kLAxinBind))
    model = model.replace("##kWsyn##", str(kWsyn))

    my_name = generate_random_name() + "_"+str(nLR)
    modelname = "model_{}.mlr".format(nLR)
    os.mkdir(my_name)
    with open(my_name + "/"+modelname, "w") as text_file:
        text_file.write(model)
    try:
        with open(my_name+"/stdout.txt", "wb") as logfile:
            subprocess.run(
                "../simulator/target/release/rust-expander "+modelname +" " + simulate_str,
                shell=True, cwd=my_name, check=True, stderr=subprocess.DEVNULL, stdout=logfile,
                capture_output=False)
    except subprocess.CalledProcessError as e:
        print("ERROR EXECUTING SUBPROCESS (Out of Memory?) ",setup)
        shutil.rmtree(my_name)
        return None

    caches = glob.glob(my_name + "/cache_*")
    assert (len(caches) <= 1)
    for c in caches:
        shutil.copy(c, ".")

    if target_time == None:
        shutil.rmtree(my_name)
        return True

    output = pd.read_csv(my_name + "/output.csv")

    output["total_beta_cat_in_nuc"] = output.filter(regex=("bCat_inNucleus*")).sum(axis=1)
    result_betaCat = pd.DataFrame()
    result_betaCat["beta_cat"] = output["total_beta_cat_in_nuc"]
    result_betaCat["time"] = output["time[s]"]
    for k in setup:
        result_betaCat[k] = setup[k]


    result_lrp6 = get_lrp6_counts(output)
    for k in setup:
        result_lrp6[k] = setup[k]
    result_df = pd.DataFrame()
    result_lrp6 = result_df.append(result_lrp6,ignore_index=True)

    # shutil.copy(my_name+"/output.csv",model_name+".csv")
    shutil.rmtree(my_name)

    return (result_betaCat,result_lrp6)

def generate_data(do_expansion_step = True,reps=5,fast=True):
    initial_setup()

    settings = []
    S1 = np.logspace(np.log10(1), np.log10(30000), base=10, num=15, dtype=int)
    S2 = np.logspace(np.log(1),np.log10(1000),base=10,num=20,dtype=int)
    nLRs_prelim = sorted(list(set(np.concatenate((S1,S2)))),reverse=True)
    nLRs = []
    for n in nLRs_prelim:
        if n < 4000 or not fast:
                nLRs.append(n)
    print(nLRs)



    for nLR in nLRs:
        for kWsyn in [4.5e-7,1.5e-6,4.5e-6]:
            for kLAxinBind in [2.412e6]:
                #print(nLR)
                settings.append({"nLR":nLR,"kWsyn":kWsyn,"kLAxinBind":kLAxinBind})


    if do_expansion_step:
        with multiprocessing.Pool() as pool:
            my_res = list(zip(tqdm(pool.imap_unordered(execute_model, settings), total=len(settings),
                            desc="run simulation (expanding)"),settings))
        for (out,setting) in my_res:
            if out == None:
                print("not able to expand ",setting,"\nretry sequentially")
                execute_model(setting)


    settings_2 = []
    settings_2Big = []
    settings_2BigXL = []
    for s in settings:
        my_reps = reps
        if s["nLR"] < 1000:
            my_reps *= 3
        if s["nLR"] < 500:
            my_reps *= 4
        if s["nLR"] <= 3000:
            my_reps *= 2
        for _ in range(my_reps):
            s["target_time"]= 43200
            if s["nLR"] < 7000:
                settings_2.append(s)
            else:
                if s["nLR"] < 15000:
                    settings_2Big.append(s)
                else:
                    settings_2BigXL.append(s)


    #random.shuffle(settings_2)
    res = []

    with multiprocessing.Pool(processes=1) as pool:
        res.extend(list(zip(tqdm(pool.imap_unordered(execute_model, settings_2BigXL), total=len(settings_2BigXL),
                        desc="run simulation (BIGXL reps)"),settings_2BigXL)))

    with multiprocessing.Pool() as pool:
        res.extend(list(zip(tqdm(pool.imap_unordered(execute_model, settings_2), total=len(settings_2),
                        desc="run simulation (reps)"),settings_2)))

    with multiprocessing.Pool(processes=12) as pool:
        res.extend(list(zip(tqdm(pool.imap_unordered(execute_model, settings_2Big), total=len(settings_2Big),
                        desc="run simulation (BIG reps)"),settings_2Big)))

    with multiprocessing.Pool(processes=1) as pool:
        res.extend(list(zip(tqdm(pool.imap_unordered(execute_model, settings_2BigXL), total=len(settings_2BigXL),
                        desc="run simulation (BIGXL reps)"),settings_2BigXL)))


    ### find unsuccessfull runs und run them using fewer threads (memory limit)
    print("unpack results & rerun failed ones")
    num_cpu = multiprocessing.cpu_count()
    first = True
    res_betaCat = []
    res_lrp6 = []

    while num_cpu > 1 or first:
        first = False
        incompleted = []
        num_cpu = int(num_cpu/2)
        for (output,settings) in res:
            if output == None:
                print("incomplete: ",settings)
                incompleted.append(settings)
            else:
                (betaCat, lrp6) = output
                res_betaCat.append(betaCat)
                res_lrp6.append(lrp6)
        with multiprocessing.Pool(processes=num_cpu) as pool:
            res = list(zip(tqdm(pool.imap_unordered(execute_model, incompleted), total=len(incompleted),
                                 desc="(re-)run simulation (failed ones) {}".format(num_cpu)), incompleted))

    # unpack results



    print("concat data frames")
    df_betaCat = pd.concat(res_betaCat)
    df_lrp6 = pd.concat(res_lrp6)

    print("write data")
    try:
        df_betaCat = pd.concat([pd.read_csv("beta_cat_traj.csv"),df_betaCat])
        df_lrp6 = pd.concat([pd.read_csv("lrp6_data.csv"),df_lrp6])
        print("appending to old data")

    except:
        print("generate new file")

    df_betaCat.to_csv("beta_cat_traj.csv")
    df_lrp6.to_csv("lrp6_data.csv")



def plot_data():
    print("start plot")
    use_log = False
    df_beta_cat= pd.read_csv("beta_cat_traj.csv")
    df_lrp6 = pd.read_csv("lrp6_data.csv")

    df_lrp6 = df_lrp6.groupby(["kLAxinBind","kWsyn","nLR"]).mean()
    wnt_labels = ["low", "medium", "high"]

    for kLAxinBind in df_lrp6.index.levels[0]:
        mydf = df_lrp6.xs(kLAxinBind)
        for ((n,kWsyn),wnttxt) in zip(enumerate(mydf.index.levels[0]),wnt_labels):
            plt.subplot(2,len(mydf.index.levels[0]),n+1)

            df = mydf.xs(kWsyn)

            plt.xscale('log')
            plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter(1))

            l6_b_p =  df["LRP6__bind_true__phos_true"] / df["#LRP6"]
            l6_b__ = df["LRP6__bind_true__phos_false"] / df["#LRP6"]
            l6____ = df["LRP6__bind_false__phos_false"] / df["#LRP6"]
            a6___p = df["AxinLrp6__phos_true"] / df["#LRP6"]
            a6____ = df["AxinLrp6__phos_false"] / df["#LRP6"]
            r__b_p = df["LRP6__bind_true__phos_true_inLR"] / df["#LRP6"]
            r_b___ = df["LRP6__bind_true__phos_false_inLR"] / df["#LRP6"]
            r_____ = df["LRP6__bind_false__phos_false_inLR"] / df["#LRP6"]

            if False:
                plt.stackplot(df.index, df["LRP6__bind_true__phos_true"] / df["#LRP6"],
                          df["LRP6__bind_true__phos_false"] / df["#LRP6"],
                          df["LRP6__bind_false__phos_false"] / df["#LRP6"],
                          df["AxinLrp6__phos_true"] / df["#LRP6"],
                          df["AxinLrp6__phos_false"] / df["#LRP6"],
                          df["LRP6__bind_true__phos_true_inLR"] / df["#LRP6"],
                          df["LRP6__bind_true__phos_false_inLR"] / df["#LRP6"],
                          df["LRP6__bind_false__phos_false_inLR"] / df["#LRP6"],
                          labels=["LRP6__bind_true__phos_true", "LRP6__bind_true__phos_false",
                                  "LRP6__bind_false__phos_false", "AxinLrp6__phos_true", "AxinLrp6__phos_false",
                                  "LRP6__bind_true__phos_true_inLR", "LRP6__bind_true__phos_false_inLR",
                                  "LRP6__bind_false__phos_false_inLR"], alpha=0.2)

            plt.fill_between(df.index, l6_b_p + l6_b__ + l6____, 1, label="in LR", color="lightgray",hatch="*")

            plt.fill_between(df.index, 0,
                             l6_b__ + l6_b_p, label="bound WNT", hatch="//", fc="none", ec="C1")
            plt.fill_between(df.index, 1- r__b_p - r_____ - r_b___,
                             1 - r_____, hatch="//", fc="none", ec="C1")


            plt.fill_between(df.index, l6_b__ + l6____ + l6_b_p,
                             l6_b__ + l6____ + l6_b_p + a6____ + a6___p, label="bound Axin", hatch="\\", fc="none", ec="C3")

            plt.fill_between(df.index, l6_b__ + l6____ + l6_b_p,
                             l6_b__ + l6____ + l6_b_p + a6___p, label="phos", hatch="o",
                             fc="none", ec="C0")

            plt.fill_between(df.index, 0, l6_b_p, hatch="o",
                             fc="none", ec="C0")
            plt.fill_between(df.index, 1-r_____ - r_b___ - r__b_p,1 -r_b___ - r_____,  hatch="o",
                             fc="none", ec="C0")



            """
                plt.fill_between(df.index, 1 - df["p_LRP6_in_rafts"], 1, label="in LR", color="lightgray")
    
                plt.fill_between(df.index, 0,
                                 df["LRP6__bind_true__phos_true"] / df["#LRP6"] + df["LRP6__bind_true__phos_false"] / df[
                                     "#LRP6"], label="bound", hatch="//", fc="none", ec="C1")
                plt.fill_between(df.index, 1 - df["p_LRP6_in_rafts"] + df["AxinLrp6__phos_true"] / df["#LRP6"] + df[
                    "AxinLrp6__phos_false"] / df["#LRP6"],
                                 1 - df["p_LRP6_in_rafts"] + df["LRP6__bind_true__phos_true_inLR"] / df["#LRP6"] + df[
                                     "LRP6__bind_true__phos_false_inLR"] / df["#LRP6"], hatch="//", fc="none", ec="C1")
    
                plt.fill_between(df.index, 0, df["LRP6__bind_true__phos_true"] / df["#LRP6"], label="phos", hatch="o",
                                 fc="none", ec="C0")
                plt.fill_between(df.index, 1 - df["p_LRP6_in_rafts"] + df["AxinLrp6__phos_false"] / df["#LRP6"],
                                 1 - df["p_LRP6_in_rafts"] + df["LRP6__bind_true__phos_true_inLR"] / df["#LRP6"] + df[
                                     "AxinLrp6__phos_true"] / df["#LRP6"], hatch="o",
                                             fc="none", ec="C0")
             """
            if False: #reference for the data and check stacking
                plt.stackplot(df.index, df["LRP6__bind_true__phos_true"] / df["#LRP6"],
                              df["LRP6__bind_true__phos_false"] / df["#LRP6"], df["LRP6__bind_false__phos_false"] / df["#LRP6"],
                              df["AxinLrp6__phos_true"] / df["#LRP6"],
                              df["AxinLrp6__phos_false"] / df["#LRP6"],
                              df["LRP6__bind_true__phos_true_inLR"] / df["#LRP6"],
                              df["LRP6__bind_true__phos_false_inLR"] / df["#LRP6"],
                              df["LRP6__bind_false__phos_false_inLR"] / df["#LRP6"],
                              labels=["LRP6__bind_true__phos_true","LRP6__bind_true__phos_false","LRP6__bind_false__phos_false","AxinLrp6__phos_true","AxinLrp6__phos_false","LRP6__bind_true__phos_true_inLR","LRP6__bind_true__phos_false_inLR","LRP6__bind_false__phos_false_inLR"], alpha=0.5)

            plt.xlabel("Number of Lipid Rafts")
            number = "{}".format(float(kWsyn)).replace("e","\cdot 10^{") + "}"
            plt.title("{} wnt $({}\\frac{{mol}}{{m^3 min}})$".format(wnttxt ,number.replace("-0","-")))

            if n == 0:
                plt.ylabel("Composition of LRP6")
            else:
                pass
                #plt.setp(plt.gca().get_yticklabels(), visible=False)
            if n == 2:
                plt.legend(ncol=1, bbox_to_anchor=(1.6, 1.))
            plt.ylim(ymax=1., ymin=0)
            plt.xlim(xmin=df.index.min(), xmax=df.index.max())

        my_cm = colorcet.cm.rainbow
        data = df_beta_cat[df_beta_cat["kLAxinBind"]==kLAxinBind].groupby(["kWsyn","nLR","time"]).mean()
        for (pn,kWsyn) in enumerate(data.index.levels[0]):
            plt.subplot(2,len(data.index.levels[0]),pn+1 + len(data.index.levels[0]))
            #plt.xlim(xmin=0,xmax=mydf.xs(1).index.max()/60)

            if(use_log):
                plt.ylim(ymin=0.9,ymax = data["beta_cat"].max()/5283 + 0.5)
                plt.yscale("log")
                fmt = ScalarFormatter()
                fmt.set_scientific(False)
                from matplotlib.ticker import FuncFormatter
                fmt = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
                plt.gca().yaxis.set_major_formatter(fmt)
                from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
                plt.gca().yaxis.set_major_locator(MultipleLocator(1))
                plt.gca().yaxis.set_minor_locator(MultipleLocator(.2))
                plt.gca().yaxis.set_minor_formatter("")
            else:
                plt.ylim(ymin=0.7,ymax = data["beta_cat"].max()/5283 + 0.3)
                plt.yscale("linear")

            if (pn == 0):
                plt.ylabel("Fold change $\\frac{\\beta cat(t=t_{final})}{\\beta cat(t=0)}$")
            plt.xlabel("time [min]")
            #plt.title("kWsyn={}".format(kWsyn))
            mydf = data.xs(kWsyn)
            for (n,nLR) in enumerate(mydf.index.levels[0]):
                    try:
                        if (n != len(mydf.index.levels[0]) - 1):# and False:
                            interp_steps = 1
                            for i in range(interp_steps):
                                nLR_next = mydf.index.levels[0][n + 1]
                                i2 = interp_steps - i
                                nlp_delta = (nLR_next - nLR) / interp_steps
                                color = my_cm(np.log((nLR + nlp_delta * i)) / np.log(np.max(mydf.index.levels[0])))
                                y1 = np.array(mydf.xs(nLR)["beta_cat"]) / 5283
                                y2 = np.array(mydf.xs(nLR_next)["beta_cat"]) / 5283
                                #print(y1)
                                #print(y2)
                                delta = (y2 - y1) / interp_steps
                                # delta[delta < 0] = 0
                                plt.fill_between(mydf.xs(nLR).index/60, y1 + delta * i, y1 + delta * (i + 1), fc=color,
                                                 ec=color)  # "black",lw=0.3)
                        if n == 0:# or True:
                            color = my_cm((nLR ) / np.max(mydf.index.levels[0]))
                            #plt.plot(mydf.xs(nLR).index/60,mydf.xs(nLR)["beta_cat"]/5283,c=color)#my_cm(nLR/data.index.levels[1].max()))
                            pass
                    except KeyError as e:
                         print("missing key",e)
                         pass
        vmax = data.index.levels[1].max()
        if vmax > 8000 and vmax < 10000:
            vmax = 10000
        #        cb = plt.colorbar(pl.cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=1,vmax=vmax), cmap=my_cm))
        cb = plt.colorbar(pl.cm.ScalarMappable(norm=matplotlib.colors.LogNorm(vmin=1,vmax=vmax), cmap=my_cm))
        cb.set_label("number of LR")
        cb.ax.invert_yaxis()

        plt.gcf().set_size_inches(11, 6, forward=True)
        plt.tight_layout()

        plt.savefig("plot_kAxinBind_{}.pdf".format(kLAxinBind))
        plt.savefig("plot_kAxinBind_{}.svg".format(kLAxinBind))
        #plt.show()
        plt.clf()
    print("done plot")

generate_data(True,2,fast=True)
plot_data()

generate_data(True,2,fast=True)
plot_data()

generate_data(True,2)
plot_data()
for i in range(20):
    print("rep {}".format(i))
    generate_data(False, 2)
    plot_data()







